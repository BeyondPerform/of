<?php

namespace App;

use App\User;
use App\CseLog;
use App\Helpers;
use Illuminate\Database\Eloquent\Model;

class CjSigninEmails extends Model {

	/**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'cj_signin_emails';

  /**
   * Table primary key.
   *
   * @var array
   */
  protected $primary_key = 'id';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
  	'user_id',
  	'mail_id',
  	'status',
    'schedule_date',
  	'log'
  ];

  public function __construct($data = null) {
    if($data) {
      parent::__construct($data);
    }
  }

  public static function startQueueSenderCheck() {
    $queue_stack = CjSigninEmails::where('status', 0)->get();
    foreach($queue_stack as $k => $q) {
      $q_user = User::where('id', $q->user_id)->first();
      if($q_user) {
        $fname = '';
				$fname = $q_user->fname;
        $send_email = $q_user->email;
        $subs = array(
          '-fname-' => array($fname)
        );
        $toname = $fname;
        switch ($q->mail_id) {
            case 1:
                $subject = "WARNING: Making Profit like this is addictive";
                $template_id = "b234c02c-fab4-412a-a4ac-270d214a2a05";
                break;
            case 2:
                $subject = "Breaking NEWS!";
                $template_id = "ca59c93e-c9f9-49de-8c94-897bcd122def";
                break;
            case 3:
                $subject = "You snooze, you lose";
                $template_id = "60673c5a-2da4-4150-8345-8ae4f6929b03";
                break;
            case 4:
                $subject = "Don’t be pipped to the post";
                $template_id = "2de52e35-0a0d-49c0-b825-6547a0b543b2";
                break;
            case 5:
                $subject = "Procrastination = Regret";
                $template_id = "660e3045-97c8-4c58-996f-b0258d48bbf8";
                break;
						case 6:
                $subject = "Tick Tock Time Is Running Out! A Few Spaces Left..";
                $template_id = "29747b86-01ff-4a09-bb43-487d1d45efbc";
                break;
						case 7:
                $subject = "Act NOW - Only 1 place remaining";
                $template_id = "9081d58b-c539-4b69-ad0d-6f6e479422bf";
                break;
        }
				// === check againsta schedule date here
				// === cron on server is starting every 3 minutes
				// === normal infelicity - 10 minutes
				$current_time = time();
				$schedule_date_stamp = strtotime($q->schedule_date);
				$infelicity_seconds = $schedule_date_stamp - $current_time;
				if($current_time <= $schedule_date_stamp && $infelicity_seconds < 600) {
					Helpers::_sgMailer($template_id, $subs, $send_email, $toname, $subject);
					$update_model = CjSigninEmails::find($q->id);
					$update_model->status = 1;
					$update_model->log = "Sent";
					$update_model->save();
				} else {
					$cse_log_data = array(
						'ces_id' => $q->id,
						'user_id' => $q->user_id,
						'mail_id' => $q->mail_id,
						'status' => $q->status,
						'schedule_date' => $q->schedule_date,
						'log' => 'not inside schedule'
					);
					$cse_log = new CseLog($cse_log_data);
					$cse_log->save();
				}
      } else {
				$cse_log_data = array(
					'ces_id' => $q->id,
					'user_id' => $q->user_id,
					'mail_id' => $q->mail_id,
					'status' => $q->status,
					'schedule_date' => $q->schedule_date,
					'log' => 'user is not found in DB'
				);
				$cse_log = new CseLog($cse_log_data);
				$cse_log->save();
      }
    }
    return true;
  }

}
