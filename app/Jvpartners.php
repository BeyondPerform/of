<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jvpartners extends Model {

	/**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'jvpartners';

  /**
   * Table primary key.
   *
   * @var array
   */
  protected $primary_key = 'id';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
  	'clicksure_id',
  	'fname',
		'lname',
  	'email',
    'skype_id',
    'ip',
  	'country',
    'country_code'
  ];

  public function __construct($data = null) {
    if($data) {
      parent::__construct($data);
    }
  }

}
