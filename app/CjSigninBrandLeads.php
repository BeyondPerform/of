<?php

namespace App;

use App\User;
use App\Helpers;
use Illuminate\Database\Eloquent\Model;

class CjSigninBrandLeads extends Model {

	/**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'cj_signin_brand_leads';

  /**
   * Table primary key.
   *
   * @var array
   */
  protected $primary_key = 'id';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
  	'user_id',
  	'brand',
  	'status',
    'schedule_date',
		'debug',
  	'log'
  ];

  public function __construct($data = null) {
    if($data) {
      parent::__construct($data);
    }
  }

  public static function startQueueSenderLeads() {
		$queue_stack = CjSigninBrandLeads::where('status', 0)->get();
		foreach($queue_stack as $k => $q) {
			// === check againsta schedule date here
			// === cron on server is starting every 3 minutes
			// === normal infelicity - 10 minutes
			$current_time = time();
			$schedule_date_stamp = strtotime($q->schedule_date);
			$infelicity_seconds = $schedule_date_stamp - $current_time;
			if($current_time <= $schedule_date_stamp && $infelicity_seconds < 600) {
				$q_user = User::where('id', $q->user_id)->first();
	      if($q_user) {
					$send_lead_res = Helpers::_sendBrandLead($q, $q_user);
					$q->status = 2;
					$q->log = $send_lead_res['msg'];
					$q->debug = $send_lead_res['debug'];
					if($send_lead_res['is_error']) {
						$q->status = 1;
					}
				} else {
					$q->status = 1;
					$q->log = "user is not found in DB";
				}
				$q->save();
			}
		}
		return true;
  }

}
