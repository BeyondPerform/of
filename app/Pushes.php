<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\HelloMarketsApi as HelloMarketsApi;
use App\HelloMarketsApiBank as HelloMarketsApiBank;

class Pushes extends Model {

	/**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'push';

  /**
   * Table primary key.
   *
   * @var array
   */
  protected $primary_key = 'id';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
  	'lead_id',
  	'brand',
  	'status',
  	'response'
  ];

  public static $ac_countries = array(
    array('country' => 'United Kingdom', 'country_code' => 'GB', 'code' => 225),
    array('country' => 'New Zealand', 'country_code' => 'NZ', 'code' => 153),
    array('country' => 'Australia', 'country_code' => 'AU', 'code' => 13),
    array('country' => 'Canada', 'country_code' => 'CA', 'code' => 38),
    array('country' => 'Singapore', 'country_code' => 'SG', 'code' => 192),
    array('country' => 'South Africa', 'country_code' => 'ZA', 'code' => 197),
  );

  public function __construct($data = null) {
    if($data) {
      parent::__construct($data);
    }
  }

  public static function pushCurlRequestSender($data, $brand, $type = 'brand') {

    $res_data = array(
      'api' => array(),
      'db_data' => array()
    );

    $push_status = 1;
    $push_response = 'Success';
    $api_result_bean = array('brand' => $brand, 'status' => true, 'msg' => 'success');

    $db_data = array(
      'lead_id' => $data['id'],
      'brand' => $brand,
      'type' => $type,
      'status' => 0,
      'response' => ""
    );

		if($brand == 'TradeSolid') {

			$zt_ac_countries = array(
				array('country' => 'Netherland', 'country_code' => 'NL', 'code' => 150),
				array('country' => 'Germany', 'country_code' => 'DE', 'code' => 80),
				array('country' => 'Norway', 'country_code' => 'NO', 'code' => 150),
				array('country' => 'Iceland', 'country_code' => 'IS', 'code' => 98),
				array('country' => 'Switzerland', 'country_code' => 'CH', 'code' => 206),
				array('country' => 'Sweden', 'country_code' => 'SE', 'code' => 205),
				array('country' => 'Malaysia', 'country_code' => 'MY', 'code' => 129),
				array('country' => 'Australia', 'country_code' => 'AU', 'code' => 13),
				array('country' => 'Hong Kong', 'country_code' => 'HK', 'code' => 96),
				array('country' => 'South Africa', 'country_code' => 'ZA', 'code' => 197),
				array('country' => 'United Arab Emirates', 'country_code' => 'AE', 'code' => 224),
				array('country' => 'Saudi Arabia', 'country_code' => 'SA', 'code' => 187),
				array('country' => 'Qatar', 'country_code' => 'QA', 'code' => 174)
      );
			$is_legal_county = false;
      $c_key = -1;
      if($data['country'] !== "") {
        foreach($zt_ac_countries as $k => $lc) {
          if($lc['country'] == $data['country'] || $lc['country_code'] == $data['country_code']) {
            $c_key = $k;
            $is_legal_county = true;
            break;
          }
        }
      }

			if($is_legal_county) {

				$send_url = "http://api-spotplatform.tradesolid.com/api";
        $send_data = array(
          'api_username' => 'thebinaryinstitute',
          'api_password' => 'ANrx6p6B4a',
          'MODULE' => 'Customer',
          'COMMAND' => 'add',
          'FirstName' => $data['fname'],
          'LastName' => $data['lname'],
          'email' => $data['email'],
          'password' => $data['psw'],
					'Phone' => $data['phone'],
          'Country' => $zt_ac_countries[$c_key]['code'],
					'currency' => 'EUR',
					'birthday' => "1980-07-21",
					'gender' => 'male',
					'registrationCountry' => $zt_ac_countries[$c_key]['code'],
          'campaignId' => 64,
          // 'subCampaign' => 4322,
					// 'subcampaignName' => 35115,
					'subcampaignID' => 4322,
          'a_id' => 35191,
					'regulateStatus' => 'pending',
					'regulateType' => 1
        );
        $ch = curl_init($send_url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($send_data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        $api_result = simplexml_load_string($result);
        $res_data['api'] = $api_result;
        curl_close($ch);

				if($api_result->errors && $api_result->errors->error) {
          $error_msg = "";
          foreach($api_result->errors->error as $err) {
            $error_msg .= $err.", ";
          }
          $error_msg = substr($error_msg, 0, -2);
					$push_status = 0;
	        $push_response = $error_msg;
	        $api_result_bean['status'] = false;
	        $api_result_bean['msg'] = $error_msg;
        }

			} else {
				$push_status = 0;
        $push_response = "Your country ".$data['country']." is not valid for sending a lead";
        $api_result_bean['status'] = false;
        $api_result_bean['msg'] = $push_response;
			}

		} else if($brand == 'Option500') {

			$zt_ac_countries = array(
				array('country' => 'United Kingdom', 'country_code' => 'GB', 'code' => 225),
				array('country' => 'Canada', 'country_code' => 'CA', 'code' => 204),
				array('country' => 'New Zealand', 'country_code' => 'NZ', 'code' => 153),
				array('country' => 'Australia', 'country_code' => 'AU', 'code' => 13),
				array('country' => 'Singapore', 'country_code' => 'SG', 'code' => 192),
				array('country' => 'Hong Kong', 'country_code' => 'HK', 'code' => 96),
				array('country' => 'Malaysia', 'country_code' => 'MY', 'code' => 129),
				array('country' => 'United Arab Emirates', 'country_code' => 'AE', 'code' => 224),
				array('country' => 'South Africa', 'country_code' => 'ZA', 'code' => 197)
      );

			$is_legal_county = false;
      $c_key = -1;
      if($data['country'] !== "") {
        foreach($zt_ac_countries as $k => $lc) {
          if($lc['country'] == $data['country'] || $lc['country_code'] == $data['country_code']) {
            $c_key = $k;
            $is_legal_county = true;
            break;
          }
        }
      }

			if($is_legal_county) {

				$send_url = "https://platform-api.tradesmarter.com/index/register";
				$send_data = array(
	        'firstName' => $data['fname'],
	        'lastName' => $data['lname'],
	        'email' => $data['email'],
	        'password' => md5($data['psw']),
	        'phone' => $data['phone'],
	        'country' => $zt_ac_countries[$c_key]['code'],
	        'landing' => json_encode(array('a_aid'=>'8b1c1e8a'))
	      );
	      $ch = curl_init($send_url);
	      curl_setopt($ch, CURLOPT_HEADER, 0);
	      curl_setopt($ch, CURLOPT_POST, true);
	      curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	      curl_setopt($ch, CURLOPT_POST, 1);
	      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	      curl_setopt($ch, CURLOPT_POSTFIELDS, $send_data);
	      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	      curl_setopt($ch, CURLOPT_USERPWD, "500-binstitute:15ea44da");
	      $result = curl_exec($ch);
	      curl_close($ch);

				if(is_object($result) && $result->error) {
	        $error_msg = "";
	        foreach($api_result->errors->error as $err) {
	          $error_msg .= $err.", ";
	        }
	        $error_msg = substr($error_msg, 0, -2);
	        $push_status = 0;
	        $push_response = $error_msg;
	        $api_result_bean['status'] = false;
	        $api_result_bean['msg'] = $error_msg;
	      }

			} else {
				$push_status = 0;
        $push_response = "Your country ".$data['country']." is not valid for sending a lead";
        $api_result_bean['status'] = false;
        $api_result_bean['msg'] = $push_response;
			}

		} else if($brand == 'OneTwoTrade') {

			$zt_ac_countries = array(
        array('country' => 'Spain', 'country_code' => 'ES', 'code' => 199),
				array('country' => 'United Kingdom', 'country_code' => 'GB', 'code' => 225),
				array('country' => 'Canada', 'country_code' => 'CA', 'code' => 204),
				array('country' => 'Italy', 'country_code' => 'IT', 'code' => 105),
				array('country' => 'Netherlands', 'country_code' => 'NL', 'code' => 150),
				array('country' => 'New Zealand', 'country_code' => 'NZ', 'code' => 153),
				array('country' => 'Denmark', 'country_code' => 'DK', 'code' => 58),
				array('country' => 'Sweden', 'country_code' => 'SE', 'code' => 205),
				array('country' => 'France', 'country_code' => 'FR', 'code' => 73),
				array('country' => 'Germany', 'country_code' => 'DE', 'code' => 80)
      );

			// $data['country'] = 'Spain';
			$is_legal_county = false;
      $c_key = -1;
      if($data['country'] !== "") {
        foreach($zt_ac_countries as $k => $lc) {
          if($lc['country'] == $data['country'] || $lc['country_code'] == $data['country_code']) {
            $c_key = $k;
            $is_legal_county = true;
            break;
          }
        }
      }

			if($is_legal_county) {

				$send_url = "http://www.api.onetwotrade.com/api";
        $send_data = array(
					'api_username' => 'TheBinaryInstitute_649',
          'api_password' => 'HfS8xq9W0k',
          'MODULE' => 'Customer',
          'COMMAND' => 'add',
          'FirstName' => $data['fname'],
          'LastName' => $data['lname'],
          'email' => $data['email'],
          'password' => $data['psw'],
          'Country' => $zt_ac_countries[$c_key]['code'],
          'campaignId' => 649,
          'subCampaign' => $data['clickid'],
          'currency' => 'USD',
          'Phone' => $data['phone'],
          'a_id' => '4c1784efdcccb',
          'birthday' => "1980-07-21"
        );
        $res_data['debug_string'] = http_build_query($send_data);
        $ch = curl_init($send_url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($send_data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        $res_data['debug'] = $result;
        $api_result = simplexml_load_string($result);
        $res_data['api'] = $api_result;
        curl_close($ch);

        if($api_result->errors && $api_result->errors->error) {
          $error_msg = "";
          foreach($api_result->errors->error as $err) {
            $error_msg .= $err.", ";
          }
          $error_msg = substr($error_msg, 0, -2);

					$push_status = 0;
          $push_response = $error_msg;
          $api_result_bean['status'] = false;
          $api_result_bean['msg'] = $error_msg;

        }

			} else {
				$push_status = 0;
        $push_response = "Your country ".$data['country']." is not valid for sending a lead";
        $api_result_bean['status'] = false;
        $api_result_bean['msg'] = $push_response;
			}

		} else if($brand == 'Keyoption') {

			$zt_ac_countries = array(
        array('country' => 'Spain', 'country_code' => 'es', 'code' => 199)
      );

			// $data['country'] = "Spain";
			$is_legal_county = false;
      $c_key = -1;
      if($data['country'] !== "") {
        foreach($zt_ac_countries as $k => $lc) {
          if($lc['country'] == $data['country'] || $lc['country_code'] == $data['country_code']) {
            $c_key = $k;
            $is_legal_county = true;
            break;
          }
        }
      }

			if($is_legal_county) {

				$send_url = "https://api-spotplatform.keyoption.com/Api";
        $send_data = array(
          'api_username' => 'thebinaryinstitute',
          'api_password' => 'aKj6Rn3hU6',
          'MODULE' => 'Customer',
          'COMMAND' => 'add',
          'FirstName' => $data['fname'],
          'LastName' => $data['lname'],
          'email' => $data['email'],
          'password' => $data['psw'],
					'Phone' => $data['phone'],
          'Country' => $zt_ac_countries[$c_key]['code'],
					'currency' => 'EUR',
					'birthday' => "1980-07-21",
					'gender' => 'male',
					'registrationCountry' => $zt_ac_countries[$c_key]['code'],
          'campaignId' => 116,
          'subCampaign' => 35191,
          'a_id' => 35191,
					'regulateStatus' => 'pending',
					'regulateType' => 1
        );

				$res_data['debug_string'] = http_build_query($send_data);
        $ch = curl_init($send_url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($send_data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        $api_result = simplexml_load_string($result);
        curl_close($ch);

        if($api_result->errors && $api_result->errors->error) {
          $error_msg = "";
          foreach($api_result->errors->error as $err) {
            $error_msg .= $err.", ";
          }
          $error_msg = substr($error_msg, 0, -2);

          $push_status = 0;
          $push_response = $error_msg;
          $api_result_bean['status'] = false;
          $api_result_bean['msg'] = $error_msg;
        }

			} else {
				$push_status = 0;
        $push_response = "Your country ".$data['country']." is not valid for sending a lead";
        $api_result_bean['status'] = false;
        $api_result_bean['msg'] = $push_response;
			}

		} else if($brand == 'Bank of Options') {

      $zt_ac_countries = array(
        array('country' => 'Australia', 'country_code' => 'AU', 'code' => 13),
        array('country' => 'United Kingdom', 'country_code' => 'UK', 'code' => 149),
        array('country' => 'Canada', 'country_code' => 'CA', 'code' => 37),
        array('country' => 'United States', 'country_code' => 'US', 'code' => 62),
        array('country' => 'Netherlands', 'country_code' => 'NL', 'code' => 151)
      );

      $is_legal_county = false;
      $c_key = -1;
      if($data['country'] !== "") {
        foreach($zt_ac_countries as $k => $lc) {
          if($lc['country'] == $data['country'] || $lc['country_code'] == $data['country_code']) {
            $c_key = $k;
            $is_legal_county = true;
            break;
          }
        }
      }

      if($is_legal_county) {

        $hm_api = new HelloMarketsApiBank();
        $hm_data = array(
          'keyword' => $data['clickid'],
          'email' => $data['email'],
          'password' => $data['psw'],
          'password_confirm' => $data['psw'],
          'firstname' => $data['fname'],
          'lastname' => $data['lname'],
          'phone' => $data['phone'],
          'currency' => 'USD',
          'country_id' => $zt_ac_countries[$c_key]['code']
        );
        $hm_lead = $hm_api->Call('create-lead', $hm_data);

        if($hm_lead->error_code == 0) {

        } else {
          $error_msg = $hm_lead->error_message;
          $push_status = 0;
          $push_response = $error_msg;
          $api_result_bean['status'] = false;
          $api_result_bean['msg'] = $error_msg;
        }

      } else {
        $push_status = 0;
        $push_response = "Your country ".$data['country']." is not valid for sending a lead";
        $api_result_bean['status'] = false;
        $api_result_bean['msg'] = $push_response;
      }

    } else if($brand == 'FTrade') {

      $zt_ac_countries = array(
        array('country' => 'Denmark', 'country_code' => 'DK', 'code' => 58),
        array('country' => 'Norway', 'country_code' => 'NO', 'code' => 160),
        array('country' => 'Sweden', 'country_code' => 'SE', 'code' => 205),
        array('country' => 'Finland', 'country_code' => 'FI', 'code' => 72),
        array('country' => 'Germany', 'country_code' => 'DE', 'code' => 80),
        array('country' => 'Netherlands', 'country_code' => 'NL', 'code' => 151),
        array('country' => 'Austria', 'country_code' => 'AT', 'code' => 14),
        array('country' => 'Switzerland', 'country_code' => 'CH', 'code' => 206),
        array('country' => 'Singapore', 'country_code' => 'SG', 'code' => 192),
        array('country' => 'Canada', 'country_code' => 'CA', 'code' => 38),
        array('country' => 'Australia', 'country_code' => 'AU', 'code' => 13),
        array('country' => 'New Zealand', 'country_code' => 'NZ', 'code' => 153)
      );

      $is_legal_county = false;
      $c_key = -1;
      if($data['country'] !== "") {
        foreach($zt_ac_countries as $k => $lc) {
          if($lc['country'] == $data['country']) {
            $c_key = $k;
            $is_legal_county = true;
            break;
          }
        }
      }

      if($is_legal_county) {
        $send_url = "http://api-spotplatform.ftrade.com/Api";
        $send_data = array(
          'api_username' => 'Thebinaryinstitute',
          'api_password' => 'Pz50WBbyz2',
          'MODULE' => 'Customer',
          'COMMAND' => 'add',
          'FirstName' => $data['fname'],
          'LastName' => $data['lname'],
          'email' => $data['email'],
          'password' => $data['psw'],
          'Country' => $zt_ac_countries[$c_key]['code'],
          'campaignId' => 218,
          'subCampaign' => $data['clickid'],
          'currency' => 'USD',
          'Phone' => $data['phone'],
          'a_id' => '4c1784efdcccb',
          'birthday' => "1980-07-21"
        );
        $res_data['debug_string'] = http_build_query($send_data);
        $ch = curl_init($send_url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($send_data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        $api_result = simplexml_load_string($result);
        curl_close($ch);

        if($api_result->errors && $api_result->errors->error) {
          $error_msg = "";
          foreach($api_result->errors->error as $err) {
            $error_msg .= $err.", ";
          }
          $error_msg = substr($error_msg, 0, -2);

          $push_status = 0;
          $push_response = $error_msg;
          $api_result_bean['status'] = false;
          $api_result_bean['msg'] = $error_msg;
        }

      } else {
        $push_status = 0;
        $push_response = "Your country ".$data['country']." is not valid for sending a lead";
        $api_result_bean['status'] = false;
        $api_result_bean['msg'] = $push_response;
      }

    } else if($brand == 'BinaryBrokerZ') {
      $send_url = "https://platform-api.tradesmarter.com/index/register";
      $send_data = array(
        'firstName' => $data['fname'],
        'lastName' => $data['lname'],
        'email' => $data['email'],
        'confirmed' => 1,
        'password' => md5($data['psw']),
        'phone' => $data['phone'],
        'country' => 'be',
        'locale' => 'en_FR',
        'landing' => json_encode(array('a_aid'=>'8b1c1e8a', 'serial'=>'camp1')),
        'lead' => 0
      );
      $ch = curl_init($send_url);
      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $send_data);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_USERPWD, "binarybrzapi:FVj6fccXcE");
      $result = curl_exec($ch);
      curl_close($ch);
      if(is_object($result) && $result->error) {
        $error_msg = "";
        foreach($api_result->errors->error as $err) {
          $error_msg .= $err.", ";
        }
        $error_msg = substr($error_msg, 0, -2);
        $push_status = 0;
        $push_response = $error_msg;
        $api_result_bean['status'] = false;
        $api_result_bean['msg'] = $error_msg;
      }
    } else if($brand == 'OptionLive') {
      $hm_api = new HelloMarketsApi();
      $hm_data = array(
        'keyword' => $data['clickid'],
        'email' => $data['email'],
        'password' => $data['psw'],
        'password_confirm' => $data['psw'],
        'firstname' => $data['fname'],
        'lastname' => $data['lname'],
        'phone' => $data['phone'],
        'currency' => 'USD',
        'country_id' => 66
      );
      $hm_lead = $hm_api->Call('create-lead', $hm_data);

      if($hm_lead->error_code == 0) {
        $hm_ac_countries = array(
          'France'
        );
        $is_legal_county = false;
        $c_key = -1;
        if($data['country'] !== "") {
          foreach($hm_ac_countries as $k => $lc) {
            if($data['country'] == $lc) {
              $c_key = $k;
              $is_legal_county = true;
              break;
            }
          }
        }
        if(!$is_legal_county) {
          $error_msg = "Your country ".$data['country']." is not valid for sending a lead";
          $push_status = 0;
          $push_response = $error_msg;
          $api_result_bean['status'] = false;
          $api_result_bean['msg'] = $error_msg;
        }
      } else {
        $error_msg = $hm_lead->error_message;
        $push_status = 0;
        $push_response = $error_msg;
        $api_result_bean['status'] = false;
        $api_result_bean['msg'] = $error_msg;
      }
    } else if($brand == "OptionStars") {
      $send_url = "https://b2b-api.tradologic.net/v1/affiliate/users";
      $apiUsername = 'starsoded';
      $apiPassword = 'Password1';

      $requestParams = array(
          'affiliateUsername' => $apiUsername,
          'accountId'         => 568,
          'dealId'            => 37563,
          'email'             => $data['email'],
          'phone'             => $data['phone'],
          'userFirstName'     => $data['fname'],
          'userLastName'      => $data['lname'],
          'userPassword'      => $data['psw'],
          'subAffiliateId'    => $data['clickid'],
          'userIpAddress'     => $data['ip']
      );

      ksort($requestParams);
      $requestParams['checksum'] = hash('sha256', implode($requestParams) . $apiPassword);

      $requestBody = json_encode($requestParams);
      $ch = curl_init($send_url);
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $requestBody);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Content-Type: application/json',
          'Content-Length: ' . strlen($requestBody))
      );
      $result = curl_exec($ch);
      $api_result = json_decode($result);
      $res_data['api'] = $api_result;
      curl_close($ch);

      if($api_result->httpStatusCode == 200 || $api_result->httpStatusCode == 201) {

      } else {
        $error_msg = "Creating broker account error:  ".$api_result->messageText;
        $push_status = 0;
        $push_response = $error_msg;
        $api_result_bean['status'] = false;
        $api_result_bean['msg'] = $error_msg;
      }
    } else if($brand == 'TradeRush') {

      $send_url = "http://api.traderush.com/api";
      // $gd_country = 'Canada';
      $is_legal_county = false;
      $c_key = -1;
      if($data['country'] !== "") {
        foreach(self::$ac_countries as $k => $lc) {
          if($lc['country'] == $data['country'] || $lc['country_code'] == $data['country_code']) {
            $c_key = $k;
            $is_legal_county = true;
            break;
          }
        }
      }
      if($is_legal_county) {
        $send_url = "http://api.traderush.com/api";
        $send_data = array(
          'api_username' => 'aff59524',
          'api_password' => '55c356a72b5c9',
          'MODULE' => 'Customer',
          'COMMAND' => 'add',
          'FirstName' => $data['fname'],
          'LastName' => $data['lname'],
          'email' => $data['email'],
          'password' => $data['psw'],
          'Country' => self::$ac_countries[$c_key]['code'],
          'campaignId' => 533,
          'subCampaign' => $data['clickid'],
          'currency' => 'USD',
          'Phone' => $data['phone'],
          'a_id' => '4c1784efdcccb',
          'day' => 15,
          'month' => 10,
          'year' => 1988
        );
        $ch = curl_init($send_url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($send_data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        $api_result = simplexml_load_string($result);
        curl_close($ch);

        if($api_result->errors && $api_result->errors->error) {
          $error_msg = "";
          foreach($api_result->errors->error as $err) {
            $error_msg .= $err.", ";
          }
          $error_msg = substr($error_msg, 0, -2);

          $push_status = 0;
          $push_response = $error_msg;
          $api_result_bean['status'] = false;
          $api_result_bean['msg'] = $error_msg;
        }
      } else {
        $push_status = 0;
        $push_response = "Your country ".$data['country']." is not valid for sending a lead";
        $api_result_bean['status'] = false;
        $api_result_bean['msg'] = $push_response;
      }
    } else if($brand == 'MagnumOptions') {
      $send_url = "http://api-spotplatform.magnumoptions.com/Api";
      // $gd_country = 'Canada';
      $is_legal_county = false;
      $c_key = -1;
      if($data['country'] !== "") {
        foreach(self::$ac_countries as $k => $lc) {
          if($lc['country'] == $data['country'] || $lc['country_code'] == $data['country_code']) {
            $c_key = $k;
            $is_legal_county = true;
            break;
          }
        }
      }
      if($is_legal_county) {
        $send_url = "http://api-spotplatform.magnumoptions.com/Api";
        $send_data = array(
          'api_username' => 'aff59524',
          'api_password' => '559e9a030bb7e',
          'MODULE' => 'Customer',
          'COMMAND' => 'add',
          'FirstName' => $data['fname'],
          'LastName' => $data['lname'],
          'email' => $data['email'],
          'password' => $data['psw'],
          'Country' => self::$ac_countries[$c_key]['code'],
          'campaignId' => 76,
          'subCampaign' => $data['clickid'],
          'currency' => 'USD',
          'Phone' => $data['phone'],
          'a_id' => '4c1784efdcccb',
          'day' => 15,
          'month' => 10,
          'year' => 1988
        );
        $ch = curl_init($send_url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($send_data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        $api_result = simplexml_load_string($result);
        curl_close($ch);

        if($api_result->errors && $api_result->errors->error) {
          $error_msg = "";
          foreach($api_result->errors->error as $err) {
            $error_msg .= $err.", ";
          }
          $error_msg = substr($error_msg, 0, -2);

          $push_status = 0;
          $push_response = $error_msg;
          $api_result_bean['status'] = false;
          $api_result_bean['msg'] = $error_msg;
        }

      } else {
        $push_status = 0;
        $push_response = "Your country ".$data['country']." is not valid for sending a lead";
        $api_result_bean['status'] = false;
        $api_result_bean['msg'] = $push_response;
      }
    } else if($brand == 'Zoom Trader') {
      $send_url = "https://b2b-api.tradologic.net/v1/affiliate/users";
      $apiUsername = 'ofigureszoom';
      $apiPassword = 'Password1';

      $requestParams = array(
          'affiliateUsername' => $apiUsername,
          'accountId'         => 230,
          'dealId'            => 31615,
          'email'             => $data['email'],
          'phone'             => $data['phone'],
          'userFirstName'     => $data['fname'],
          'userLastName'      => $data['lname'],
          'userPassword'      => $data['psw'],
          'subAffiliateId'    => $data['clickid'],
          'userIpAddress'     => $data['ip']
      );

      ksort($requestParams);
      $requestParams['checksum'] = hash('sha256', implode($requestParams) . $apiPassword);

      $requestBody = json_encode($requestParams);
      $ch = curl_init($send_url);
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $requestBody);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Content-Type: application/json',
          'Content-Length: ' . strlen($requestBody))
      );
      $result = curl_exec($ch);
      $api_result = json_decode($result);
      curl_close($ch);

      if($api_result->httpStatusCode == 200 || $api_result->httpStatusCode == 201) {

      } else {
        $push_status = 0;
        $push_response = $api_result->messageText;
        $api_result_bean['status'] = false;
        $api_result_bean['msg'] = $api_result->messageText;
      }

    } else if($brand == 'Swift Option') {
      $send_url = "https://b2b-api.tradologic.net/v1/affiliate/users";
      $apiUsername = 'Ofiguresswift';
      $apiPassword = 'Password1';
      $requestParams = array(
          'affiliateUsername' => $apiUsername,
          'accountId'         => 293,
          'dealId'            => 31520,
          'email'             => $data['email'],
          'phone'             => $data['phone'],
          'userFirstName'     => $data['fname'],
          'userLastName'      => $data['lname'],
          'userPassword'      => $data['psw'],
          'subAffiliateId'    => $data['clickid'],
          'userIpAddress'     => $data['ip']
      );
      ksort($requestParams);
      $requestParams['checksum'] = hash('sha256', implode($requestParams) . $apiPassword);
      $requestBody = json_encode($requestParams);
      $ch = curl_init($send_url);
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $requestBody);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Content-Type: application/json',
          'Content-Length: ' . strlen($requestBody))
      );
      $result = curl_exec($ch);
      $api_result = json_decode($result);
      curl_close($ch);
      if($api_result->httpStatusCode == 200 || $api_result->httpStatusCode == 201) {

      } else {
        $push_status = 0;
        $push_response = $api_result->messageText;
        $api_result_bean['status'] = false;
        $api_result_bean['msg'] = $api_result->messageText;
      }
    } else if($brand == 'CherryTrade') {
      $send_url = "https://api-spotplatform.cherrytrade.com/Api";
      if($data['country'] == 'United States') {
        $send_data = array(
          'api_username' => 'aff2426',
          'api_password' => '552fca58338c6',
          'MODULE' => 'Customer',
          'COMMAND' => 'add',
          'FirstName' => $data['fname'],
          'LastName' => $data['lname'],
          'email' => $data['email'],
          'password' => $data['psw'],
          'Country' => 226,
          'campaignId' => 63,
          'subCampaign' => $data['clickid'],
          'currency' => 'USD',
          'Phone' => $data['phone'],
          'a_id' => '4c1784efdcccb',
          'day' => 15,
          'month' => 10,
          'year' => 1988
        );
        $ch = curl_init($send_url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($send_data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);

        $api_result = simplexml_load_string($result);
        curl_close($ch);

        if($api_result->errors && $api_result->errors->error) {
          $error_msg = "";
          foreach($api_result->errors->error as $err) {
            $error_msg .= $err.", ";
          }
          $error_msg = substr($error_msg, 0, -2);

          $push_status = 0;
          $push_response = $error_msg;
          $api_result_bean['status'] = false;
          $api_result_bean['msg'] = $error_msg;
        }

      } else {
        $push_status = 0;
        $push_response = "Your country ".$data['country']." is not valid for sending a lead";
        $api_result_bean['status'] = false;
        $api_result_bean['msg'] = $push_response;
      }
    } else if($brand == 'OptionsXO') {
      $send_url = "http://54.171.98.181/api/marketeer/customer/registerTrader";
      $send_data = array(
        'affiliateUserName' => 'OdedAbbou',
        'affiliatePassword' => 'Oded12345',
        'userName' => 'user'.$data['clickid'],
        'password' => $data['psw'],
        'firstName' => $data['fname'],
        'lastName' => $data['lname'],
        'phoneNumber' => $data['phone'],
        'email' => $data['email'],
        'subCampaignId' => $data['clickid'],
        'currencyId' => 'USD',
        'countryId' => $data['country_code'],
        'trackingCode' => 1334
      );

      $ch = curl_init($send_url);
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($send_data));
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      $result = curl_exec($ch);
      $api_result = json_decode($result);
      curl_close($ch);

      if($api_result->errorCodes == null) {

      } else {
        $push_status = 0;
        $push_response = $api_result->errorCodes;
        $api_result_bean['status'] = false;
        $api_result_bean['msg'] = $api_result->errorCodes;
      }
    } else if($brand == 'GRO Options') {
      $send_url = "http://api-spotplatform.rboptions.com/Api";
      $send_data = array(
        'api_username' => 'optionfigures',
        'api_password' => '55801b2cf38b9',
        'MODULE' => 'Customer',
        'COMMAND' => 'add',
        'FirstName' => $data['fname'],
        'LastName' => $data['lname'],
        'email' => $data['email'],
        'password' => $data['psw'],
        'campaignId' => 131,
        'subCampaignId' => $data['clickid'],
        'Phone' => $data['phone'],
        'Country' => 225,
        'currency' => 'USD'
      );
      $ch = curl_init($send_url);
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($send_data));
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      $result = curl_exec($ch);
      $api_result = simplexml_load_string($result);
      curl_close($ch);

      if($api_result->errors && $api_result->errors->error) {
        $error_msg = "";
        foreach($api_result->errors->error as $err) {
          $error_msg .= $err.", ";
        }
        $error_msg = substr($error_msg, 0, -2);

        $push_status = 0;
        $push_response = $error_msg;
        $api_result_bean['status'] = false;
        $api_result_bean['msg'] = $error_msg;

      }

    } else if($brand == "OptionRally") {
      $ru_campaign = 2302;
      $ru_subcampaign = "37969_1892506";
      $en_campaign = 2294;
      $en_subcampaign = "37968_1892536";

      $detect_campaign = "";
      $detect_subcampaign = "";
      if($data['country'] == 'Russian Federation') {
        $detect_campaign = $ru_campaign;
        $detect_subcampaign = $ru_subcampaign."_".$unique_clickid;
      } else {
        $detect_campaign = $en_campaign;
        $detect_subcampaign = $en_subcampaign."_".$unique_clickid;
      }

      $send_url = "http://proxy.optionrally.com/api/";
      $send_data = array(
        'api_username' => 'odedabbou',
        'api_password' => 'hb69kl2x',
        'MODULE' => 'Customer',
        'COMMAND' => 'add',
        'FirstName' => $data['fname'],
        'LastName' => $data['lname'],
        'email' => $data['email'],
        'Phone' => $data['phone'],
        'campaignId' => $detect_campaign,
        'subCampaign' => $detect_subcampaign,
        'Country' => $data['country'],
        'birthday' => '1970-01-21',
        'gender' => 'male',
        'password' => $data['psw'],
        'currency' => 'USD'
      );
      $options = array(
          'http' => array(
              'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
              'method'  => 'POST',
              'content' => http_build_query($send_data),
          ),
      );
      $context  = stream_context_create($options);
      $result = file_get_contents($send_url, false, $context);
      $api_result = simplexml_load_string($result);

      if($api_result->errors && $api_result->errors->error) {
        $error_msg = "";
        foreach($api_result->errors->error as $err) {
          $error_msg .= $err.", ";
        }
        $error_msg = substr($error_msg, 0, -2);
        $push_status = 0;
        $push_response = $error_msg;

        $api_result_bean['status'] = false;
        $api_result_bean['msg'] = $error_msg;
      }
    } else if($brand == 'EZTrader') {
      $send_url = "https://www.EZTrader.com/handlers/AffiliateUserRegistration.ashx/";
      $send_data = array(
        'AffiliateID' => "7742",
        'AffiliateName' => "eleadstudio",
        'AffiliatePassword' => "12345678",
        'FirstName' => $data['fname'],
        'LastName' => $data['lname'],
        'Email' => $data['email'],
        'Country' => $data['gd_country'],
        'StreetAddress' => 'Default Street',
        'City' => 'Default City',
        'State' => '',
        'Zip' => '374646',
        'Phone' => $data['phone'],
        'BirthDate' => '1970-01-21',
        'AccountCurrency' => '1',
        'Password' => $data['psw'],
        'UserIsOver18' => 'true',
        'CultureName' => 'en-US',
        'UserIP' => $data['ip']
      );
      $options = array(
          'http' => array(
              'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
              'method'  => 'POST',
              'content' => http_build_query($send_data),
          ),
      );
      $context  = stream_context_create($options);
      $result = file_get_contents($send_url, false, $context);
      $api_result = json_decode($result);
      if($api_result->Status == 'Error') {
        $error_msg = "";
        foreach($api_result->Errors as $err) {
          $error_msg .= $err.", ";
        }
        $error_msg = substr($error_msg, 0, -2);
        $push_status = 0;
        $push_response = $error_msg;

        $api_result_bean['status'] = false;
        $api_result_bean['msg'] = $error_msg;
      }
    } else if($brand == 'uBinary') {
      $send_ubinary_url = "http://api.ubinary.com/trading/affiliate/35578/user/register?data=";
      $send_ubinary_data = array(
        'FirstName' => $data['fname'],
        'LastName' => $data['lname'],
        'PhoneNumber' => $data['phone'],
        'Email' => $data['email'],
        'LanguageCode' => "en",
        'Tlid' => '111222333444555666',
        'Password' => $data['psw'],
        'CountryCode' => $data['country_code'],
        'FromIp' => $data['ip'],
        'Ctag' => 'user.JKFDSIE3243-4V432'
      );
      $send_ubinary_data = urlencode(json_encode($send_ubinary_data));
      $send_ubinary_url = $send_ubinary_url.$send_ubinary_data;
      $ubinary_api_call = file_get_contents($send_ubinary_url);
      $api_result = json_decode($ubinary_api_call);
      if($api_result->ErrorMessage !== '') {
        $push_status = 0;
        $push_response = $error_msg;

        $api_result_bean['status'] = false;
        $api_result_bean['msg'] = $api_result->ErrorMessage;
      }
    }

    $db_data['status'] = $push_status;
    $db_data['response'] = $push_response;
    $res_data['db_data'] = $db_data;
    $res_data['api'] = $api_result_bean;

    return $res_data;
  }

}
