<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Acl extends Model {

	/**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'acl';

  /**
   * Table primary key.
   *
   * @var array
   */
  protected $primary_key = 'id';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
  	'user_id',
  	'level_id',
    'level_lbl',
  	'status',
		'payment_id'
  ];

  public function __construct($data = null) {
    if($data) {
      parent::__construct($data);
    }
  }

}
