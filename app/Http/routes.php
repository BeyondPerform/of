<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('pages.home');
});


Route::group(['middleware' => 'auth.admin'], function () {
  Route::get('/admin', 'AdminController@index');

  Route::get('/admin/clickbank', 'AdminController@clickbank');
  Route::get('/admin/bleads', 'AdminController@bleads');
  Route::get('/admin/cleads', 'AdminController@cleads');
  Route::get('/admin/jvpartners', 'AdminController@jvpartners');
  Route::get('/admin/autopushreports', 'AdminController@autopushreports');
  Route::post('/admin/storebraintreepushes', 'AdminController@storebraintreepushes');
  Route::post('/admin/storepushes', 'AdminController@storepushes');
  Route::post('/admin/getpushes', 'AdminController@getpushes');
  Route::get('/admin/exportcleads', 'AdminController@exportcleads');
  Route::get('/admin/exportbleads', 'AdminController@exportbleads');
  Route::get('/admin/exportjv', 'AdminController@exportjv');
  Route::get('/admin/exportclickbank', 'AdminController@exportclickbank');
});

Route::group(['middleware' => 'auth'], function () {
  Route::get('/calculator/home', 'CalculatorController@index');

  Route::get('/calculator/currencies', 'CalculatorController@currencies');
  Route::get('/calculator/commodities', 'CalculatorController@commodities');
  Route::get('/calculator/indices', 'CalculatorController@indices');
  Route::get('/calculator/brokers', 'CalculatorController@brokers');
  Route::get('/calculator/userguide', 'CalculatorController@userguide');
  Route::get('/calculator/rbrokers', 'CalculatorController@rbrokers');
  Route::get('/calculator/promotions', 'CalculatorController@promotions');

  Route::get('/calculator/payment', 'CalculatorController@payment');
  Route::post('/calculator/pay', 'CalculatorController@pay');
});

Route::group(['middleware' => 'guest'], function () {
  Route::get('/adminlogin', function () {
    return view('pages.auth.adminlogin');
  });
  Route::get('/signin', function () {
    return view('pages.auth.signin');
  });
  Route::post('/subscribe', 'Auth\AuthController@sbmsignupuser');
  Route::post('/signin', 'Auth\AuthController@signIn');

  Route::get('password/email', 'Auth\PasswordController@getEmail');
  Route::post('password/email', 'Auth\PasswordController@postEmail');
  Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
  Route::post('password/reset', 'Auth\PasswordController@postReset');
});

Route::get('/signup_success', function () {
  return view('pages.signup_success');
});
Route::get('/calc_payment_success', function () {
  return view('pages.calc_payment_success');
});

Route::get('/logout', 'Auth\AuthController@logout');

Route::get('access-denied', function(){
  return view('errors.access-denied');
});

Route::resource('leads', 'LeadsController');

Route::get('/terms', function () {
    return view('pages.terms');
});
Route::get('/privacy', function () {
    return view('pages.privacy');
});
Route::get('/risk_disclaimer', function () {
    return view('pages.risk_disclaimer');
});

Route::get('/partner', function () {
  return view('pages.jv');
});
Route::post('/partners/jvpartner_applications', 'JvpartnerController@jvpartner_applications');

Route::get('/crons/cj_signin_emails', 'Crons\CronsController@cj_signin_emails');
Route::get('/crons/cj_signin_brand_leads', 'Crons\CronsController@cj_signin_brand_leads');
