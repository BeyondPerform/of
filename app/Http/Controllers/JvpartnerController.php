<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Jvpartners;
use App\Helpers;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Mail;

class JvpartnerController extends Controller {

  public function jvpartner_applications(Request $request) {
    $res_data = array(
      'status' => false,
      'msg' => "",
      'bean' => null,
      'mailer' => null
    );
    $data = $request->all();

    $ip = $_SERVER['REMOTE_ADDR'];

    $gd_detect = @geoip_country_name_by_name($ip);
    $country = "";
    $country_code = "";
    if($gd_detect) {
      $country = $gd_detect;
      $country_code = @geoip_country_code_by_name($ip);
    }

    $db_data = array(
      'clicksure_id' => $data['clicksure_id'],
      'fname' => $data['fname'],
      'lname' => $data['lname'],
      'email' => $data['email'],
      'skype_id' => $data['skype_id'],
      'ip' => $ip,
      'country' => $country,
      'country_code' => $country_code
    );

    $sql_check_res = Jvpartners::where('clicksure_id', $db_data['clicksure_id'])->where('email', $db_data['email'])->get();
    if(count($sql_check_res) > 0) {
      $res_data['bean'] = $sql_check_res[0];
    } else {
      $res_data['bean'] = $db_data;
      $lead = new Jvpartners($db_data);
      $lead->save();

      $email_data = array();
      $email = $data['email'];

      $contact_params = array(
        array(
          'email' => $email,
          'first_name' => $data['fname'],
          'skype_id' => $data['skype_id']
        )
      );
      $sg = Helpers::_contactApiAssign('419117', $contact_params);

      $partner_subs = array(
        '-fname-' => array($data['fname'])
      );
      $jv_mailer = Helpers::_sgMailer("7c4e184a-c9a7-4b0e-9d2a-690a64489578", $partner_subs, $email, $data['fname'], "OptionFigures - JV/Partners");
      $res_data['mailer'] = $jv_mailer;

    }
    $res_data['status'] = true;

    echo json_encode($res_data);
  }

}
