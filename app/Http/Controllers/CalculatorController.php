<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Acl;
use App\Payments;
use App\Helpers;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Auth;

use Braintree;
use Braintree\Configuration;
use Braintree_Configuration;
use Braintree_ClientToken;
use Braintree_Customer;
use Braintree_Transaction;
use Braintree_Subscription;

class CalculatorController extends Controller {

  /**
   * Create a new password controller instance.
   *
   * @return void
   */
  public function __construct() {
    $this->middleware('auth');
  }

  private function _canAccess() {
    $status = true;
    $user = Auth::user();
    $payment_check = Acl::where('user_id', $user->id)
                          ->where('level_id', 1)
                          ->where('status', 1)
                          ->get();
    if(count($payment_check) === 0) {
      $status = false;
    }
    return $status;
  }

  public function index() {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    // Set HTTP Auth credentials
    curl_setopt($ch, CURLOPT_USERPWD, 'tsdemoapi:YkqCea2R');
    $params = array (
      // instruments is a json encoded array of the instrument id's we wish to get history of:
      'instruments' => json_encode(array(1)),
      'lite' => 1
    );
    curl_setopt($ch, CURLOPT_URL, 'https://platform-api.tradesmarter.com/instrument/history');
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    $chartHistory = curl_exec($ch);

    $dash_chart_1 = $chartHistory;
    $dash_chart_2 = $chartHistory;
    $dash_chart_3 = $chartHistory;
    return view('pages.calc.home', ['dash_chart_1' => $dash_chart_1, 'dash_chart_2' => $dash_chart_2, 'dash_chart_3' => $dash_chart_3]);
  }

  public function currencies() {
    if($this->_canAccess()) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      // Set HTTP Auth credentials
      curl_setopt($ch, CURLOPT_USERPWD, 'tsdemoapi:YkqCea2R');
      $params = array (
        // instruments is a json encoded array of the instrument id's we wish to get history of:
        'instruments' => json_encode(array(1)),
        'lite' => 1
      );
      curl_setopt($ch, CURLOPT_URL, 'https://platform-api.tradesmarter.com/instrument/history');
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
      $chartHistory = curl_exec($ch);

      $curr_chart_1 = $chartHistory;
      $curr_chart_2 = $chartHistory;
      $curr_chart_3 = $chartHistory;
      $curr_chart_4 = $chartHistory;

      return view('pages.calc.currencies', [
        'curr_chart_1' => $curr_chart_1,
        'curr_chart_2' => $curr_chart_2,
        'curr_chart_3' => $curr_chart_3,
        'curr_chart_4' => $curr_chart_4
      ]);
    } else {
      return redirect("/calculator/payment");
    }
  }

  public function commodities() {
    if($this->_canAccess()) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      // Set HTTP Auth credentials
      curl_setopt($ch, CURLOPT_USERPWD, 'tsdemoapi:YkqCea2R');
      $params = array (
        // instruments is a json encoded array of the instrument id's we wish to get history of:
        'instruments' => json_encode(array(1)),
        'lite' => 1
      );
      curl_setopt($ch, CURLOPT_URL, 'https://platform-api.tradesmarter.com/instrument/history');
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
      $chartHistory = curl_exec($ch);

      $comm_chart_1 = $chartHistory;
      $comm_chart_2 = $chartHistory;
      $comm_chart_3 = $chartHistory;
      $comm_chart_4 = $chartHistory;
      $comm_chart_5 = $chartHistory;
      $comm_chart_6 = $chartHistory;

      return view('pages.calc.commodities', [
        'comm_chart_1' => $comm_chart_1,
        'comm_chart_2' => $comm_chart_2,
        'comm_chart_3' => $comm_chart_3,
        'comm_chart_4' => $comm_chart_4,
        'comm_chart_5' => $comm_chart_5,
        'comm_chart_6' => $comm_chart_6
      ]);
    } else {
      return redirect("/calculator/payment");
    }
  }

  public function indices() {
    if($this->_canAccess()) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      // Set HTTP Auth credentials
      curl_setopt($ch, CURLOPT_USERPWD, 'tsdemoapi:YkqCea2R');
      $params = array (
        // instruments is a json encoded array of the instrument id's we wish to get history of:
        'instruments' => json_encode(array(1)),
        'lite' => 1
      );
      curl_setopt($ch, CURLOPT_URL, 'https://platform-api.tradesmarter.com/instrument/history');
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
      $chartHistory = curl_exec($ch);

      $indices_chart_1 = $chartHistory;
      $indices_chart_2 = $chartHistory;
      $indices_chart_3 = $chartHistory;
      $indices_chart_4 = $chartHistory;
      $indices_chart_5 = $chartHistory;
      $indices_chart_6 = $chartHistory;
      $indices_chart_7 = $chartHistory;

      return view('pages.calc.indices', [
        'indices_chart_1' => $indices_chart_1,
        'indices_chart_2' => $indices_chart_2,
        'indices_chart_3' => $indices_chart_3,
        'indices_chart_4' => $indices_chart_4,
        'indices_chart_5' => $indices_chart_5,
        'indices_chart_6' => $indices_chart_6,
        'indices_chart_7' => $indices_chart_7
      ]);
    } else {
      return redirect("/calculator/payment");
    }
  }

  public function brokers() {
    if($this->_canAccess()) {
      return view('pages.calc.brokers');
    } else {
      return redirect("/calculator/payment");
    }
  }

  public function userguide() {
    return view('pages.calc.userguide');
  }

  public function rbrokers() {
    if($this->_canAccess()) {
      return view('pages.calc.rbrokers');
    } else {
      return redirect("/calculator/payment");
    }
  }

  public function promotions() {
    return view('pages.calc.promotions');
  }

  public function payment() {
    if(!$this->_canAccess()) {
      // Braintree_Configuration::environment('sandbox');
      // Braintree_Configuration::merchantId('fzrbrgc2r5jrtn5c');
      // Braintree_Configuration::publicKey('bsp5v93z43fyzqtp');
      // Braintree_Configuration::privateKey('ef7be1beb162507e1bcca5f0e8323353');

      Braintree_Configuration::environment('production');
      Braintree_Configuration::merchantId('66hv665frvgmymp3');
      Braintree_Configuration::publicKey('fyj255fcdxwmtdhg');
      Braintree_Configuration::privateKey('6b420335a01139bcedfea169c5bc6719');

      $clientToken = Braintree_ClientToken::generate();
      $auth_object = Auth::user();
      $auth_id = $auth_object->id;
      $auth_user = User::where('id', $auth_id)->first();
      return view('pages.calc.payment', [ "client_token" => $clientToken, "user" =>  $auth_user ]);
    } else {
      return redirect('/calculator/home');
    }
  }

  public function pay(Request $request) {
    $res_data = array(
      'status' => false,
      'msg' => '',
      'debug' => null,
      'is_payment_error' => false,
      'payment_errors' => null
    );
    if(!$this->_canAccess()) {
      // Braintree_Configuration::environment('sandbox');
      // Braintree_Configuration::merchantId('fzrbrgc2r5jrtn5c');
      // Braintree_Configuration::publicKey('bsp5v93z43fyzqtp');
      // Braintree_Configuration::privateKey('ef7be1beb162507e1bcca5f0e8323353');

      Braintree_Configuration::environment('production');
      Braintree_Configuration::merchantId('66hv665frvgmymp3');
      Braintree_Configuration::publicKey('fyj255fcdxwmtdhg');
      Braintree_Configuration::privateKey('6b420335a01139bcedfea169c5bc6719');

      $data = $request->all();
      $auth_object = Auth::user();
      $auth_id = $auth_object->id;
      $auth_user = User::where('id', $auth_id)->first();

      $payment_errors = array();
      // ==== create customer and payment method (start)
      $paymentMethodToken = '';
      $customerId = '';
      $customer = Braintree_Customer::create([
        'firstName' => $auth_user->fname,
        'lastName' => $auth_user->lname,
        'email' => $auth_user->email,
        'phone' => $auth_user->phone,
        'paymentMethodNonce' => $data['nonce']
      ]);
      if($customer->success) {
        $customerId = $customer->customer->id;
        $paymentMethodToken = $customer->customer->paymentMethods[0]->token;
        $res_data['customer_id'] = $customerId;
        $res_data['paymentMethodToken'] = $paymentMethodToken;

        $pay_amount = "1";
        $pay_result = Braintree_Transaction::sale(
          [
            'customerId' => $customerId,
            'amount' => $pay_amount,
            'descriptor' => [
                'name' => 'OFS*section'
            ],
            'options' => [
              'submitForSettlement' => True
            ]
          ]
        );
        if(!$pay_result->success) {
          $res_data['is_payment_error'] = true;
          foreach($pay_result->errors->deepAll() as $pay_error) {
            $payment_errors[] = $pay_error->message;
          }
        }
      } else {
        $res_data['is_payment_error'] = true;
        foreach($customer->errors->deepAll() as $error) {
          $payment_errors[] = $error->message;
        }
      }
      // === create subscription
      if(!$res_data['is_payment_error']) {
        $subscription_result = Braintree_Subscription::create([
          'paymentMethodToken' => $paymentMethodToken,
          'planId' => 'of_subscription_plan'
          // 'planId' => 'test_plan'
        ]);
        $res_data['subscription_result'] = $subscription_result;
        if($subscription_result->success) {
          $res_data['subscription_result'] = $subscription_result;
        } else {
          $res_data['is_payment_error'] = true;
          foreach($subscription_result->errors->deepAll() as $error) {
            $payment_errors[] = $error->message;
          }
        }
      }
      $res_data['payment_errors'] = $payment_errors;

      $status = 1;
      $payment_details = "";
      if($res_data['is_payment_error']) {
        $status = 0;
        $payment_details = implode("::", $res_data['payment_errors']);
      }
      $payment_insert = array(
        'status' => $status,
        'payment_details' => $payment_details
      );
      $payment = new Payments($payment_insert);
      $payment->save();
      $save_payment_id = $payment->id;

      $acl_insert = array(
        'user_id' => $auth_id,
        'level_id' => 1,
        'level_lbl' => "Calculator access",
        'status' => $status,
        'payment_id' => $save_payment_id
      );
      $acl = new Acl($acl_insert);
      $acl->save();

      $contact_params = array(
        array(
          'email' => $auth_user->email,
          'first_name' => $auth_user->fname,
          'last_name' => $auth_user->lname,
          'phone_number' => $auth_user->phone
        )
      );
      $sg = Helpers::_contactApiAssign('419109', $contact_params);

    } else {
      $res_data['msg'] = 'No access';
    }

    echo json_encode($res_data);
  }

}
