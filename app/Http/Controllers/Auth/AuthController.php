<?php

namespace App\Http\Controllers\Auth;

use Log;
use App\Brands;
use App\CjSigninBrandLeads;
use App\User;
use App\Roles;
use App\CjSigninEmails;
use App\Helpers;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Session;
use Mail;

use Braintree;
use Braintree\Configuration;
use Braintree_Configuration;
use Braintree_ClientToken;
use Braintree_Customer;
use Braintree_Transaction;

use SendGrid;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $redirectTo = '/calculator/home';

    // public function __construct() {
    //   $this->middleware('auth.user');
    // }

    /**
     * Sign in process.
     *
     * @return void
     */
    public function signIn(Request $request) {
      $this->validate($request, [
        'email' => 'required', 'password' => 'required',
      ]);
      $credentials = $this->getCredentials($request);
      if (Auth::attempt($credentials, true)) {
          if(Auth::user()->is(Roles::ADMIN)){
            $this->redirectTo = '/admin';
          }
          return $this->handleUserWasAuthenticated($request, false);
      }
      return redirect('/signin')
          ->withInput($request->only('email', 'remember'))
          ->withErrors([
              'email' => $this->getFailedLoginMessage(),
      ]);
    }

    /**
     * Logout.
     *
     * @return void
     */
    public function logout()
    {
        Auth::logout();
        return redirect('/signin');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email'            => 'required|email|max:255|unique:users',
            'password'              => 'required|confirmed|min:6',
            'password_confirmation' => 'required|min:3'
        ]);
    }

     /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function getCredentials(Request $request)
    {
        return $request->only('email', 'password');
    }

    public function sbmgeneratebraintoken(Request $request) {
      $res_data = array(
        'status' => false,
        'msg' => '',
        'client_token' => ''
      );

      // Braintree_Configuration::environment('sandbox');
      // Braintree_Configuration::merchantId('fzrbrgc2r5jrtn5c');
      // Braintree_Configuration::publicKey('bsp5v93z43fyzqtp');
      // Braintree_Configuration::privateKey('ef7be1beb162507e1bcca5f0e8323353');

      Braintree_Configuration::environment('production');
      Braintree_Configuration::merchantId('66hv665frvgmymp3');
      Braintree_Configuration::publicKey('fyj255fcdxwmtdhg');
      Braintree_Configuration::privateKey('6b420335a01139bcedfea169c5bc6719');

      $clientToken = Braintree_ClientToken::generate();
      $res_data['client_token'] = $clientToken;
      $res_data['status'] = true;
      echo json_encode($res_data);
    }

    public function sbmsignupuser(Request $request) {
      $data = $request->all();
      $res_data = array(
        'status' => false,
        'msg' => '',
        'error_code' => 0
      );

      $unique_clickid = uniqid();
      $ip = $_SERVER['REMOTE_ADDR'];
      $fname = $data['fname'];
      $lname = $data['lname'];
      $email = $data['email'];
      $psw = $data['password'];
      $phone = $data['phone'];
      $gd_detect = @geoip_country_name_by_name($ip);
      $country = "";
      $country_code = "";
      if($gd_detect) {
        $gd_country = $gd_detect;
        $gd_country_code = @geoip_country_code_by_name($ip);
      }

      $user_acl_check = User::where('email', $email)->get();
      if(count($user_acl_check) > 0) {
        $res_data['msg'] = 'Already successfully subscribed under this email';
        $res_data['error_code'] = 2;
      } else {
        $user_push_data = array(
          'fname' => $fname,
          'lname' => $lname,
          'email' => $email,
          'password' => bcrypt($psw),
          'phone' => $phone,
          'country' => $country,
          'country_code' => $country_code,
          'ip' => $ip,
          'lead_id' => uniqid(),
          'role_id' => 1
        );

        $user_lead = new User($user_push_data);
        $user_lead->save();
        $save_user_id = $user_lead->id;

        // === put cronjobs emals queue (start)
        for ($i = 1; $i <= 7; $i++) {
          $schedule_date = "";
          switch ($i) {
              case 1:
                  $schedule_date = date('Y-m-d H:i:s', strtotime("+24 hours", time()));
                  // $schedule_date = date('Y-m-d H:i:s', strtotime("+10 minutes", time()));
                  break;
              case 2:
                  $schedule_date = date('Y-m-d H:i:s', strtotime("+48 hours", time()));
                  // $schedule_date = date('Y-m-d H:i:s', strtotime("+20 minutes", time()));
                  break;
              case 3:
                  $schedule_date = date('Y-m-d H:i:s', strtotime("+72 hours", time()));
                  // $schedule_date = date('Y-m-d H:i:s', strtotime("+30 minutes", time()));
                  break;
              case 4:
                  $schedule_date = date('Y-m-d H:i:s', strtotime("+96 hours", time()));
                  // $schedule_date = date('Y-m-d H:i:s', strtotime("+40 minutes", time()));
                  break;
              case 5:
                  $schedule_date = date('Y-m-d H:i:s', strtotime("+120 hours", time()));
                  // $schedule_date = date('Y-m-d H:i:s', strtotime("+50 minutes", time()));
                  break;
              case 6:
                  $schedule_date = date('Y-m-d H:i:s', strtotime("+144 hours", time()));
                  // $schedule_date = date('Y-m-d H:i:s', strtotime("+50 minutes", time()));
                  break;
              case 7:
                  $schedule_date = date('Y-m-d H:i:s', strtotime("+168 hours", time()));
                  // $schedule_date = date('Y-m-d H:i:s', strtotime("+50 minutes", time()));
                  break;
          }
          $cse_push_data = array(
            'user_id' => $save_user_id,
            'mail_id' => $i,
            'schedule_date' => $schedule_date
          );
          $cj_signin_emails = new CjSigninEmails($cse_push_data);
          $cj_signin_emails->save();
        }
        // === put cronjobs emals queue (end)

        // === put cronjobs brands leads queue (start)
        $brands = Brands::orderBy('created_at', 'DESC')->get();
        $schedule_ld_date = date('Y-m-d H:i:s', strtotime("+192 hours", time()));
        foreach($brands as $brand) {
          $csbr_leads_data = array(
            "user_id" => $save_user_id,
            "brand" => $brand['brand'],
            "schedule_date" => $schedule_ld_date
          );
          $cj_lead_push = new CjSigninBrandLeads($csbr_leads_data);
          $cj_lead_push->save();
        }
        // === put cronjobs brands leads queue (end)

        $contact_params = array(
          array(
            'email' => $email,
            'first_name' => $fname,
            'last_name' => $lname,
            'phone_number' => $phone
          )
        );
        $sg = Helpers::_contactApiAssign('419104', $contact_params);

        $login_mailer_subs = array(
          '-fname-' => array($fname),
          '-email-' => array($email),
          '-psw-' => array($psw)
        );
        $login_mailer = Helpers::_sgMailer("db6665c8-d992-433d-9f23-7bac073f1810", $login_mailer_subs, $email, $fname, "OptionFigures Membership - Login Information");

        $mailer_subs = array(
          '-fname-' => array($fname)
        );
        $mailer = Helpers::_sgMailer("ab820a3b-84dc-427f-bdb9-49c2bab5f3b8", $mailer_subs, $email, $fname, "OptionFigures Membership - Important Information");

        $email_w = "cb@the-binary-institute.com";
        // $email_w = "ishulgin8@gmail.com";
        $email_data_w = array(
          'fname' => $fname,
          'lname' => $lname,
          'email' => $email,
          'psw' => $psw,
          'phone' => $phone
        );
        Mail::send('emails.basic_regi', $email_data_w, function($message) use ($email_w) {
            $message->from('support@option-figures.com', 'OptionFigures');
            $message->to($email_w)->subject('OptionFigures - New User');
        });

        $credentials = $this->getCredentials($request);
        $auth_attempt = Auth::attempt($credentials, true);

        $res_data['status'] = true;
        $res_data['msg'] = 'User is successfully saved';
      }

      echo json_encode($res_data);
    }

}
