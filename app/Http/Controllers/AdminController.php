<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\CjSigninBrandLeads;
use App\Leads;
use App\CbLeads;
use App\Pushes;
use App\Csvlogs;
use App\Jvpartners;
use App\User;
use App\Acl;
use App\Clickbank;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Excel;

class AdminController extends Controller {

  /**
   * Create a new password controller instance.
   *
   * @return void
   */
  public function __construct() {
    $this->middleware('auth.admin');

    $leads = Leads::orderBy('created_at', 'DESC')->get();
    view()->share('leads', $leads);

    $cb_leads_stack = array();
    $cb_leads = User::where('role_id', 1)->orderBy('created_at', 'DESC')->get();
    foreach($cb_leads as $k => $clead) {
      $mid = array(
        'info' => $clead,
        'payed' => false
      );
      $user_acl = Acl::where('user_id', $clead['id'])->get();
      if(count($user_acl) > 0) {
        $mid['payed'] = true;
      }
      $cb_leads_stack[] = $mid;
    }
    view()->share('cb_leads', $cb_leads_stack);

    $clickbank_leads_stack = array();
    $clickbank_leads = Clickbank::orderBy('created_at', 'DESC')->get();
    view()->share('clickbank_leads', $clickbank_leads);

    $jv_partners = Jvpartners::orderBy('created_at', 'DESC')->get();
    view()->share('jv_partners', $jv_partners);

    $autopushreports = CjSigninBrandLeads::orderBy('created_at', 'DESC')->get();
    $apr_stack = array();
    foreach($autopushreports as $auto_r) {
        $mid = array(
          'report' => $auto_r,
          'lead' => null
        );
        $q_user = User::where('id', $auto_r['user_id'])->first();
	      if($q_user) {
					$mid['lead'] = $q_user;
        }
        $apr_stack[] = $mid;
    }
    view()->share('autopushreports', $apr_stack);

    $brands = array(
      "BinaryBrokerZ",
      "OptionLive",
      "Keyoption",
      "OneTwoTrade",
      "Bank of Options",
      "FTrade",
      "TradeRush",
      "OptionsXO",
      "GRO Options",
      "Zoom Trader",
      "Swift Option",
      "MagnumOptions",
      "uBinary",
      "CherryTrade",
      "EZTrader",
      "OptionRally",
      'Option500',
      'TradeSolid'
    );
    view()->share('brands', $brands);
  }

  public function exportclickbank(Request $request) {
    $unique_clickid = uniqid()."_clickbank";
    $clickbank_leads = Clickbank::orderBy('created_at', 'DESC')->get();
    Excel::create($unique_clickid, function($excel) use($clickbank_leads) {
        $excel->sheet('sheet', function($sheet) use($clickbank_leads) {
          $sheet->fromArray($clickbank_leads);
        });
    })->store('xls')->export('xls');
    return Redirect::back();
  }

  public function exportcleads(Request $request) {
    $unique_clickid = uniqid()."_cbleads";
    $cb_leads = CbLeads::orderBy('created_at', 'DESC')->get();
    Excel::create($unique_clickid, function($excel) use($cb_leads) {
        $excel->sheet('sheet', function($sheet) use($cb_leads) {
          $sheet->fromArray($cb_leads);
        });
    })->store('xls')->export('xls');
    return Redirect::back();
  }

  public function exportbleads(Request $request) {
    $unique_clickid = uniqid()."_bleads";
    $leads = Leads::orderBy('created_at', 'DESC')->get();
    Excel::create($unique_clickid, function($excel) use($leads) {
        $excel->sheet('sheet', function($sheet) use($leads) {
          $sheet->fromArray($leads);
        });
    })->store('xls')->export('xls');
    return Redirect::back();
  }

  public function exportjv(Request $request) {
    $unique_clickid = uniqid()."_jv";
    $jv_partners = Jvpartners::orderBy('created_at', 'DESC')->get();
    Excel::create($unique_clickid, function($excel) use($jv_partners) {
        $excel->sheet('sheet', function($sheet) use($jv_partners) {
          $sheet->fromArray($jv_partners);
        });
    })->store('xls')->export('xls');
    return Redirect::back();
  }

  public function index() {
    return view('admin.pages.index');
  }

  public function bleads() {
    return view('admin.pages.bleads');
  }

  public function clickbank() {
    return view('admin.pages.clickbank');
  }

  public function cleads() {
    return view('admin.pages.cleads');
  }

  public function autopushreports() {
    return view('admin.pages.autopushreports');
  }

  public function jvpartners() {
    return view('admin.pages.jvpartners');
  }

  public function storepushes(Request $request) {
    $res_data = array(
      'status' => false,
      'msg' => '',
      'api_results' => null
    );
    $data = $request->all();
    $lead_id = $data['lead_id'];
    $push_brands = $data['push_brands'];
    $lead = Leads::where('id', $lead_id)->get();
    if(count($lead) > 0) {
      $lead_bean = $lead[0];
      $push_brands_stack = array();
      if($push_brands !== '') {
        $push_brands_stack = explode(",", $push_brands);
        foreach($push_brands_stack as $brand) {
          $push_s_res = Pushes::pushCurlRequestSender($lead_bean, $brand);
          $push = new Pushes($push_s_res['db_data']);
          $push->save();
          $res_data['api_results'][] = $push_s_res['api'];
        }
        $res_data['status'] = true;
      } else {
        $res_data['msg'] = "At least one brand must be selected";
      }
    } else {
      $res_data['msg'] = "Lead is not found";
    }
    echo json_encode($res_data);
  }

  public function getpushes(Request $request) {
    $data = $request->all();
    $lead_id = $data['lead_id'];
    $push = Pushes::where('lead_id', $lead_id)->get();
    return view('admin.pages.getpushes', ['push' => $push]);
  }

  public function storebraintreepushes(Request $request) {
    $res_data = array(
      'status' => false,
      'msg' => '',
      'api_results' => null
    );
    $data = $request->all();
    $lead_id = $data['lead_id'];
    $push_brands = $data['push_brands'];
    $lead = User::where('id', $lead_id)->get();
    if(count($lead) > 0) {
      $lead_bean = $lead[0];
      $push_brands_stack = array();
      if($push_brands !== '') {
        $push_brands_stack = explode(",", $push_brands);
        foreach($push_brands_stack as $brand) {
          $push_s_res = Pushes::pushCurlRequestSender($lead_bean, $brand, 'braintree');
          $push = new Pushes($push_s_res['db_data']);
          $push->save();
          $res_data['api_results'][] = $push_s_res['api'];
        }
        $res_data['status'] = true;
      } else {
        $res_data['msg'] = "At least one brand must be selected";
      }
    } else {
      $res_data['msg'] = "Lead is not found";
    }
    echo json_encode($res_data);
  }

}
