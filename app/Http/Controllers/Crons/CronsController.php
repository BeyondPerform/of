<?php

namespace App\Http\Controllers\Crons;

use Illuminate\Http\Request;

use App\CjSigninEmails;
use App\CjSigninBrandLeads;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CronsController extends Controller {

  public function cj_signin_emails(Request $request) {
    $queue_sender = CjSigninEmails::startQueueSenderCheck();
    exit;
  }

  public function cj_signin_brand_leads(Request $request) {
    $queue_sender = CjSigninBrandLeads::startQueueSenderLeads();
    exit;
  }

}
