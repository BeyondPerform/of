<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Leads;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class LeadsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $ip = $_SERVER['REMOTE_ADDR'];
        $unique_clickid = uniqid();

        $gd_detect = @geoip_country_name_by_name($ip);
        $country = "";
        $country_code = "";
        if($gd_detect) {
          $country = $gd_detect;
          $country_code = @geoip_country_code_by_name($ip);
        }
        $data['country']      = $country;
        $data['country_code'] = $country_code;
        $data['ip']           = $ip;
        $data['clickid']      = $unique_clickid;
        $data['brand']        = $data['broker_name'];

        $leads_s_res = Leads::leadCurlRequestSender($data);
        $lead = new Leads($leads_s_res['db_data']);
        $lead->save();
        echo json_encode($leads_s_res);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
