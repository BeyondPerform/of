<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\HelloMarketsApi as HelloMarketsApi;
use App\HelloMarketsApiBank as HelloMarketsApiBank;
use App\Country;

class Helpers extends Model {

  // public static function _sgMailerPartner($template_id, $send_email, $toname, $subject) {
  //   $sendgrid_key = "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";
  //   $js = array(
  //     'filters' => array('templates' => array('settings' => array('enable' => 1, 'template_id' => $template_id)))
  //   );
  //   $res_data['js'] = json_encode($js);
  //   $params = array(
  //     'to'        => $send_email,
  //     'toname'    => $toname,
  //     'from'      => "jv@the-binary-institute.com",
  //     'fromname'  => "The Binary Institute - Partners Program",
  //     'subject'   => $subject,
  //     'text'      => "",
  //     'html'      => "<strong></strong>",
  //     'x-smtpapi' => json_encode($js),
  //   );
  //   $session = curl_init("https://api.sendgrid.com/api/mail.send.json");
  //   curl_setopt($session, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $sendgrid_key));
  //   curl_setopt($session, CURLOPT_POST, true);
  //   curl_setopt($session, CURLOPT_POSTFIELDS, $params);
  //   curl_setopt($session, CURLOPT_HEADER, false);
  //   curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
  //   $response = curl_exec($session);
  //   curl_close($session);
  //
  //   return $response;
  // }

  public static function _sgMailer($template_id, $subs, $send_email, $toname, $subject) {
    $sendgrid_key = "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";
    $js = array(
      'sub' => $subs,
      'filters' => array('templates' => array('settings' => array('enable' => 1, 'template_id' => $template_id)))
    );
    $res_data['js'] = json_encode($js);
    $params = array(
      'to'        => $send_email,
      'toname'    => $toname,
      'from'      => "josh@option-figures.com",
      'fromname'  => "OptionFigures",
      'subject'   => $subject,
      'text'      => "",
      'html'      => "<strong></strong>",
      'x-smtpapi' => json_encode($js),
    );
    $session = curl_init("https://api.sendgrid.com/api/mail.send.json");
    curl_setopt($session, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $sendgrid_key));
    curl_setopt($session, CURLOPT_POST, true);
    curl_setopt($session, CURLOPT_POSTFIELDS, $params);
    curl_setopt($session, CURLOPT_HEADER, false);
    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($session);
    curl_close($session);

    return $response;
  }

  public static function _contactApiAssign($list_id, $params) {
		$res_data = array(
      'status' => false,
      'msg' => '',
      'debug' => null,
      'js' => null,
      'id' => '',
      'list_debug' => null
    );
    $sendgrid_apikey = "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";

    // === create contact
    $params = json_encode($params);
    $res_data['js'] = $params;
    $request =  "https://api.sendgrid.com/v3/contactdb/recipients";
    $session = curl_init($request);
    // curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
    curl_setopt($session, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $sendgrid_apikey));
    curl_setopt($session, CURLOPT_POST, true);
    curl_setopt($session, CURLOPT_POSTFIELDS, $params);
    curl_setopt($session, CURLOPT_HEADER, false);
    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($session);
    curl_close($session);
    $object_response = json_decode($response);
    $recipient_id = $object_response->persisted_recipients[0];
    $res_data['id'] = $recipient_id;
    $res_data['debug'] = $response;

    // add contact to specific list
    $request_list = "https://api.sendgrid.com/v3/contactdb/lists/".$list_id."/recipients/".$recipient_id;
    $session_list = curl_init($request_list);
    // curl_setopt($session_list, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
    curl_setopt($session_list, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $sendgrid_apikey));
    curl_setopt($session_list, CURLOPT_POST, true);
    curl_setopt($session_list, CURLOPT_HEADER, false);
    curl_setopt($session_list, CURLOPT_RETURNTRANSFER, true);
    $response_list = curl_exec($session_list);
    curl_close($session_list);
    $res_data['list_debug'] = $response_list;

    return $res_data;
	}

  public static function _sendBrandLead($q_object, $user_lead_object) {
    $res_data = array(
      'is_error' => false,
      'msg' => 'Success',
      'debug' => ''
    );
    $brand = $q_object->brand;
    $schedule_date = $q_object->schedule_date;

    $zt_ac_countries = Country::all();

    if($brand == 'TradeSolid') {

      $zt_ac_countries = array(
				array('country' => 'Netherland', 'country_code' => 'NL', 'code' => 150),
				array('country' => 'Germany', 'country_code' => 'DE', 'code' => 80),
				array('country' => 'Norway', 'country_code' => 'NO', 'code' => 150),
				array('country' => 'Iceland', 'country_code' => 'IS', 'code' => 98),
				array('country' => 'Switzerland', 'country_code' => 'CH', 'code' => 206),
				array('country' => 'Sweden', 'country_code' => 'SE', 'code' => 205),
				array('country' => 'Malaysia', 'country_code' => 'MY', 'code' => 129),
				array('country' => 'Australia', 'country_code' => 'AU', 'code' => 13),
				array('country' => 'Hong Kong', 'country_code' => 'HK', 'code' => 96),
				array('country' => 'South Africa', 'country_code' => 'ZA', 'code' => 197),
				array('country' => 'United Arab Emirates', 'country_code' => 'AE', 'code' => 224),
				array('country' => 'Saudi Arabia', 'country_code' => 'SA', 'code' => 187),
				array('country' => 'Qatar', 'country_code' => 'QA', 'code' => 174)
      );

      $is_legal_county = false;
      $c_key = -1;
      if($user_lead_object->country !== "") {
        foreach($zt_ac_countries as $k => $lc) {
          if($lc['country'] == $user_lead_object->country || $lc['country_code'] == $user_lead_object->country_code) {
            $c_key = $k;
            $is_legal_county = true;
            break;
          }
        }
      }

			if($is_legal_county) {

        $send_url = "http://api-spotplatform.tradesolid.com/api";
        $send_data = array(
          'api_username' => 'thebinaryinstitute',
          'api_password' => 'ANrx6p6B4a',
          'MODULE' => 'Customer',
          'COMMAND' => 'add',
          'FirstName' => $user_lead_object->fname,
          'LastName' => $user_lead_object->lname,
          'email' => $user_lead_object->email,
          'password' => $user_lead_object->psw,
					'Phone' => $user_lead_object->phone,
          'Country' => $zt_ac_countries[$c_key]['code'],
					'currency' => 'EUR',
					'birthday' => "1980-07-21",
					'gender' => 'male',
					'registrationCountry' => $zt_ac_countries[$c_key]['code'],
          'campaignId' => 64,
          // 'subCampaign' => 4322,
          // 'subcampaignName' => 35115,
          'subcampaignID' => 4322,
          'a_id' => 35191,
					'regulateStatus' => 'pending',
					'regulateType' => 1
        );

        $ch = curl_init($send_url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($send_data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        $api_result = simplexml_load_string($result);
        curl_close($ch);

        if($api_result->errors && $api_result->errors->error) {
          $error_msg = "";
          foreach($api_result->errors->error as $err) {
            $error_msg .= $err.", ";
          }
          $error_msg = substr($error_msg, 0, -2);
          $res_data['is_error'] = true;
	        $res_data['msg'] = $error_msg;
        }

      } else {
        $res_data['is_error'] = true;
        $res_data['msg'] = "Country ".$user_lead_object->country." is not valid for sending a lead";
      }

    } else if($brand == 'Option500') {

      $zt_ac_countries = array(
				array('country' => 'United Kingdom', 'country_code' => 'GB', 'code' => 225),
				array('country' => 'Canada', 'country_code' => 'CA', 'code' => 204),
				array('country' => 'New Zealand', 'country_code' => 'NZ', 'code' => 153),
				array('country' => 'Australia', 'country_code' => 'AU', 'code' => 13),
				array('country' => 'Singapore', 'country_code' => 'SG', 'code' => 192),
				array('country' => 'Hong Kong', 'country_code' => 'HK', 'code' => 96),
				array('country' => 'Malaysia', 'country_code' => 'MY', 'code' => 129),
				array('country' => 'United Arab Emirates', 'country_code' => 'AE', 'code' => 224),
				array('country' => 'South Africa', 'country_code' => 'ZA', 'code' => 197)
      );
			$is_legal_county = false;
      $c_key = -1;
      if($user_lead_object->country !== "") {
        foreach($zt_ac_countries as $k => $lc) {
          if($lc['country'] == $user_lead_object->country || $lc['country_code'] == $user_lead_object->country_code) {
            $c_key = $k;
            $is_legal_county = true;
            break;
          }
        }
      }

      if($is_legal_county) {

        $send_url = "https://platform-api.tradesmarter.com/index/register";
				$send_data = array(
	        'firstName' => $user_lead_object->fname,
	        'lastName' => $user_lead_object->lname,
	        'email' => $user_lead_object->email,
	        'password' => md5($user_lead_object->psw),
	        'phone' => $user_lead_object->phone,
	        'country' => $zt_ac_countries[$c_key]['code'],
	        'landing' => json_encode(array('a_aid'=>'8b1c1e8a'))
	      );
	      $ch = curl_init($send_url);
	      curl_setopt($ch, CURLOPT_HEADER, 0);
	      curl_setopt($ch, CURLOPT_POST, true);
	      curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	      curl_setopt($ch, CURLOPT_POST, 1);
	      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	      curl_setopt($ch, CURLOPT_POSTFIELDS, $send_data);
	      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	      curl_setopt($ch, CURLOPT_USERPWD, "500-binstitute:15ea44da");
	      $result = curl_exec($ch);
	      curl_close($ch);
	      $res_data['debug'] = $result;

	      if(is_object($result) && $result->error) {
	        $error_msg = $result->error->message;
          $res_data['is_error'] = true;
	        $res_data['msg'] = $error_msg;
	      }
      } else {
        $res_data['is_error'] = true;
        $res_data['msg'] = "Country ".$user_lead_object->country." is not valid for sending a lead";
      }

    } else if($brand == 'OneTwoTrade') {
      $is_legal_county = false;
      $c_key = -1;
      if($user_lead_object->country !== "") {
        foreach($zt_ac_countries as $k => $lc) {
          if($lc['name'] == $user_lead_object->country || $lc['iso'] == $user_lead_object->country_code) {
            $c_key = $k;
            $is_legal_county = true;
            break;
          }
        }
      }

			if($is_legal_county) {

				$send_url = "http://www.api.onetwotrade.com/api";
        $send_data = array(
          'api_username' => 'TheBinaryInstitute_649',
          'api_password' => 'HfS8xq9W0k',
          'MODULE' => 'Customer',
          'COMMAND' => 'add',
          'FirstName' => $user_lead_object->fname,
          'LastName' => $user_lead_object->lname,
          'email' => $user_lead_object->email,
          'password' => $user_lead_object->psw,
          'Country' => $zt_ac_countries[$c_key]['id'],
          'campaignId' => 649,
          'subCampaign' => $user_lead_object->lead_id,
          'currency' => 'USD',
          'Phone' => $user_lead_object->phone,
          'a_id' => '4c1784efdcccb',
          'birthday' => "1980-07-21"
        );
        $ch = curl_init($send_url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($send_data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        $res_data['debug'] = $result;
        $api_result = simplexml_load_string($result);
        curl_close($ch);

        if($api_result->errors && $api_result->errors->error) {
          $error_msg = "";
          foreach($api_result->errors->error as $err) {
            $error_msg .= $err.", ";
          }
          $error_msg = substr($error_msg, 0, -2);
          $res_data['is_error'] = true;
          $res_data['msg'] = $error_msg;

          if($error_msg == "") {
            $res_data['msg'] = $api_result->errors->error->message;
          }
        }

			} else {
				$res_data['is_error'] = true;
        $res_data['msg'] = "Country ".$user_lead_object->country." is not valid for sending a lead";
			}

    } else if($brand == 'Keyoption') {
      $is_legal_county = false;
      $c_key = -1;
      if($user_lead_object->country !== "") {
        foreach($zt_ac_countries as $k => $lc) {
          if($lc['name'] == $user_lead_object->country || $lc['iso'] == $user_lead_object->country_code) {
            $c_key = $k;
            $is_legal_county = true;
            break;
          }
        }
      }

			if($is_legal_county) {

				$send_url = "https://api-spotplatform.keyoption.com/Api";
        $send_data = array(
          'api_username' => 'thebinaryinstitute',
          'api_password' => 'aKj6Rn3hU6',
          'MODULE' => 'Customer',
          'COMMAND' => 'add',
          'FirstName' => $user_lead_object->fname,
          'LastName' => $user_lead_object->lname,
          'email' => $user_lead_object->email,
          'password' => $user_lead_object->psw,
					'Phone' => $user_lead_object->phone,
          'Country' => $zt_ac_countries[$c_key]['id'],
					'currency' => 'EUR',
					'birthday' => "1980-07-21",
					'gender' => 'male',
					'registrationCountry' => $zt_ac_countries[$c_key]['id'],
          'campaignId' => 116,
          'subCampaign' => 35191,
          'a_id' => 35191,
					'regulateStatus' => 'pending',
					'regulateType' => 1
        );
        $ch = curl_init($send_url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($send_data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        $res_data['debug'] = $result;
        $api_result = simplexml_load_string($result);
        curl_close($ch);

        if($api_result->errors && $api_result->errors->error) {
          $error_msg = "";
          foreach($api_result->errors->error as $err) {
            $error_msg .= $err.", ";
          }
          $error_msg = substr($error_msg, 0, -2);
          $res_data['is_error'] = true;
          $res_data['msg'] = $error_msg;

          if($error_msg == "") {
            $res_data['msg'] = $api_result->errors->error->message;
          }
        }

			} else {
        $res_data['is_error'] = true;
        $res_data['msg'] = "Country ".$user_lead_object->country." is not valid for sending a lead";
			}

    } else if($brand == 'FTrade') {
      $is_legal_county = false;
      $c_key = -1;
      if($user_lead_object->country !== "") {
        foreach($zt_ac_countries as $k => $lc) {
          if($lc['name'] == $user_lead_object->country || $lc['iso'] == $user_lead_object->country_code) {
            $c_key = $k;
            $is_legal_county = true;
            break;
          }
        }
      }

      if($is_legal_county) {
        $send_url = "http://api-spotplatform.ftrade.com/Api";
        $send_data = array(
          'api_username' => 'Thebinaryinstitute',
          'api_password' => 'Pz50WBbyz2',
          'MODULE' => 'Customer',
          'COMMAND' => 'add',
          'FirstName' => $user_lead_object->fname,
          'LastName' => $user_lead_object->lname,
          'email' => $user_lead_object->email,
          'password' => $user_lead_object->psw,
          'Country' => $zt_ac_countries[$c_key]['id'],
          'campaignId' => 218,
          'subCampaign' => $user_lead_object->lead_id,
          'currency' => 'USD',
          'Phone' => $user_lead_object->phone,
          'a_id' => '4c1784efdcccb',
          'birthday' => "1980-07-21"
        );
        $ch = curl_init($send_url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($send_data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        $res_data['debug'] = $result;
        $api_result = simplexml_load_string($result);
        curl_close($ch);

        if($api_result->errors && $api_result->errors->error) {
          $error_msg = "";
          foreach($api_result->errors->error as $err) {
            $error_msg .= $err.", ";
          }
          $error_msg = substr($error_msg, 0, -2);
          $res_data['is_error'] = true;
          $res_data['msg'] = $error_msg;

          if($error_msg == "") {
            $res_data['msg'] = $api_result->errors->error->message;
          }
        }

      } else {
        $res_data['is_error'] = true;
        $res_data['msg'] = "Country ".$user_lead_object->country." is not valid for sending a lead";
      }

    } else if($brand == 'OptionLive') {
      $is_legal_county = false;
      $c_key = -1;
      if($user_lead_object->country !== "") {
        foreach($zt_ac_countries as $k => $lc) {
          if($lc['name'] == $user_lead_object->country || $lc['iso'] == $user_lead_object->country_code) {
            $c_key = $k;
            $is_legal_county = true;
            break;
          }
        }
      }

      if($is_legal_county) {
        $hm_api = new HelloMarketsApi();
        $hm_data = array(
          'keyword' => $user_lead_object->lead_id,
          'email' => $user_lead_object->email,
          'password' => $user_lead_object->psw,
          'password_confirm' => $user_lead_object->psw,
          'firstname' => $user_lead_object->fname,
          'lastname' => $user_lead_object->lname,
          'phone' => $user_lead_object->phone,
          'currency' => 'USD',
          'country_id' => 66
        );
        $hm_lead = $hm_api->Call('create-lead', $hm_data);

        if($hm_lead->error_code == 0) {

        } else {
          $res_data['is_error'] = true;
          $res_data['msg'] = $hm_lead->error_message;
        }

      } else {
        $res_data['is_error'] = true;
        $res_data['msg'] = "Country ".$user_lead_object->country." is not valid for sending a lead";
      }

    } else if($brand == 'Bank of Options') {
      $is_legal_county = false;
      $c_key = -1;
      if($user_lead_object->country !== "") {
        foreach($zt_ac_countries as $k => $lc) {
          if($lc['name'] == $user_lead_object->country || $lc['iso'] == $user_lead_object->country_code) {
            $c_key = $k;
            $is_legal_county = true;
            break;
          }
        }
      }

      if($is_legal_county) {

        $hm_api = new HelloMarketsApiBank();
        $hm_data = array(
          'keyword' => $user_lead_object->lead_id,
          'email' => $user_lead_object->email,
          'password' => $user_lead_object->psw,
          'password_confirm' => $user_lead_object->psw,
          'firstname' => $user_lead_object->fname,
          'lastname' => $user_lead_object->lname,
          'phone' => $user_lead_object->phone,
          'currency' => 'USD',
          'country_id' => $zt_ac_countries[$c_key]['id']
        );
        $hm_lead = $hm_api->Call('create-lead', $hm_data);

        if($hm_lead->error_code == 0) {

        } else {
          $res_data['is_error'] = true;
          $res_data['msg'] = $hm_lead->error_message;
        }

      } else {
        $res_data['is_error'] = true;
        $res_data['msg'] = "Country ".$user_lead_object->country." is not valid for sending a lead";
      }

    }

    return $res_data;
  }

}
