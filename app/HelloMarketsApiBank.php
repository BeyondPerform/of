<?php

namespace App;

class HelloMarketsApiBank {
	public $Module,$Domain,$AuthId,$AuthSign,$AuthKey,$AuthSecret,$Signs,$apiArray,$ncl,$cl,$cl1,$cl2,$LastReply,$IsOkay;
	function __construct(){
    $this->Module='affiliate';
    $this->Domain="www.bankofoptions.com";
    $this->AuthId="25";
    $this->AuthSign="641dd9c5cd5e8e01678aa7cc9922e0e7a4650c9c74aa7b90bc8e2d6b0b49b1ae9a50278fe877257e5b1b8c8b52aec57b51b88611776486885d112eb8747cda584c5cc64a43db40bd0888281a139ac4bb222aadb29d8a1a19a16e40fe8e0f04540140086e425f2f09ef2725f41594f5ffff5fbef20dfb54873e8d50d05a88693294aba427f4ffc40c9f62da79fa44064c00f0cdabb4e5e0611b33f7b2d1f297b4d222232c10427d36d5713780f99c878521bada9fdb5678899833d888fb34b932e93613511c29e122552cb91ae2927d424701c173388d76c1aed056fd8c9bd7532df6e9dd765d92ace0ae68df15aca9aec37734df7f93f79a3edd7569252075a6b5f80285a2bac220e6b6583cf0be221da2cef97984e1102e76fe86c7f24f4a0698dd0fc1c054f1ab85558e5c4a799237ac34b786bbc2a1e02733b9aa421b8486c48a4695b6bcd470ee7ad53784ee46bab5ae1e30afe453a055703053636635211a68c71250a29f046f34f02dc2f855cd20c532f2cbafd3b7ca3082d48b8473a57ad0341d77a0380aa798937a743052286fc25a964577d90dd5a4e4752baa9a55b740dbba";
    $this->AuthKey="9890491b3cea81d3f711ea9ceb8bfe105ae59404";
    $this->AuthSecret="c11bb73ed1da0fab489d936bd6f7e4fd";
		$this->Signs=array();
		$this->apiArray=array();
		$this->cl=(1<<5)+(1<<3)+(1<<1);
		$this->ncl=@strlen($this->AuthSign)/$this->cl;
		$this->cl1=$this->cl-(1<<5);
		$this->cl2=($this->cl1<<2)-(1<<3);
		$this->LastReply=null;
		$this->IsOkay=false;
		$this->errorCode=-100;
		for($i=$j=0,$g=$this->cl1;$i<$this->ncl;$i++,$j+=$this->cl,$g+=$this->cl) {
			$this->Signs[@substr($this->AuthSign,$j,$this->cl1)]=@substr($this->AuthSign,$g,$this->cl2);
		}
		$this->apiArray=array('auth_key'=>$this->AuthKey,'auth_signature'=>'');
	}
	private function forgeData($array) {
		if(!is_array($array)) return $array;
		$string=array();
		foreach($array as $key=>$val) {
			if(is_array($val)) $val=implode(',',$val);$string[]="{$key}#{$val}";
		}
		return implode('~',$string);
	}
	private function innerEncrypt($text,$salt) {
		return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256,$salt,$text,MCRYPT_MODE_ECB,mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256,MCRYPT_MODE_ECB),MCRYPT_RAND))));
	}
	private function Encrypt($array) {
		return $this->innerEncrypt($this->forgeData($array),$this->AuthSecret);
	}
	private function Decrypt($text,$salt) {
		return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256,$salt,base64_decode($text),MCRYPT_MODE_ECB,mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256,MCRYPT_MODE_ECB),MCRYPT_RAND)));
	}
	public function Call($apiFunctionName,$queryArray=array()) {
		if(is_array($queryArray)) {
			$queryArray=array_merge($queryArray,array('auth_id'=>$this->AuthId));
		} else {
			$queryArray=array('auth_id'=>$this->AuthId);
		}
		$this->apiArray["auth_signature"]=$this->Encrypt($queryArray);
		$url='http://'.$this->Domain.'/api.php/'.$this->Module.'/'.$apiFunctionName.'/';
		$postData=@http_build_query($this->apiArray);
		$curl=curl_init($url);
		curl_setopt($curl,CURLOPT_CUSTOMREQUEST,"POST");
		curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($curl,CURLOPT_POSTFIELDS,$postData);
		$response=curl_exec($curl);
		curl_close($curl);
		$this->LastReply=null;
		$json=@json_decode($response);
		if(is_object($json)||is_array($json)) {
			return $json;
		} else {
			$Signature=@substr($response,0,$this->cl1);
			if(isset($this->Signs[$Signature])) {
				$CollectedData=@substr($response,$this->cl1,@strlen($response)-$this->cl1);
				$this->LastReply=@json_decode($this->Decrypt($CollectedData,$this->Signs[$Signature]));
				$this->IsOkay=false;
				$good_auth_key=false;
				$has_error_code=false;
				if(is_object($this->LastReply)) {
					if(@property_exists($this->LastReply,"auth_key")) {
						if($this->LastReply->auth_key==$this->AuthKey) {
							$good_auth_key=true;
						}
					}
					if(@property_exists($this->LastReply,"error_code")) {
						$this->errorCode=$this->LastReply->error_code;
						if($this->errorCode<0) {
							$has_error_code=true;
						}
					}
				}
				$this->IsOkay=$good_auth_key&&!$has_error_code;
			}
		}
		return $this->LastReply;
	}
}
