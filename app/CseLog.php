<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CseLog extends Model {

	/**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'cse_log';

  /**
   * Table primary key.
   *
   * @var array
   */
  protected $primary_key = 'id';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'ces_id',
  	'user_id',
  	'mail_id',
  	'status',
    'schedule_date',
  	'log'
  ];

  public function __construct($data = null) {
    if($data) {
      parent::__construct($data);
    }
  }

}
