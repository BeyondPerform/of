<?php

namespace App;

class HelloMarketsApi {
	public $Module,$Domain,$AuthId,$AuthSign,$AuthKey,$AuthSecret,$Signs,$apiArray,$ncl,$cl,$cl1,$cl2,$LastReply,$IsOkay;
	function __construct(){
		$this->Module='affiliate';
		$this->Domain="www.optionlive.com";
		$this->AuthId="13";
		$this->AuthSign="da1d8e6e12744a8b5d4a94c707addb7ca77b09bab5cb4cd833dc7327a863a03ea0b331d7090890b033348c74d828a594d3cc727d30ddacda93377b74b2fd3d5ec7400f8c5c4a9ea55b5b9be5c44875222b8955b1b490458cade16e98a86944da589c46994a4aa95585789c6fece8cd3d50f800e2d335f30883dbbc2cd4db4bfa4f2c4dcfc1b05c24d0deb1c4ca6b1502c1ddbb99deaa5d1a504f8ceae0b8f366aa576e9b2e6e026f0e5e838cb8088b4169ad0999ddb4ddd5985cd8b20a56772803ea45b57955b04b4563675d961ad55fd78f429f923f8a833f023f19626188a0ce6a9a0a48889d54a9cabd57b3006dd0342f0b7661204d0e00d70b0c035ca3053bb028e00ed0de020cdd871dcc20e073cb7972c050dd2363753fc242c5233c687f824555f32294f79ff6a3902e3c1691d5fcd651e6b3679f8b50d56a484744abb3db998bb647ae3625568bb88ab64a83dcef8cf85c2657523401ede667d67345b3200be0c2addbeb2b7bace43ab4ca9e659a92ce257d3aa6ea1bb031b1a08de71a77551404de4e6047574657677e31ba4a05da44ab42adbc0c2bad40f7ac2d0c53b8bf26";
		$this->AuthKey="35c90b9a34393acb7bc067dffd1b0d5c52f511e6";
		$this->AuthSecret="c94e3929e0feb71eeec03e7e4ef70919";
		$this->Signs=array();
		$this->apiArray=array();
		$this->cl=(1<<5)+(1<<3)+(1<<1);
		$this->ncl=@strlen($this->AuthSign)/$this->cl;
		$this->cl1=$this->cl-(1<<5);
		$this->cl2=($this->cl1<<2)-(1<<3);
		$this->LastReply=null;
		$this->IsOkay=false;
		$this->errorCode=-100;
		for($i=$j=0,$g=$this->cl1;$i<$this->ncl;$i++,$j+=$this->cl,$g+=$this->cl) {
			$this->Signs[@substr($this->AuthSign,$j,$this->cl1)]=@substr($this->AuthSign,$g,$this->cl2);
		}
		$this->apiArray=array('auth_key'=>$this->AuthKey,'auth_signature'=>'');
	}
	private function forgeData($array) {
		if(!is_array($array)) return $array;
		$string=array();
		foreach($array as $key=>$val) {
			if(is_array($val)) $val=implode(',',$val);$string[]="{$key}#{$val}";
		}
		return implode('~',$string);
	}
	private function innerEncrypt($text,$salt) {
		return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256,$salt,$text,MCRYPT_MODE_ECB,mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256,MCRYPT_MODE_ECB),MCRYPT_RAND))));
	}
	private function Encrypt($array) {
		return $this->innerEncrypt($this->forgeData($array),$this->AuthSecret);
	}
	private function Decrypt($text,$salt) {
		return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256,$salt,base64_decode($text),MCRYPT_MODE_ECB,mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256,MCRYPT_MODE_ECB),MCRYPT_RAND)));
	}
	public function Call($apiFunctionName,$queryArray=array()) {
		if(is_array($queryArray)) {
			$queryArray=array_merge($queryArray,array('auth_id'=>$this->AuthId));
		} else {
			$queryArray=array('auth_id'=>$this->AuthId);
		}
		$this->apiArray["auth_signature"]=$this->Encrypt($queryArray);
		$url='http://'.$this->Domain.'/api.php/'.$this->Module.'/'.$apiFunctionName.'/';
		$postData=@http_build_query($this->apiArray);
		$curl=curl_init($url);
		curl_setopt($curl,CURLOPT_CUSTOMREQUEST,"POST");
		curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,false);
		curl_setopt($curl,CURLOPT_POSTFIELDS,$postData);
		$response=curl_exec($curl);
		curl_close($curl);
		$this->LastReply=null;
		$json=@json_decode($response);
		if(is_object($json)||is_array($json)) {
			return $json;
		} else {
			$Signature=@substr($response,0,$this->cl1);
			if(isset($this->Signs[$Signature])) {
				$CollectedData=@substr($response,$this->cl1,@strlen($response)-$this->cl1);
				$this->LastReply=@json_decode($this->Decrypt($CollectedData,$this->Signs[$Signature]));
				$this->IsOkay=false;
				$good_auth_key=false;
				$has_error_code=false;
				if(is_object($this->LastReply)) {
					if(@property_exists($this->LastReply,"auth_key")) {
						if($this->LastReply->auth_key==$this->AuthKey) {
							$good_auth_key=true;
						}
					}
					if(@property_exists($this->LastReply,"error_code")) {
						$this->errorCode=$this->LastReply->error_code;
						if($this->errorCode<0) {
							$has_error_code=true;
						}
					}
				}
				$this->IsOkay=$good_auth_key&&!$has_error_code;
			}
		}
		return $this->LastReply;
	}
}
