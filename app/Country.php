<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model {

	/**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'country';

  /**
   * Table primary key.
   *
   * @var array
   */
  protected $primary_key = 'id';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'iso',
  	'name'
  ];

  public function __construct($data = null) {
    if($data) {
      parent::__construct($data);
    }
  }

}
