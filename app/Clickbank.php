<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clickbank extends Model {

	/**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'clickbank';

  /**
   * Table primary key.
   *
   * @var array
   */
  protected $primary_key = 'id';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */

  protected $fillable = [
    'fname',
  	'lname',
  	'email',
  	'psw',
    'phone',
  	'country',
    'country_code',
    'ip',
    'lead_id',
    'sale'
  ];

  public function __construct($data = null) {
    if($data) {
      parent::__construct($data);
    }
  }

}
