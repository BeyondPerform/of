activateApiCallModal = (broker_name) ->
  $('#broker_api_call_modal input[type=\'text\'], #broker_api_call_modal input[type=\'password\']').removeClass 'error'
  $('#broker_api_call_modal input[type=\'text\'], #broker_api_call_modal input[type=\'password\']').val ''
  $('#broker_api_call_modal input[type=\'text\'], #broker_api_call_modal input[type=\'password\']').blur()
  $('#broker_api_call_modal #broker_name').text broker_name
  $('#broker_api_call_modal #broker_api_sbm').attr 'onclick', 'return openBrokerAccount(\'' + broker_name + '\')'
  $('#broker_api_call_modal').modal 'show'
  return

openBrokerAccount = (broker_name) ->
  $('#broker_api_call_modal input[type=\'text\'], #broker_api_call_modal input[type=\'password\']').removeClass 'error'
  fname = $.trim($('#broker_api_call_modal #fname').val())
  lname = $.trim($('#broker_api_call_modal #lname').val())
  email = $.trim($('#broker_api_call_modal #email').val())
  psw = $.trim($('#broker_api_call_modal #psw').val())
  phone = $.trim($('#broker_api_call_modal #phone').val())
  _token = $('input[name=_token]').val()
  email_pattern = /^([a-z0-9_\-]+\.)*[a-z0-9_\-]+@([a-z0-9][a-z0-9\-]*[a-z0-9]\.)+[a-z]{2,4}$/i
  if fname == '' or lname == '' or !email_pattern.test(email) or psw == '' or phone == ''
    if fname == ''
      $('#broker_api_call_modal #fname').addClass 'error'
    if lname == ''
      $('#broker_api_call_modal #lname').addClass 'error'
    if !email_pattern.test(email)
      $('#broker_api_call_modal #email').addClass 'error'
    if psw == ''
      $('#broker_api_call_modal #psw').addClass 'error'
    if phone == ''
      $('#broker_api_call_modal #phone').addClass 'error'
  else
    $.ajax
      url: "#{base_url}/leads"
      async: false
      type: 'post'
      dataType: 'json'
      data:
        'broker_name': broker_name
        'fname': fname
        'lname': lname
        'email': email
        'psw': psw
        'phone': phone
        '_token': _token
      success: (data) ->
        $('#broker_api_call_modal input[type=\'text\'], #broker_api_call_modal input[type=\'password\']').removeClass 'error'
        $('#broker_api_call_modal input[type=\'text\'], #broker_api_call_modal input[type=\'password\']').val ''
        $('#broker_api_call_modal input[type=\'text\'], #broker_api_call_modal input[type=\'password\']').blur()
        $('#broker_api_call_modal').modal 'hide'
        if data.status
          toastr.success data.msg, 'Trade with Confidence', timeOut: 10000
        else
          toastr.error data.msg, 'Trade with Confidence', timeOut: 10000
        return
  false
