$(document).ready ->
  $('.video_demo').click ->
    if @paused then @play() else @pause()
    return
  $("#susbscribe_form_modal .form-control").bind "keyup", (e) ->
    $(this).removeClass('error') if e.keyCode isnt 13

activateSubscribeAccountModal = ->
  $("#susbscribe_form_modal").modal "show"

subscribeAccount = (e) ->
  $(e).prop "disabled", true
  $("#susbscribe_form_modal .form-control").removeClass "error"
  fname = $.trim $("#fname").val()
  lname = $.trim $("#lname").val()
  email = $.trim $("#email").val()
  psw = $.trim $("#psw").val()
  phone = $.trim $("#phone").val()
  email_pattern = /^([a-z0-9_\-]+\.)*[a-z0-9_\-]+@([a-z0-9][a-z0-9\-]*[a-z0-9]\.)+[a-z]{2,4}$/i
  token = $("input[type='hidden'][name='_token']").val()
  if fname isnt "" and lname isnt "" and phone isnt "" and email_pattern.test(email) and psw isnt ""
    $.ajax
      url: "#{base_url}/subscribe"
      async: false
      type: 'post'
      dataType: 'json'
      data:
        'fname': fname
        'lname': lname
        'email': email
        'password': psw
        'phone': phone
        '_token': token
      success: (data) ->
        if data.status
          location.href = "#{base_url}/signup_success"
        else
          $(e).prop "disabled", false
          $('#email').addClass 'error'
          toastr.error "<p>#{data.msg}</p>"
  else
    error_msg = ""
    if fname is ""
      $("#fname").addClass 'error'
      error_msg += "<p>First name cant be empty</p>"
    if lname is ""
      $("#lname").addClass 'error'
      error_msg += "<p>Last name cant be empty</p>"
    if phone is ""
      $("#phone").addClass 'error'
      error_msg += "<p>Phone cant be empty</p>"
    if !email_pattern.test(email)
      $("#email").addClass 'error'
      error_msg += "<p>Invalid email format</p>"
    if psw is ""
      $("#psw").addClass 'error'
      error_msg += "<p>Password cant be empty</p>"
    toastr.error error_msg
    $(e).prop "disabled", false
  false

loginFormSubmit = (e) ->
  $(e).prop "disabled", true
  $(".form-control").removeClass "error"
  email = $.trim $("#login_email").val()
  psw = $.trim $("#login_psw").val()
  email_pattern = /^([a-z0-9_\-]+\.)*[a-z0-9_\-]+@([a-z0-9][a-z0-9\-]*[a-z0-9]\.)+[a-z]{2,4}$/i
  token = $("input[type='hidden'][name='_token']").val()
  if email_pattern.test(email) and psw isnt ""
    $.ajax
      url: "#{base_url}/login"
      async: false
      type: 'post'
      dataType: 'json'
      data:
        'email': email
        'password': psw
        '_token': token
      success: (data) ->
        if data.status
          location.reload()
        else
          $(e).prop "disabled", false
          toastr.error "<p>#{data.msg}</p>"
  else
    error_msg = ""
    if !email_pattern.test(email)
      $("#login_email").addClass 'error'
      error_msg += "<p>Invalid email format</p>"
    if psw is ""
      $("#login_psw").addClass 'error'
      error_msg += "<p>Password cant be empty</p>"
    toastr.error error_msg
    $(e).prop "disabled", false
  false
