$(document).ready ->
  $("#susbscribe_form_modal .form-control").bind "keyup", (e) ->
    $(this).removeClass('error') if e.keyCode isnt 13

validatePaymentForm = (form_data) ->
  card_type = null
  v_errors = []
  is_valid_cardnumber = $.payform.validateCardNumber(form_data.payform.cc_cardnumber)
  if is_valid_cardnumber
    card_type = $.payform.parseCardType(form_data.payform.cc_cardnumber)
  else
    mid_error =
      id: 'cc_cardnumber'
      type: 'card_number'
      msg: 'Invalid credit card number'
    v_errors.push mid_error
  if card_type == null
    mid_error =
      type: 'card_type'
      msg: 'Invalid card type'
    v_errors.push mid_error
  is_cvv_valid = $.payform.validateCardCVC(form_data.payform.cc_cvv, card_type)
  if !is_cvv_valid
    mid_error =
      id: 'cc_cvv'
      type: 'cvv'
      msg: 'Invalid CVV2'
    v_errors.push mid_error
  is_valid_expire = false
  parse_cc_expire = $.payform.parseCardExpiry(form_data.payform.cc_expire)
  if !_.isEmpty(parse_cc_expire)
    is_valid_expire = $.payform.validateCardExpiry(parse_cc_expire.month, parse_cc_expire.year)
  if !is_valid_expire
    mid_error =
      id: 'cc_expire'
      type: 'expire'
      msg: 'Invalid expiration date'
    v_errors.push mid_error
  validate_res =
    'errors': v_errors
    'card_data':
      'cc_cardnumber': form_data.payform.cc_cardnumber
      'cc_expire': form_data.payform.cc_expire
      'cc_cvv': form_data.payform.cc_cvv
      'card_type': card_type
  validate_res

subscribeAccount = (e) ->
  token = $("input[type='hidden'][name='_token']").val()
  $('#susbscribe_form .form-control').removeClass 'error'
  $(e).prop 'disabled', true
  form_data =
    payform:
      'cc_cardnumber': $.trim($('#cc_cardnumber').val())
      'cc_cvv': $.trim($('#cc_cvv').val())
      'cc_expire': $.trim($('#cc_expire').val())
  validation_res = validatePaymentForm(form_data)
  if validation_res.errors.length > 0
    errors_msg = ''
    i = 0
    while i < validation_res.errors.length
      $('#susbscribe_form #' + validation_res.errors[i].id).addClass 'error'
      errors_msg += '<p>' + validation_res.errors[i].msg + '</p>'
      i++
    toastr.error errors_msg
    $(e).prop 'disabled', false
  else
    client_token = $.trim($('#client_token').val())
    client = new (braintree.api.Client)(clientToken: client_token)
    client.tokenizeCard {
      number: form_data.payform.cc_cardnumber
      cardholderName: fname + ' ' + lname
      expirationDate: form_data.payform.cc_expire
      cvv: form_data.payform.cc_cvv
    }, (err, nonce) ->
      if !err
        $.ajax
          url: "#{base_url}/calculator/pay"
          async: false
          type: 'post'
          dataType: 'json'
          data:
            "_token": token
            'nonce': nonce
          success: (data) ->
            $(e).prop 'disabled', false
            if data.is_payment_error
              payment_error_msg = ''
              i = 0
              while i < data.payment_errors.length
                payment_error_msg += '<p>' + data.payment_errors[i] + '</p>'
                i++
              toastr.error payment_error_msg
            else
              location.href = "#{base_url}/calc_payment_success"
            return
      else
        toastr.error 'Payment processing error'
        $(e).prop 'disabled', false
      return
  false
