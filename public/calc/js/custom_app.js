(function($) {
    $.rand = function(arg) {
        if ($.isArray(arg)) {
            return arg[$.rand(arg.length)];
        } else if (typeof arg === "number") {
            return Math.floor(Math.random() * arg);
        } else {
            return 4;  // chosen by fair dice roll
        }
    };
})(jQuery);


$(document).ready(function(e) {

  $(".user_guide .navi > li > a").bind("click", function(e) {
    var data_cid = $(this).attr("data-cid");
    $.scrollTo($(".user_guide #" + data_cid), 400);
  });


	$(".login_form .form-control").bind("keyup", function(e) {
		if(e.keyCode !== 13) {
			$(this).removeClass('error');
			$(this).parent().next().hide();
			$(this).parent().next().text("");
		}
	});

	$(".promo_navi > li > a").bind("click", function(e) {
      var data_id = $(this).attr("data-id");
      $(this).addClass("active");
      $(".promo_navi > li > a").not($(this)).removeClass("active");
      $("#broker_" + data_id).addClass("active");
      $(".promo_bean").not($("#broker_" + data_id)).removeClass("active");
  });

	if($("#dashboard_trade_navi").length > 0) {
	  $("#dashboard_trade_navi > li > a").bind("click", function(e) {
	  	var data_chart_id = $(this).attr("data-chart-id");
	  	if(data_chart_id == 'dash_chart_1') {
	  		setTimeout(function() {
	  			initChart('dash_chart_1', dash_chart_1[1]);
	  		}, 200);
	  	} else if(data_chart_id == 'dash_chart_2') {
	  		setTimeout(function() {
	  			initChart('dash_chart_2', dash_chart_2[1]);
	  		}, 200);
	  	} else if(data_chart_id == 'dash_chart_3') {
	  		setTimeout(function() {
	  			initChart('dash_chart_3', dash_chart_3[1]);
	  		}, 200);
	  	}
	  });
	}

	if($("#currency_trade_navi").length > 0) {
    initChart('modal_progress_chart', curr_chart_1[1]);
	  $("#currency_trade_navi > li > a").bind("click", function(e) {
	  	var data_chart_id = $(this).attr("data-chart-id");
	  	if(data_chart_id == 'curr_chart_1') {
	  		setTimeout(function() {
	  			initChart('curr_chart_1', curr_chart_1[1]);
	  		}, 200);
	  	} else if(data_chart_id == 'curr_chart_2') {
	  		setTimeout(function() {
	  			initChart('curr_chart_2', curr_chart_2[1]);
	  		}, 200);
	  	} else if(data_chart_id == 'curr_chart_3') {
	  		setTimeout(function() {
	  			initChart('curr_chart_3', curr_chart_3[1]);
	  		}, 200);
	  	} else if(data_chart_id == 'curr_chart_4') {
	  		setTimeout(function() {
	  			initChart('curr_chart_4', curr_chart_4[1]);
	  		}, 200);
	  	}
	  });
	}

	if($("#comm_trade_navi").length > 0) {
    initChart('modal_progress_chart', comm_chart_1[1]);
	  $("#comm_trade_navi > li > a").bind("click", function(e) {
	  	var data_chart_id = $(this).attr("data-chart-id");
	  	if(data_chart_id == 'comm_chart_1') {
	  		setTimeout(function() {
	  			initChart('comm_chart_1', comm_chart_1[1]);
	  		}, 200);
	  	} else if(data_chart_id == 'comm_chart_2') {
	  		setTimeout(function() {
	  			initChart('comm_chart_2', comm_chart_2[1]);
	  		}, 200);
	  	} else if(data_chart_id == 'comm_chart_3') {
	  		setTimeout(function() {
	  			initChart('comm_chart_3', comm_chart_3[1]);
	  		}, 200);
	  	} else if(data_chart_id == 'comm_chart_4') {
	  		setTimeout(function() {
	  			initChart('comm_chart_4', comm_chart_4[1]);
	  		}, 200);
	  	} else if(data_chart_id == 'comm_chart_5') {
        setTimeout(function() {
          initChart('comm_chart_5', comm_chart_5[1]);
        }, 200);
      } else if(data_chart_id == 'comm_chart_6') {
        setTimeout(function() {
          initChart('comm_chart_6', comm_chart_6[1]);
        }, 200);
      }
	  });
	}

  if($("#indices_trade_navi").length > 0) {
    initChart('modal_progress_chart', indices_chart_1[1]);
    $("#indices_trade_navi > li > a").bind("click", function(e) {
      var data_chart_id = $(this).attr("data-chart-id");
      if(data_chart_id == 'indices_chart_1') {
        setTimeout(function() {
          initChart('indices_chart_1', indices_chart_1[1]);
        }, 200);
      } else if(data_chart_id == 'indices_chart_2') {
        setTimeout(function() {
          initChart('indices_chart_2', indices_chart_2[1]);
        }, 200);
      } else if(data_chart_id == 'indices_chart_3') {
        setTimeout(function() {
          initChart('indices_chart_3', indices_chart_3[1]);
        }, 200);
      } else if(data_chart_id == 'indices_chart_4') {
        setTimeout(function() {
          initChart('indices_chart_4', indices_chart_4[1]);
        }, 200);
      } else if(data_chart_id == 'indices_chart_5') {
        setTimeout(function() {
          initChart('indices_chart_5', indices_chart_5[1]);
        }, 200);
      } else if(data_chart_id == 'indices_chart_6') {
        setTimeout(function() {
          initChart('indices_chart_6', indices_chart_6[1]);
        }, 200);
      } else if(data_chart_id == 'indices_chart_7') {
        setTimeout(function() {
          initChart('indices_chart_7', indices_chart_7[1]);
        }, 200);
      }
    });
  }

  function initChart(render_chart_id, chart_data) {
	    var fillColor = {
          linearGradient : {
              x1: 0, 
              y1: 0, 
              x2: 0, 
              y2: 1
          },
          stops : [[0, 'rgba(248,170,143,0.8)'],
                   [1, 'rgba(248,170,143,0)']]
      };
	    
	    var chart = new Highcharts.StockChart({
        yAxis: {
          id: 'highcharts-grid',
          gridLineColor: '#e5e5e5',
          lineColor: '#cf9581',
          lineWidth: 0
        },
        chart: {
          renderTo: render_chart_id
        },
        rangeSelector: {
          enabled: false
        },
        navigator: {
          enabled: false
        },
        scrollbar: {
          enabled: false
        },
        credits: {
            enabled: false
        },
        series: [{
          id: 'chart-series',
          name: 'Price',
          type: 'area',
          data: chart_data,
          threshold : null,
          fillColor: fillColor
        }],
        plotOptions: {
          line: {
            lineWidth: 1,
            color: "#676767",
            dataGrouping: {
                enabled: false
            },
            allowPointSelect: false                         
          },
          area: {
            lineWidth: 1,
            color: "#e5e5e5",
            dataGrouping: {
                enabled: false
            }
          }
        }
	    });
	}

	if($("#dash_chart_1").length > 0) {
		initChart('dash_chart_1', dash_chart_1[1]);
	}

	if($("#curr_chart_1").length > 0) {
		initChart('curr_chart_1', curr_chart_1[1]);
	}

	if($("#comm_chart_1").length > 0) {
		initChart('comm_chart_1', comm_chart_1[1]);
	}

  if($("#indices_chart_1").length > 0) {
    initChart('indices_chart_1', indices_chart_1[1]);
  }

});

var restorePsw, restorePswFormSubmit, loginFormSubmit, startCalcModal, startAnimationsQueu, finishCalcProgress, resetAnimations, activateApiCallModal, openBrokerAccount;

startCalcModal = function(e) {
  resetAnimations();
  $("#calc_progress_modal").modal("show");
  startAnimationsQueu();
}
startAnimationsQueu = function(e) {
  $("#calc_progress_modal .logs").val("Connecting to servers");
  $("#calc_progress_modal #mc_arrow_1").animate({"opacity": "1"}, 1800, function() {
    $("#calc_progress_modal .logs").val("Connecting to servers\nLoading results from all sources...");
    $("#calc_progress_modal #mc_progress .progress-bar").attr("aria-valuenow", "20");
    $("#calc_progress_modal #mc_progress .progress-bar").css("width", "20%");
    $("#calc_progress_modal #mc_progress .progress-bar span").text("20%");

    $("#calc_progress_modal #mc_arrow_2").animate({"opacity": "1"}, 1800, function() {
      $("#calc_progress_modal .logs").val("Connecting to servers\nLoading results from all sources...\n....Downloading results from all sources\n...Please Wait...");
      $("#calc_progress_modal #mc_progress .progress-bar").attr("aria-valuenow", "40");
      $("#calc_progress_modal #mc_progress .progress-bar").css("width", "40%");
      $("#calc_progress_modal #mc_progress .progress-bar span").text("40%");

      $("#calc_progress_modal #mc_arrow_3").animate({"opacity": "1"}, 1800, function() {
        $("#calc_progress_modal .logs").val("Connecting to servers\nLoading results from all sources...\n....Downloading results from all sources\n...Please Wait...\nProccessing results...");
        $("#calc_progress_modal #mc_progress .progress-bar").attr("aria-valuenow", "60");
        $("#calc_progress_modal #mc_progress .progress-bar").css("width", "60%");
        $("#calc_progress_modal #mc_progress .progress-bar span").text("60%");

        $("#calc_progress_modal #mc_arrow_4").animate({"opacity": "1"}, 1800, function() {
          $("#calc_progress_modal .logs").val("Connecting to servers\nLoading results from all sources...\n....Downloading results from all sources\n...Please Wait...\nProccessing results...\n...Calculating results...\nPlease Wait...");
          $("#calc_progress_modal #mc_progress .progress-bar").attr("aria-valuenow", "80");
          $("#calc_progress_modal #mc_progress .progress-bar").css("width", "80%");
          $("#calc_progress_modal #mc_progress .progress-bar span").text("80%");

          $("#calc_progress_modal #mc_arrow_5").animate({"opacity": "1"}, 1800, function() {
            finishCalcProgress();
          });
        });
      });
    });
  });
}
finishCalcProgress = function(e) {
  $("#calc_progress_modal #mc_progress .progress-bar").attr("aria-valuenow", "100");
  $("#calc_progress_modal #mc_progress .progress-bar").css("width", "100%");
  $("#calc_progress_modal #mc_progress .progress-bar span").text("100%");
  $("#calc_progress_modal .logs").val("Connecting to servers\nLoading results from all sources...\n....Downloading results from all sources\n...Please Wait...\nProccessing results...\n...Calculating results...\nPlease Wait...\n...Process completed.");
  var choosen_asset = $(".asset_contract > option:selected").val();
  var choosen_timeslot = $(".asset_timeslot > option:selected").val();

  var shapes = ['up', 'down', 'neutral'];
  var selected_shape = $.rand(shapes);

  setTimeout(function() {
    $("#calc_progress_modal").modal('hide');
    if(choosen_asset !== "0") {
      $("#calc_results_modal .choosen_asset").text(choosen_asset);
    }
    if(choosen_timeslot !== "0") {
      $("#calc_results_modal .choosen_timeslot").text(choosen_timeslot);
    }
    for(var i = 0; i < shapes.length; i++) {
      $(".shapes_bean").removeClass(shapes[i]);
      $(".shapes_bean_txt").removeClass(shapes[i]);
      $(".shapes_bean_txt").text("");
    }
    $(".shapes_bean").addClass(selected_shape);
    $(".shapes_bean_txt").addClass(selected_shape);
    $(".shapes_bean_txt").text(selected_shape);
    $("#calc_results_modal").modal('show');
  }, 1000);
}

resetAnimations = function(e) {
  $("#calc_progress_modal .logs").val("");
  $("#calc_progress_modal #mc_progress .progress-bar").attr("aria-valuenow", "0");
  $("#calc_progress_modal #mc_progress .progress-bar").css("width", "0%");
  $("#calc_progress_modal #mc_progress .progress-bar span").text("0%");
  $("#calc_progress_modal .mc_arrow").css("opacity", "0");
}

restorePsw = function(user_id, e) {
  var rp_psw, rp_psw_conf;
  $("#sbm_psw_res_holder").html("");
  $(".form-control").removeClass('error');
  $(".form_error").hide();
  $(".form_error").text("");
  rp_psw = $.trim($("#rp_psw").val());
  rp_psw_conf = $.trim($("#rp_psw_conf").val());
  if(rp_psw.length >= 3 && rp_psw == rp_psw_conf) {
    send_data = {
      'psw': rp_psw,
      'user_id': user_id
    };
    $(e).addClass('disabled');
    $(e).attr('disabled', true);
    $.ajax({
      url: "" + base_url + "index/ajaxrestorepsw",
      async: false,
      type: "POST",
      dataType: 'json',
      data: send_data,
      success: function(data) {
        $(".form_error").hide();
        $(".form_error").text("");
        $(".form-control").removeClass('error');
        $(".form-control").val("");
        $(".form-control").blur();
        $(e).removeClass('disabled');
        $(e).removeAttr('disabled');
        var alert_content = "<div class='alert alert-danger alert_custom_def'><strong>" + data.msg + "</strong></div>";
        if (data && data.status) {
          $('form').remove();
          alert_content = "<div class='alert alert-info alert_custom_def'><strong>" + data.msg + "</strong></div>";
        }
        $("#sbm_psw_res_holder").html(alert_content);
      }
    });
  } else {
    if(rp_psw.length < 3) {
      $(".rp_psw_error").text("Too short password");
      $(".rp_psw_error").fadeOut('fast');
      $(".rp_psw_error").fadeIn('fast');
    }
    if(rp_psw != rp_psw_conf) {
      $(".rp_psw_conf_error").text("Password is not confirmed");
      $(".rp_psw_conf_error").fadeOut('fast');
      $(".rp_psw_conf_error").fadeIn('fast');
    }
  }
  return false;
}

restorePswFormSubmit = function(e) {
  var email_pattern, login_email, send_data;
  $("#sbm_res_holder").html("");
  $(".form-control").removeClass('error');
  $(".form_error").hide();
  $(".form_error").text("");
  email_pattern = /^([a-z0-9_\-]+\.)*[a-z0-9_\-]+@([a-z0-9][a-z0-9\-]*[a-z0-9]\.)+[a-z]{2,4}$/i;
  login_email = $.trim($("#login_email").val());
  if (email_pattern.test(login_email)) {
    send_data = {
      'email': login_email
    };
    $(e).addClass('disabled');
    $(e).attr('disabled', true);
    $.ajax({
      url: "" + base_url + "index/ajaxrestorepswform",
      async: false,
      type: "POST",
      dataType: 'json',
      data: send_data,
      success: function(data) {
        $(".form-control").removeClass('error');
        $(".form_error").hide();
        $(".form_error").text("");
        $("#login_email").val("");
        $("#login_email").blur();
        $(e).removeClass('disabled');
        $(e).removeAttr('disabled');
        var alert_content = "<div class='alert alert-danger alert_custom_def'><strong>" + data.msg + "</strong></div>";
        if (data && data.status) {
          alert_content = "<div class='alert alert-info alert_custom_def'><strong>" + data.msg + "</strong></div>";
        }
        $("#sbm_res_holder").html(alert_content);
      }
    });
  } else {
    if (!email_pattern.test(login_email)) {
      $(".login_email_error").text("Incorrect email format");
      $(".login_email_error").fadeOut('fast');
      $(".login_email_error").fadeIn('fast');
      $("#login_email").addClass("error");
    }
  }
  return false;
}

loginFormSubmit = function(e) {
  var email_pattern, login_email, login_psw, send_data;
  $(".form-control").removeClass('error');
  $(".form_error").hide();
  $(".form_error").text("");
  email_pattern = /^([a-z0-9_\-]+\.)*[a-z0-9_\-]+@([a-z0-9][a-z0-9\-]*[a-z0-9]\.)+[a-z]{2,4}$/i;
  login_email = $.trim($("#login_email").val());
  login_psw = $.trim($("#login_psw").val());
  if (email_pattern.test(login_email) && login_psw.length >= 3) {
    send_data = {
      'email': login_email,
      'psw': login_psw
    };
    $.ajax({
      url: "" + base_url + "index/ajaxlogin",
      async: false,
      type: "POST",
      dataType: 'json',
      data: send_data,
      success: function(data) {
        $(".form-control").removeClass('error');
        if (data && data.status) {
          location.reload();
        } else {
          if (data.error_code === 2) {
          	$(".login_psw_error").text(data.msg);
          	$(".login_psw_error").fadeOut('fast');
          	$(".login_psw_error").fadeIn('fast');
            $("#login_psw").addClass('error');
          } else if (data.error_code === 3) {
            alert(data.error_code);
          }
        }
      }
    });
  } else {
    if (!email_pattern.test(login_email)) {
    	$(".login_email_error").text("Incorrect email format");
    	$(".login_email_error").fadeOut('fast');
    	$(".login_email_error").fadeIn('fast');
    	$("#login_email").addClass("error");
  	}
    if (login_psw.length < 3) {
    	$(".login_psw_error").text("Too short password");
    	$(".login_psw_error").fadeOut('fast');
    	$(".login_psw_error").fadeIn('fast');
    	$("#login_psw").addClass('error');
  	}
  }
  return false;
};

activateApiCallModal = function(broker_name) {
  $("#calc_results_modal").modal('hide');
  
  $("#broker_api_call_modal input[type='text'], #broker_api_call_modal input[type='password']").removeClass('error');
  $("#broker_api_call_modal input[type='text'], #broker_api_call_modal input[type='password']").val("");
  $("#broker_api_call_modal input[type='text'], #broker_api_call_modal input[type='password']").blur();
  $("#broker_api_call_modal #broker_name").text(broker_name);
  $("#broker_api_call_modal #broker_api_sbm").attr("onclick", "return openBrokerAccount('" + broker_name + "')")

  $("#broker_api_call_modal").modal("show");
}

openBrokerAccount = function(broker_name) {
  $("#broker_api_call_modal input[type='text'], #broker_api_call_modal input[type='password']").removeClass('error');
  var fname = $.trim($("#broker_api_call_modal #fname").val());
  var lname = $.trim($("#broker_api_call_modal #lname").val());
  var email = $.trim($("#broker_api_call_modal #email").val());
  var psw = $.trim($("#broker_api_call_modal #psw").val());
  var phone = $.trim($("#broker_api_call_modal #phone").val());
  var email_pattern = /^([a-z0-9_\-]+\.)*[a-z0-9_\-]+@([a-z0-9][a-z0-9\-]*[a-z0-9]\.)+[a-z]{2,4}$/i;
  if(fname === '' || lname === '' || !email_pattern.test(email) || psw === '' || phone === '') {
    if(fname === '') {
      $("#broker_api_call_modal #fname").addClass('error');
    }
    if(lname === '') {
      $("#broker_api_call_modal #lname").addClass('error');
    }
    if(!email_pattern.test(email)) {
      $("#broker_api_call_modal #email").addClass('error');
    }
    if(psw === '') {
      $("#broker_api_call_modal #psw").addClass('error');
    }
    if(phone === '') {
      $("#broker_api_call_modal #phone").addClass('error');
    }
  } else {
    $.ajax({
      url: base_url + "index/openbrokeraccount",
      async: false,
      type: "post",
      dataType: 'json',
      data: {
        'broker_name': broker_name,
        'fname': fname,
        'lname': lname,
        'email': email,
        'psw': psw,
        'phone': phone
      },
      success: function(data) {
        $("#broker_api_call_modal input[type='text'], #broker_api_call_modal input[type='password']").removeClass('error');
        $("#broker_api_call_modal input[type='text'], #broker_api_call_modal input[type='password']").val("");
        $("#broker_api_call_modal input[type='text'], #broker_api_call_modal input[type='password']").blur();
        console.log(data);
        $("#broker_api_call_modal").modal("hide");
        if(data.status) {
          $.notify("<strong>" + data.msg + "</strong>", {type: "info", showProgressbar: false});
        } else {
          $.notify("<strong>" + data.msg + "</strong>", {type: "danger", showProgressbar: false});
        }
      }
    });
  }
  return false;
}
