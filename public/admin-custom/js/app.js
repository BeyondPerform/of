$(document).ready(function() {
	App.init();
	Dashboard.init();
  TableManageResponsive.init();

  $(".bleads_tbl tbody tr").each(function(i, v) {
  	var brands_source_el = $(v).find(".push_tags");
		brands_source_el.tagsinput({
		  itemValue: 'value',
		  itemText: 'text'
		});
		var push_tags_selector = $(v).find(".push_tags_selector");
		push_tags_selector.unbind("change");
		push_tags_selector.bind("change", function(e) {
		  var s_txt, s_val, tmp_val;
		  brands_source_el.tagsinput({
		    itemValue: 'value',
		    itemText: 'text'
		  });
		  var s_val = $(this).val();
		  var s_txt = push_tags_selector.find("option:selected").text();
	    brands_source_el.tagsinput("add", {
	      'value': s_val,
	      'text': s_txt
	    });
		});
  });

  var file_up_ui, file_uploader;
  file_uploader = null;
  if($("#import_leads_csv").length > 0) {
    var _token = $('input[name=_csv_upload_token]').val();
    file_up_ui = $("#import_leads_csv");
    file_uploader = new AjaxUpload(file_up_ui, {
      action: "leadsimporthandler",
      name: 'leads_file',
      responseType: 'json',
      autoSubmit: true,
      data: {
        '_token': _token
      },
      onChange: function(file, extension) {

      },
      onComplete: function(file, response) {
        // console.log("c file: " + file);
        // console.log("c response: " + response, typeof(response));
        // console.log($.parseJSON(response));
        location.reload();
      }
    });
  }

});

function activatePushModal(e, lead_id) {
  var _token = $('input[name=_token_' + lead_id + ']').val();
  $.ajax({
    url: "getpushes",
    async: false,
    type: "post",
    dataType: 'html',
    data: {
      'lead_id': lead_id,
      '_token': _token
    },
    success: function(data) {
      $("#lead_push_modal .modal-body").html(data);
      $("#lead_push_modal").modal("show");
    }
  });
}

function pushBusersToBrands(e, lead_id) {
	$(e).attr("disabled", true);
	$(e).addClass("disabled");
	var push_brands = $.trim($("#push_tags_" + lead_id).val());
	var _token = $('input[name=_token_' + lead_id + ']').val();
	$.ajax({
      url: base_url + "/admin/storebraintreepushes",
      async: false,
      type: "post",
      dataType: 'json',
      data: {
        'lead_id': lead_id,
        'push_brands': push_brands,
        '_token': _token
      },
      success: function(data) {
        console.log(data);
        if(data.status) {
        	for(var i = 0; i < data.api_results.length; i++) {
            var is_api_error = true;
            if(data.api_results[i].status) is_api_error = false;

            var res_msg = "declined for unknow reason";
            if(data.api_results[i].msg !== null && data.api_results[i].msg !== "") res_msg = data.api_results[i].msg;

            if(is_api_error) {
            	toastr.error("<strong>" + data.api_results[i].brand + ": </strong>" + res_msg, 'Leads Push', {timeOut: 10000});
            } else {
            	toastr.success("<strong>" + data.api_results[i].brand + ": </strong>" + res_msg, 'Leads Push', {timeOut: 10000});
            }

          }
        } else {
        	toastr.error(data.msg, 'Leads Push', {timeOut: 10000});
        }
				$(e).removeAttr("disabled");
				$(e).removeClass("disabled");
      }
    });
}

function pushhBrands(e, lead_id) {
	$(e).attr("disabled", true);
	$(e).addClass("disabled");
	var push_brands = $.trim($("#push_tags_" + lead_id).val());
	var _token = $('input[name=_token_' + lead_id + ']').val();
	$.ajax({
      url: base_url + "/admin/storepushes",
      async: false,
      type: "post",
      dataType: 'json',
      data: {
        'lead_id': lead_id,
        'push_brands': push_brands,
        '_token': _token
      },
      success: function(data) {
        console.log(data);
        if(data.status) {
        	for(var i = 0; i < data.api_results.length; i++) {
            var is_api_error = true;
            if(data.api_results[i].status) is_api_error = false;

            var res_msg = "declined for unknow reason";
            if(data.api_results[i].msg !== null && data.api_results[i].msg !== "") res_msg = data.api_results[i].msg;

            if(is_api_error) {
            	toastr.error("<strong>" + data.api_results[i].brand + ": </strong>" + res_msg, 'Leads Push', {timeOut: 10000});
            } else {
            	toastr.success("<strong>" + data.api_results[i].brand + ": </strong>" + res_msg, 'Leads Push', {timeOut: 10000});
            }

          }
        } else {
        	toastr.error(data.msg, 'Leads Push', {timeOut: 10000});
        }
				$(e).attr("disabled", false);
				$(e).removeClass("disabled");
      }
    });
}
