function submitClicksureForm(e) {
  var fname = $.trim($("#fname").val());
  var lname = $.trim($("#lname").val());
  var email = $.trim($("#email").val());
  var clicksure_id = $.trim($("#clicksure_id").val());
  var skype_id = $.trim($("#skype_id").val());
  var email_pattern = /^([a-z0-9_\-]+\.)*[a-z0-9_\-]+@([a-z0-9][a-z0-9\-]*[a-z0-9]\.)+[a-z]{2,4}$/i;
  var _token = $('input[name=_token]').val();
  if(fname !== "" &&  email_pattern.test(email) && clicksure_id !== "") {
    var send_data = {
      'fname': fname,
      'lname': lname,
      'email': email,
      'clicksure_id': clicksure_id,
      'skype_id': skype_id,
      '_token': _token
    };
    $.ajax({
      url: base_url + "/partners/jvpartner_applications",
      async: false,
      type: "POST",
      dataType: 'json',
      data: send_data,
      success: function(data) {
        if (data && data.status) {
          if(data.bean) {
            $("#fname").val(data.bean.fname);
            $("#lname").val(data.bean.lname);
            $("#email").val(data.bean.email);
            $("#clicksure_id").val(data.bean.clicksure_id);
            $("#skype_id").val(data.bean.skype_id);
          }
          Cookies.set('jva_fname', data.bean.fname, { expires: 7, path: '' });
          Cookies.set('jva_lname', data.bean.lname, { expires: 7, path: '' });
          Cookies.set('jva_email', data.bean.email, { expires: 7, path: '' });
          Cookies.set('jva_clicksure_id', data.bean.clicksure_id, { expires: 7, path: '' });
          Cookies.set('jva_skype_id', data.bean.skype_id, { expires: 7, path: '' });
          cookiesFormFiller();
          toastr.success("<p>Application successfully submitted</p>");
          setTimeout("location.reload()", 3000);
        } else {
          toastr.error("<p>" + data.msg + "</p>");
        }
      }
    });
  } else {
    error_msg = "";
    if(fname === '') {
      error_msg += "<p>Full name can't be empty</p>";
    }
    if(!email_pattern.test(email)) {
      $("#email").addClass('error');
      error_msg += "<p>Incorrect email format</p>";
    }
    if(clicksure_id === '') {
      error_msg += "<p>CLICKSURE ID can't be empty</p>";
    }
    toastr.error(error_msg);
  }
  return false;
}

function cookiesFormFiller() {
  var c_fname = Cookies.get('jva_fname');
  var c_lname = Cookies.get('jva_lname');
  var c_email = Cookies.get('jva_email');
  var c_clicksure_id = Cookies.get('jva_clicksure_id');
  var c_skype_id = Cookies.get('jva_skype_id');
  if(typeof(c_clicksure_id) !== "undefined") {
    $("#fname").val(c_fname);
    $("#lname").val(c_lname);
    $("#email").val(c_email);
    $("#clicksure_id").val(c_clicksure_id);
    $("#skype_id").val(c_skype_id);
    // $("#landing_page_ta_1").val("http://" + c_clicksure_id + ".opfigures.cpa.clicksure.com");
    // $("#landing_page_ta_2").val("http://" + c_clicksure_id + ".opfigures.cpa.clicksure.com");
    // $("#landing_page_ta_3").val("http://" + c_clicksure_id + ".opfigures.cpa.clicksure.com");
    // $("#landing_page_ta_4").val("http://" + c_clicksure_id + ".opfigures.cpa.clicksure.com");
    // $(".clicksure_id_email").text(c_clicksure_id);
  }
}

cookiesFormFiller();
