@extends('layout.calc')
@section('content')

<h2 class='section_title'>Indices Calculator</h2>
<p>Indices Historical Data</p>

<div class='rubix-panel-container'>
<div class='rubix-panel-header bg-lightred fg-white'>
  <div class='container-fluid rubix-grid'>
    <div class='row'>
      <div class='col-xs-12'>
        <ul id="indices_trade_navi" class='nav nav-tabs nav-justified nav-red mt_15'>
          <li class='b-tab active'>
            <a href="#indices_calc_1" data-chart-id="indices_chart_1" role="tab" data-toggle="tab">Dow 30</a>
          </li>
          <li class='b-tab'>
            <a href="#indices_calc_2" data-chart-id="indices_chart_2" role="tab" data-toggle="tab">S&P 500</a>
          </li>
          <li class='b-tab'>
            <a href="#indices_calc_3" data-chart-id="indices_chart_3" role="tab" data-toggle="tab">Nasdaq 100</a>
          </li>
          <li class='b-tab'>
            <a href="#indices_calc_4" data-chart-id="indices_chart_4" role="tab" data-toggle="tab">DAX 2</a>
          </li>
          <li class='b-tab'>
            <a href="#indices_calc_5" data-chart-id="indices_chart_5" role="tab" data-toggle="tab">FTSE100</a>
          </li>
          <li class='b-tab'>
            <a href="#indices_calc_6" data-chart-id="indices_chart_6" role="tab" data-toggle="tab">CAC40</a>
          </li>
          <li class='b-tab'>
            <a href="#indices_calc_7" data-chart-id="indices_chart_7" role="tab" data-toggle="tab">Nikkei 225</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class='rubix-panel-body'>
  <div class='container-fluid rubix-grid'>
    <div class='row'>
      <div class='col-xs-12'>
        <div class='tab-content'>
          <div class='tab-pane trade_charts_section curr_mode clearfix active' id="indices_calc_1">
            <div class='chart_side'>
              <div id='indices_chart_1'></div>
            </div>
            <div class='config_rside'>
              <p class='hl'>Calculate</p>
              <p class='desc'>
                Please Choose your favorite contract and timeslot and press run button
              </p>
              <div class='calc_opts_form'>
                <div class='calc_opt_line clearfix'>
                  <label>Select Contract</label>
                  <select class='form-control asset_contract w_200'>
                    <option value='0'>Select</option>
                    <option value='Dow 30'>Dow 30</option>
                    <option value='S&P 500'>S&P 500</option>
                    <option value='Nasdaq 100'>Nasdaq 100</option>
                    <option value='DAX 2'>DAX 2</option>
                    <option value='FTSE100'>FTSE100</option>
                    <option value='CAC40'>CAC40</option>
                    <option value='Nikkei 225'>Nikkei 225</option>
                  </select>
                </div>
                <div class='calc_opt_line clearfix'>
                  <label>Select Timeslot</label>
                  <select class='form-control asset_timeslot w_200'>
                    <option value='0'>Select</option>
                    <option value='1 Day'>1 Day</option>
                    <option value='1 Hour'>1 Hour</option>
                    <option value='15 Minutes'>15 Minutes</option>
                    <option value='5 Minute'>5 Minute</option>
                    <option value='1 Minute'>1 Minute</option>
                  </select>
                </div>
                <button type='button' onclick="startCalcModal()" class='btn btn btn-lg btn-success'>Calculate</button>
              </div>
            </div>
          </div>
          <div class='tab-pane trade_charts_section curr_mode clearfix' id="indices_calc_2">
            <div class='chart_side'>
              <div id='indices_chart_2'></div>
            </div>
            <div class='config_rside'>
              <p class='hl'>Calculate</p>
              <p class='desc'>
                Please Choose your favorite contract and timeslot and press run button
              </p>
              <div class='calc_opts_form'>
                <div class='calc_opt_line clearfix'>
                  <label>Select Contract</label>
                  <select class='form-control asset_contract w_200'>
                    <option value='0'>Select</option>
                    <option value='Dow 30'>Dow 30</option>
                    <option value='S&P 500'>S&P 500</option>
                    <option value='Nasdaq 100'>Nasdaq 100</option>
                    <option value='DAX 2'>DAX 2</option>
                    <option value='FTSE100'>FTSE100</option>
                    <option value='CAC40'>CAC40</option>
                    <option value='Nikkei 225'>Nikkei 225</option>
                  </select>
                </div>
                <div class='calc_opt_line clearfix'>
                  <label>Select Timeslot</label>
                  <select class='form-control asset_timeslot w_200'>
                    <option value='0'>Select</option>
                    <option value='1 Day'>1 Day</option>
                    <option value='1 Hour'>1 Hour</option>
                    <option value='15 Minutes'>15 Minutes</option>
                    <option value='5 Minute'>5 Minute</option>
                    <option value='1 Minute'>1 Minute</option>
                  </select>
                </div>
                <button type='button' onclick="startCalcModal()" class='btn btn btn-lg btn-success'>Calculate</button>
              </div>
            </div>
          </div>
          <div class='tab-pane trade_charts_section curr_mode clearfix' id="indices_calc_3">
            <div class='chart_side'>
              <div id='indices_chart_3'></div>
            </div>
            <div class='config_rside'>
              <p class='hl'>Calculate</p>
              <p class='desc'>
                Please Choose your favorite contract and timeslot and press run button
              </p>
              <div class='calc_opts_form'>
                <div class='calc_opt_line clearfix'>
                  <label>Select Contract</label>
                  <select class='form-control asset_contract w_200'>
                    <option value='0'>Select</option>
                    <option value='Dow 30'>Dow 30</option>
                    <option value='S&P 500'>S&P 500</option>
                    <option value='Nasdaq 100'>Nasdaq 100</option>
                    <option value='DAX 2'>DAX 2</option>
                    <option value='FTSE100'>FTSE100</option>
                    <option value='CAC40'>CAC40</option>
                    <option value='Nikkei 225'>Nikkei 225</option>
                  </select>
                </div>
                <div class='calc_opt_line clearfix'>
                  <label>Select Timeslot</label>
                  <select class='form-control asset_timeslot w_200'>
                    <option value='0'>Select</option>
                    <option value='1 Day'>1 Day</option>
                    <option value='1 Hour'>1 Hour</option>
                    <option value='15 Minutes'>15 Minutes</option>
                    <option value='5 Minute'>5 Minute</option>
                    <option value='1 Minute'>1 Minute</option>
                  </select>
                </div>
                <button type='button' onclick="startCalcModal()" class='btn btn btn-lg btn-success'>Calculate</button>
              </div>
            </div>
          </div>
          <div class='tab-pane trade_charts_section curr_mode clearfix' id="indices_calc_4">
            <div class='chart_side'>
              <div id='indices_chart_4'></div>
            </div>
            <div class='config_rside'>
              <p class='hl'>Calculate</p>
              <p class='desc'>
                Please Choose your favorite contract and timeslot and press run button
              </p>
              <div class='calc_opts_form'>
                <div class='calc_opt_line clearfix'>
                  <label>Select Contract</label>
                  <select class='form-control asset_contract w_200'>
                    <option value='0'>Select</option>
                    <option value='Dow 30'>Dow 30</option>
                    <option value='S&P 500'>S&P 500</option>
                    <option value='Nasdaq 100'>Nasdaq 100</option>
                    <option value='DAX 2'>DAX 2</option>
                    <option value='FTSE100'>FTSE100</option>
                    <option value='CAC40'>CAC40</option>
                    <option value='Nikkei 225'>Nikkei 225</option>
                  </select>
                </div>
                <div class='calc_opt_line clearfix'>
                  <label>Select Timeslot</label>
                  <select class='form-control asset_timeslot w_200'>
                    <option value='0'>Select</option>
                    <option value='1 Day'>1 Day</option>
                    <option value='1 Hour'>1 Hour</option>
                    <option value='15 Minutes'>15 Minutes</option>
                    <option value='5 Minute'>5 Minute</option>
                    <option value='1 Minute'>1 Minute</option>
                  </select>
                </div>
                <button type='button' onclick="startCalcModal()" class='btn btn btn-lg btn-success'>Calculate</button>
              </div>
            </div>
          </div>
          <div class='tab-pane trade_charts_section curr_mode clearfix' id="indices_calc_5">
            <div class='chart_side'>
              <div id='indices_chart_5'></div>
            </div>
            <div class='config_rside'>
              <p class='hl'>Calculate</p>
              <p class='desc'>
                Please Choose your favorite contract and timeslot and press run button
              </p>
              <div class='calc_opts_form'>
                <div class='calc_opt_line clearfix'>
                  <label>Select Contract</label>
                  <select class='form-control asset_contract w_200'>
                    <option value='0'>Select</option>
                    <option value='Dow 30'>Dow 30</option>
                    <option value='S&P 500'>S&P 500</option>
                    <option value='Nasdaq 100'>Nasdaq 100</option>
                    <option value='DAX 2'>DAX 2</option>
                    <option value='FTSE100'>FTSE100</option>
                    <option value='CAC40'>CAC40</option>
                    <option value='Nikkei 225'>Nikkei 225</option>
                  </select>
                </div>
                <div class='calc_opt_line clearfix'>
                  <label>Select Timeslot</label>
                  <select class='form-control asset_timeslot w_200'>
                    <option value='0'>Select</option>
                    <option value='1 Day'>1 Day</option>
                    <option value='1 Hour'>1 Hour</option>
                    <option value='15 Minutes'>15 Minutes</option>
                    <option value='5 Minute'>5 Minute</option>
                    <option value='1 Minute'>1 Minute</option>
                  </select>
                </div>
                <button type='button' onclick="startCalcModal()" class='btn btn btn-lg btn-success'>Calculate</button>
              </div>
            </div>
          </div>
          <div class='tab-pane trade_charts_section curr_mode clearfix' id="indices_calc_6">
            <div class='chart_side'>
              <div id='indices_chart_6'></div>
            </div>
            <div class='config_rside'>
              <p class='hl'>Calculate</p>
              <p class='desc'>
                Please Choose your favorite contract and timeslot and press run button
              </p>
              <div class='calc_opts_form'>
                <div class='calc_opt_line clearfix'>
                  <label>Select Contract</label>
                  <select class='form-control asset_contract w_200'>
                    <option value='0'>Select</option>
                    <option value='Dow 30'>Dow 30</option>
                    <option value='S&P 500'>S&P 500</option>
                    <option value='Nasdaq 100'>Nasdaq 100</option>
                    <option value='DAX 2'>DAX 2</option>
                    <option value='FTSE100'>FTSE100</option>
                    <option value='CAC40'>CAC40</option>
                    <option value='Nikkei 225'>Nikkei 225</option>
                  </select>
                </div>
                <div class='calc_opt_line clearfix'>
                  <label>Select Timeslot</label>
                  <select class='form-control asset_timeslot w_200'>
                    <option value='0'>Select</option>
                    <option value='1 Day'>1 Day</option>
                    <option value='1 Hour'>1 Hour</option>
                    <option value='15 Minutes'>15 Minutes</option>
                    <option value='5 Minute'>5 Minute</option>
                    <option value='1 Minute'>1 Minute</option>
                  </select>
                </div>
                <button type='button' onclick="startCalcModal()" class='btn btn btn-lg btn-success'>Calculate</button>
              </div>
            </div>
          </div>
          <div class='tab-pane trade_charts_section curr_mode clearfix' id="indices_calc_7">
            <div class='chart_side'>
              <div id='indices_chart_7'></div>
            </div>
            <div class='config_rside'>
              <p class='hl'>Calculate</p>
              <p class='desc'>
                Please Choose your favorite contract and timeslot and press run button
              </p>
              <div class='calc_opts_form'>
                <div class='calc_opt_line clearfix'>
                  <label>Select Contract</label>
                  <select class='form-control asset_contract w_200'>
                    <option value='0'>Select</option>
                    <option value='Dow 30'>Dow 30</option>
                    <option value='S&P 500'>S&P 500</option>
                    <option value='Nasdaq 100'>Nasdaq 100</option>
                    <option value='DAX 2'>DAX 2</option>
                    <option value='FTSE100'>FTSE100</option>
                    <option value='CAC40'>CAC40</option>
                    <option value='Nikkei 225'>Nikkei 225</option>
                  </select>
                </div>
                <div class='calc_opt_line clearfix'>
                  <label>Select Timeslot</label>
                  <select class='form-control asset_timeslot w_200'>
                    <option value='0'>Select</option>
                    <option value='1 Day'>1 Day</option>
                    <option value='1 Hour'>1 Hour</option>
                    <option value='15 Minutes'>15 Minutes</option>
                    <option value='5 Minute'>5 Minute</option>
                    <option value='1 Minute'>1 Minute</option>
                  </select>
                </div>
                <button type='button' onclick="startCalcModal()" class='btn btn btn-lg btn-success'>Calculate</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

@include('pages.calc.partials.brands', array('t' => 'Trade with Confidence'))

<script type="text/javascript">
  var indices_chart_1 = <?= $indices_chart_1 ?>;
  var indices_chart_2 = <?= $indices_chart_2 ?>;
  var indices_chart_3 = <?= $indices_chart_3 ?>;
  var indices_chart_4 = <?= $indices_chart_4 ?>;
  var indices_chart_5 = <?= $indices_chart_5 ?>;
  var indices_chart_6 = <?= $indices_chart_6 ?>;
  var indices_chart_7 = <?= $indices_chart_7 ?>;
</script>

@stop
