@extends('layout.calc')
@section('content')

<h2 class='section_title'>User Guide</h2>

<div class='rubix-panel-container user_guide'>
  <div class='rubix-panel-body'>
    <div class='container-fluid rubix-grid'>
      <div class='row'>
        <div class='col-xs-12'>
          <ul class='navi'>
            <li><a data-cid='s1' href="javascript: void(0)">What Is Option Figures?</a></li>
            <li><a data-cid='s2' href="javascript: void(0)">How Do I Get Started?</a></li>
            <li><a data-cid='s3' href="javascript: void(0)">Accessing Option Figures and Basic Features</a></li>
            <li><a data-cid='s4' href="javascript: void(0)">Signing Up with a Broker</a></li>
            <li><a data-cid='s5' href="javascript: void(0)">Using the Binary Options Calculator</a></li>
            <li><a data-cid='s6' href="javascript: void(0)">Funding Your Account</a></li>
          </ul>
        </div>
      </div>

      <div class='row content_area'>
        <div class='col-xs-12'>

          <div id='s1' class='content_area_segment'>
            <h4>What Is Option Figures?</h4>
            <div>
              <p>Option Figures is a unique binary trading system that is designed to make binary options trading easy and profitable for both seasoned traders and novices.</p>
              <p>This excellent system comes with a binary options calculator which uses integrated technical analysis methods, such as market trends, MACD, and RSI. These methods allow the calculator to make accurate predictions on an asset's performance over a given time frame.</p>
              <p>Option Figures’ super fast and efficient algorithms provide its users an advantage over other traders since it is optimised to capture and analyse  loads of information that may influence an asset's performance. It does all the number crunching by itself and let its users make money without doing anything. The result -- minimum risk, maximum profits, at the least amount of effort.</p>
            </div>
          </div>

          <div id='s2' class='content_area_segment'>
            <h4>How Do I Get Started?</h4>
            <div>
              <p>Option Figures is completely web-based, so getting started with trading binary  is as easy as 1, 2, 3. Simply go to <a href='http://option-figures.com/cb/'>http://option-figures.com/cb/</a>, click the ‘Subscribe Now’ button, and fill out the required information.  This will give you instant access to the system and you can start trading and making money in as short as 5 minutes.</p>
            </div>
          </div>

          <div id='s3' class='content_area_segment'>
            <h4>Accessing Option Figures and Basic Features</h4>
            <div>
              <p>After completing the subscription process, you will instantly have access to Option Figures. Remember the email address and password you set up during registration, as this will be your login credentials to the tool.</p>
              <p>Step 1: Login To Your Account</p>
              <p>To access the live interface, just go to <a href='http://option-figures.com/cb/calc/index/login'>http://option-figures.com/cb/calc/index/login</a>. Type in your email address and password, then click on the login button (see below).</p>
              <div class='text-center'>
                <img src="<?php echo url('/'); ?>/calc/images/user_guide/ug1.png">
              </div>
              <p>Step 2: The Dashboard Features</p>
              <p>The Dashboard has 3 main sections: Calculator Launcher for all 3 asset types, Trade Room, and a list of Recommended Brokers.</p>
              <p>Calculator Launcher:</p>
              <div class='text-center'>
                <img src="<?php echo url('/'); ?>/calc/images/user_guide/ug2.png">
              </div>
              <p>TradeRoom:</p>
              <div class='text-center'>
                <img src="<?php echo url('/'); ?>/calc/images/user_guide/ug3.png">
              </div>
              <p>Recommended Brokers:</p>
              <div class='text-center'>
                <img src="<?php echo url('/'); ?>/calc/images/user_guide/ug4.png">
              </div>
            </div>
          </div>

          <div id='s4' class='content_area_segment'>
            <h4>Signing Up with a Broker</h4>
            <div>
              <p>After signing in, you will be required to sign up with a broker. Option Figures has a list of recommended brokers who offers the highest possible leverage to its customers. You can then fund this account to start trading. To sign up with a broker, check the list of accepted countries first to ensure that your country is within scope.</p>
              <div class='text-center'>
                <img src="<?php echo url('/'); ?>/calc/images/user_guide/ug5.png">
              </div>
              <p>After confirming that your country is included in the list, click on the Step 1: Open Account button. A pop up will appear on your screen, fill out all the required information to open a free account with these brokers. See below:</p>
              <div class='text-center'>
                <img src="<?php echo url('/'); ?>/calc/images/user_guide/ug6.png">
              </div>
            </div>
          </div>

          <div id='s5' class='content_area_segment'>
            <h4>Using the Binary Options Calculator</h4>
            <div>
              <p>The binary options calculator can be launched from either the dashboard or the quick links from the left hand side of the main page. Just select the type of asset you want to trade in and click on the ‘Launch Calculator’ button, as seen below:</p>
              <div class='text-center'>
                <img src="<?php echo url('/'); ?>/calc/images/user_guide/ug7.png">
              </div>
              <p>After launching the calculator, specify the currency, commodity, or indices you want to trade on the ‘Select Contract’ dropdown menu. Then choose a time frame using the ‘Select Timeslot’ dropdown menu.</p>
              <p>Step 1: Select Contract</p>
              <div class='text-center'>
                <img src="<?php echo url('/'); ?>/calc/images/user_guide/ug8.png">
              </div>
              <p>Step 2: Select Timeslot</p>
              <div class='text-center'>
                <img src="<?php echo url('/'); ?>/calc/images/user_guide/ug9.png">
              </div>
              <p>Step 3: Let the calculator do its magic</p>
              <p>After specifying the contract and duration, click on the calculate button and the calculator will do the heavy lifting for you. In just a matter of seconds, it will predict whether the asset will go UP or Down, depending on technical factors.</p>
              <div class='text-center'>
                <img src="<?php echo url('/'); ?>/calc/images/user_guide/ug10.png">
              </div>
              <p>Remember, DO NOT refresh the page while the calculator is working. Once it’s done calculating results, a pop up will appear on your screen indicating whether the asset will go up or down at the end of the specified time frame. Using this intel, you can start trading right away.</p>
              <div class='text-center'>
                <img src="<?php echo url('/'); ?>/calc/images/user_guide/ug11.png">
              </div>
              <p>We know that a trader needs to make more right choices than wrong ones to be successful in trading. Option Figures makes this possible by doing all the required analysis for you.</p>
            </div>
          </div>

          <div id='s6' class='content_area_segment'>
            <h4>Funding Your Account</h4>
            <div>
              <p>Before you can start trading using the results from the Binary Options Calculator, remember to fund to the account with your preferred broker first. To do this, simply click on the ‘Fund Account button from the ‘Recommended Brokers’ section on the dashboard or directly from the results pop up screen.</p>
              <p>From dashboard</p>
              <div class='text-center'>
                <img src="<?php echo url('/'); ?>/calc/images/user_guide/ug12.png">
              </div>
              <p>Or the results popup screen</p>
              <div class='text-center'>
                <img src="<?php echo url('/'); ?>/calc/images/user_guide/ug13.png">
              </div>
            </div>
          </div>

        </div>
      </div>

    </div>
  </div>
</div>

@stop
