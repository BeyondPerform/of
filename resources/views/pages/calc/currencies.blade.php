@extends('layout.calc')
@section('content')

<h2 class='section_title'>Currencies Calculator</h2>
<p>Currencies Historical Data</p>

<div class='rubix-panel-container'>
<div class='rubix-panel-header bg-lightred fg-white'>
  <div class='container-fluid rubix-grid'>
    <div class='row'>
      <div class='col-xs-12'>
        <ul id="currency_trade_navi" class='nav nav-tabs nav-justified nav-red mt_15'>
          <li class='b-tab active'>
            <a href="#curr_calc_1" data-chart-id="curr_chart_1" role="tab" data-toggle="tab">AUD/USD</a>
          </li>
          <li class='b-tab'>
            <a href="#curr_calc_2" data-chart-id="curr_chart_2" role="tab" data-toggle="tab">EUR/GBP</a>
          </li>
          <li class='b-tab'>
            <a href="#curr_calc_3" data-chart-id="curr_chart_3" role="tab" data-toggle="tab">EUR/USD</a>
          </li>
          <li class='b-tab'>
            <a href="#curr_calc_4" data-chart-id="curr_chart_4" role="tab" data-toggle="tab">GBP/USD</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class='rubix-panel-body'>
  <div class='container-fluid rubix-grid'>
    <div class='row'>
      <div class='col-xs-12'>
        <div class='tab-content'>
          <div class='tab-pane trade_charts_section curr_mode clearfix active' id="curr_calc_1">
            <div class='chart_side'>
              <div id='curr_chart_1'></div>
            </div>
            <div class='config_rside'>
              <p class='hl'>Calculate</p>
              <p class='desc'>
                Please Choose your favorite contract and timeslot and press run button
              </p>
              <div class='calc_opts_form'>
                <div class='calc_opt_line clearfix'>
                  <label>Select Contract</label>
                  <select class='form-control asset_contract w_200'>
                    <option value='EUR/USD'>EUR/USD</option>
                    <option value='USD/JPY'>USD/JPY</option>
                    <option value='USD/CHF'>USD/CHF</option>
                    <option value='USD/CAD'>USD/CAD</option>
                    <option value='EUR/JPY'>EUR/JPY</option>
                    <option value='AUD/USD'>AUD/USD</option>
                    <option value='NZD/USD'>NZD/USD</option>
                    <option value='EUR/GBP'>EUR/GBP</option>
                    <option value='EUR/CHF'>EUR/CHF</option>
                  </select>
                </div>
                <div class='calc_opt_line clearfix'>
                  <label>Select Timeslot</label>
                  <select class='form-control asset_timeslot w_200'>
                    <option value='1 Day'>1 Day</option>
                    <option value='1 Hour'>1 Hour</option>
                    <option value='15 Minutes'>15 Minutes</option>
                    <option value='5 Minute'>5 Minute</option>
                    <option value='1 Minute'>1 Minute</option>
                  </select>
                </div>
                <button type='button' onclick="startCalcModal()" class='btn btn btn-lg btn-success'>Calculate</button>
              </div>
            </div>
          </div>
          <div class='tab-pane trade_charts_section curr_mode clearfix' id="curr_calc_2">
            <div class='chart_side'>
              <div id='curr_chart_2'></div>
            </div>
            <div class='config_rside'>
              <p class='hl'>Calculate</p>
              <p class='desc'>
                Please Choose your favorite contract and timeslot and press run button
              </p>
              <div class='calc_opts_form'>
                <div class='calc_opt_line clearfix'>
                  <label>Select Contract</label>
                  <select class='form-control asset_contract w_200'>
                    <option value='EUR/USD'>EUR/USD</option>
                    <option value='USD/JPY'>USD/JPY</option>
                    <option value='USD/CHF'>USD/CHF</option>
                    <option value='USD/CAD'>USD/CAD</option>
                    <option value='EUR/JPY'>EUR/JPY</option>
                    <option value='AUD/USD'>AUD/USD</option>
                    <option value='NZD/USD'>NZD/USD</option>
                    <option value='EUR/GBP'>EUR/GBP</option>
                    <option value='EUR/CHF'>EUR/CHF</option>
                  </select>
                </div>
                <div class='calc_opt_line clearfix'>
                  <label>Select Timeslot</label>
                  <select class='form-control asset_timeslot w_200'>
                    <option value='1 Day'>1 Day</option>
                    <option value='1 Hour'>1 Hour</option>
                    <option value='15 Minutes'>15 Minutes</option>
                    <option value='5 Minute'>5 Minute</option>
                    <option value='1 Minute'>1 Minute</option>
                  </select>
                </div>
                <button type='button' onclick="startCalcModal()" class='btn btn btn-lg btn-success'>Calculate</button>
              </div>
            </div>
          </div>
          <div class='tab-pane trade_charts_section curr_mode clearfix' id="curr_calc_3">
            <div class='chart_side'>
              <div id='curr_chart_3'></div>
            </div>
            <div class='config_rside'>
              <p class='hl'>Calculate</p>
              <p class='desc'>
                Please Choose your favorite contract and timeslot and press run button
              </p>
              <div class='calc_opts_form'>
                <div class='calc_opt_line clearfix'>
                  <label>Select Contract</label>
                  <select class='form-control asset_contract w_200'>
                    <option value='EUR/USD'>EUR/USD</option>
                    <option value='USD/JPY'>USD/JPY</option>
                    <option value='USD/CHF'>USD/CHF</option>
                    <option value='USD/CAD'>USD/CAD</option>
                    <option value='EUR/JPY'>EUR/JPY</option>
                    <option value='AUD/USD'>AUD/USD</option>
                    <option value='NZD/USD'>NZD/USD</option>
                    <option value='EUR/GBP'>EUR/GBP</option>
                    <option value='EUR/CHF'>EUR/CHF</option>
                  </select>
                </div>
                <div class='calc_opt_line clearfix'>
                  <label>Select Timeslot</label>
                  <select class='form-control asset_timeslot w_200'>
                    <option value='1 Day'>1 Day</option>
                    <option value='1 Hour'>1 Hour</option>
                    <option value='15 Minutes'>15 Minutes</option>
                    <option value='5 Minute'>5 Minute</option>
                    <option value='1 Minute'>1 Minute</option>
                  </select>
                </div>
                <button type='button' onclick="startCalcModal()" class='btn btn btn-lg btn-success'>Calculate</button>
              </div>
            </div>
          </div>
          <div class='tab-pane trade_charts_section curr_mode clearfix' id="curr_calc_4">
            <div class='chart_side'>
              <div id='curr_chart_4'></div>
            </div>
            <div class='config_rside'>
              <p class='hl'>Calculate</p>
              <p class='desc'>
                Please Choose your favorite contract and timeslot and press run button
              </p>
              <div class='calc_opts_form'>
                <div class='calc_opt_line clearfix'>
                  <label>Select Contract</label>
                  <select class='form-control asset_contract w_200'>
                    <option value='EUR/USD'>EUR/USD</option>
                    <option value='USD/JPY'>USD/JPY</option>
                    <option value='USD/CHF'>USD/CHF</option>
                    <option value='USD/CAD'>USD/CAD</option>
                    <option value='EUR/JPY'>EUR/JPY</option>
                    <option value='AUD/USD'>AUD/USD</option>
                    <option value='NZD/USD'>NZD/USD</option>
                    <option value='EUR/GBP'>EUR/GBP</option>
                    <option value='EUR/CHF'>EUR/CHF</option>
                  </select>
                </div>
                <div class='calc_opt_line clearfix'>
                  <label>Select Timeslot</label>
                  <select class='form-control asset_timeslot w_200'>
                    <option value='1 Day'>1 Day</option>
                    <option value='1 Hour'>1 Hour</option>
                    <option value='15 Minutes'>15 Minutes</option>
                    <option value='5 Minute'>5 Minute</option>
                    <option value='1 Minute'>1 Minute</option>
                  </select>
                </div>
                <button type='button' onclick="startCalcModal()" class='btn btn btn-lg btn-success'>Calculate</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

@include('pages.calc.partials.brands', array('t' => 'Trade with Confidence'))

<script type="text/javascript">
  var curr_chart_1 = <?= $curr_chart_1 ?>;
  var curr_chart_2 = <?= $curr_chart_2 ?>;
  var curr_chart_3 = <?= $curr_chart_3 ?>;
  var curr_chart_4 = <?= $curr_chart_4 ?>;
</script>

@stop
