@extends('layout.calc')
@section('content')

<h2 class='section_title'>Dashboard</h2>

<div class='dashboard_navi_holder'>

  <div class='container-fluid rubix-grid zero_padds'>
    <div class='row'>

      <div class='col-xs-12 col-sm-4 col-md-4'>
        <div class='rubix-panel-container'>
          <div class='rubix-panel'>
            <div class='rubix-panel-body'>
              <div class='container-fluid rubix-grid'>
                <div class='row'>
                  <div class='col-xs-12'>
                    <p class='hl'>Currencies</p>
                  </div>
                </div>
              </div>
            </div>
            <div class='rubix-panel-footer bg-red'>
              <div class='container-fluid rubix-grid'>
                <div class='row'>
                  <div class='col-xs-12'>
                    <div class='text-center'>
                      <a href="<?php echo url('/'); ?>/calculator/currencies" class='btn btn-lg btn-success'>Launch Calculator</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class='col-xs-12 col-sm-4 col-md-4'>
        <div class='rubix-panel-container'>
          <div class='rubix-panel'>
            <div class='rubix-panel-body'>
              <div class='container-fluid rubix-grid'>
                <div class='row'>
                  <div class='col-xs-12'>
                    <p class='hl'>Commodities</p>
                  </div>
                </div>
              </div>
            </div>
            <div class='rubix-panel-footer bg-red'>
              <div class='container-fluid rubix-grid'>
                <div class='row'>
                  <div class='col-xs-12'>
                    <div class='text-center'>
                      <a href="<?php echo url('/'); ?>/calculator/commodities" class='btn btn-lg btn-success'>Launch Calculator</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class='col-xs-12 col-sm-4 col-md-4'>
        <div class='rubix-panel-container'>
          <div class='rubix-panel'>
            <div class='rubix-panel-body'>
              <div class='container-fluid rubix-grid'>
                <div class='row'>
                  <div class='col-xs-12'>
                    <p class='hl'>Indices</p>
                  </div>
                </div>
              </div>
            </div>
            <div class='rubix-panel-footer bg-red'>
              <div class='container-fluid rubix-grid'>
                <div class='row'>
                  <div class='col-xs-12'>
                    <div class='text-center'>
                      <a href="<?php echo url('/'); ?>/calculator/indices" class='btn btn-lg btn-success'>Launch Calculator</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>

</div>

<div class='rubix-panel-container'>
<div class='rubix-panel-header bg-lightred fg-white'>
  <div class='container-fluid rubix-grid'>
    <div class='row'>
      <div class='col-xs-12'>
        <h3>TradeRoom</h3>
        <ul id="dashboard_trade_navi" class='nav nav-tabs nav-justified nav-red'>
          <li class='b-tab active'>
            <a href="#trade_room_1" data-chart-id="dash_chart_1" role="tab" data-toggle="tab">60 Seconds</a>
          </li>
          <li class='b-tab'>
            <a href="#trade_room_2" data-chart-id="dash_chart_2" role="tab" data-toggle="tab">Touch</a>
          </li>
          <li class='b-tab'>
            <a href="#trade_room_3" data-chart-id="dash_chart_3" role="tab" data-toggle="tab">Call/put</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class='rubix-panel-body'>
  <div class='container-fluid rubix-grid'>
    <div class='row'>
      <div class='col-xs-12'>
        <div class='tab-content'>
          <div class='tab-pane trade_charts_section clearfix active' id="trade_room_1">
            <div class='chart_side'>
              <div id='dash_chart_1'></div>
            </div>
            <div class='config_rside'>
              <p class='hl'>Place Trade</p>
              <form>
                <div class='text-center mb_20'>
                  <input type='text' class='form-control text-center inline_block w_150' value='250$'>
                </div>
                <div class='text-center'>
                  <button type='submit' class='btn btn btn-lg btn-success' onclick='return false;'>Place Trade</button>
                </div>
              </form>
            </div>
          </div>
          <div class='tab-pane trade_charts_section clearfix' id="trade_room_2">
            <div class='chart_side'>
              <div id='dash_chart_2'></div>
            </div>
            <div class='config_rside'>
              <p class='hl'>Place Trade</p>
              <form>
                <div class='text-center mb_20'>
                  <input type='text' class='form-control text-center inline_block w_150' value='250$'>
                </div>
                <div class='text-center'>
                  <button type='submit' class='btn btn btn-lg btn-success' onclick='return false;'>Place Trade</button>
                </div>
              </form>
            </div>
          </div>
          <div class='tab-pane trade_charts_section clearfix' id="trade_room_3">
            <div class='chart_side'>
              <div id='dash_chart_3'></div>
            </div>
            <div class='config_rside'>
              <p class='hl'>Place Trade</p>
              <form>
                <div class='text-center mb_20'>
                  <input type='text' class='form-control text-center inline_block w_150' value='250$'>
                </div>
                <div class='text-center'>
                  <button type='submit' class='btn btn btn-lg btn-success' onclick='return false;'>Place Trade</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<script type="text/javascript">
  var dash_chart_1 = <?= $dash_chart_1 ?>;
  var dash_chart_2 = <?= $dash_chart_2 ?>;
  var dash_chart_3 = <?= $dash_chart_3 ?>;
</script>

@stop
