@extends('layout.calc')
@section('content')

<div class='rubix-panel-container'>
  <div class='rubix-panel-header bg-lightred fg-white'>
    <div class='container-fluid rubix-grid'>
      <div class='row'>
        <div class='col-xs-12'>
          <h3>Exclusive Promotions</h3>
        </div>
      </div>
    </div>
  </div>
  <div class='rubix-panel-body'>
    <div class='container-fluid rubix-grid'>
      <div class='row'>
        <div class='col-xs-12'>

          <div class='ex_promo_wrap'>
            <div class="row line_wr">
              <div class="col-xs-3 col-sm-3 col-md-3 logo">
                <img src="{{ URL::asset('images/of_logo_dark.png') }}">
              </div>
              <div class="col-xs-3 col-sm-8 col-md-8 desc">
                <p>
                  Option-Figures.com Asset Calculator is a powerful, easy-to-use tool to start making real money online. Even if you don’t know the first thing about Binary Options Trading, you can start in a matter of minutes. Nothing to learn, no experience required.
                </p>
              </div>
              <div class="col-xs-3 col-sm-1 col-md-1">
                <div class="cnt">
                  <a class='btn btn-success' href="http://beyondtrkk.com/path/lp.php?trvid=10026&trvx=8b1e3a7f" target="_blank">visit</a>
                </div>
              </div>
            </div>

            <div class="row line_wr">
              <div class="col-xs-3 col-sm-3 col-md-3 logo">
                <img src="{{ URL::asset('images/millionaire-blueprint.png') }}" style="width: 180px;">
              </div>
              <div class="col-xs-3 col-sm-8 col-md-8 desc">
                <p>
                  The Millionaire Blueprint is an extremely popular binary trading platform. Upon first glance at the site, you are met with the introductory video that details the platform and gives examples of traders who have had success trading with this automated binary trading robot
                </p>
              </div>
              <div class="col-xs-3 col-sm-1 col-md-1">
                <div class="cnt">
                  <a class='btn btn-success' href="http://beyondtrkk.com/path/lp.php?trvid=10027&trvx=bea31354" target="_blank">visit</a>
                </div>
              </div>
            </div>

            <div class="row line_wr">
              <div class="col-xs-3 col-sm-3 col-md-3 logo">
                <img src="{{ URL::asset('images/pushmoneylogo.png') }}" style="width: 180px;">
              </div>
              <div class="col-xs-3 col-sm-8 col-md-8 desc">
                <p>
                  Push Money App is a highly advanced money making system. Created by Dennis Moreland and Mike Callahan, Push Money App claims not only does it generate trades and place them on our behalf; it does it all.
                </p>
              </div>
              <div class="col-xs-3 col-sm-1 col-md-1">
                <div class="cnt">
                  <a class='btn btn-success' href="http://beyondtrkk.com/path/lp.php?trvid=10028&trvx=b8abf95b" target="_blank">visit</a>
                </div>
              </div>
            </div>

            <div class="row line_wr">
              <div class="col-xs-3 col-sm-3 col-md-3 logo">
                <img src="{{ URL::asset('images/buffett_campaign_logo.png') }}" style="width: 180px;">
              </div>
              <div class="col-xs-3 col-sm-8 col-md-8 desc">
                <p>
                  This system created by software developer Jeremy Fin. claims to earn traders profits of $1978 a day, using the insider trading secrets of one of the most renown financial genius, and self-made multi-millionaire, Warren Buffett.
                </p>
              </div>
              <div class="col-xs-3 col-sm-1 col-md-1">
                <div class="cnt">
                  <a class='btn btn-success' href="http://beyondtrkk.com/path/lp.php?trvid=10029&trvx=e855e805" target="_blank">visit</a>
                </div>
              </div>
            </div>

            <div class="row line_wr">
              <div class="col-xs-3 col-sm-3 col-md-3 logo">
                <img src="{{ URL::asset('images/neo2-logo-300x105.png') }}" style="width: 180px;">
              </div>
              <div class="col-xs-3 col-sm-8 col-md-8 desc">
                <p>
                  Dr. Jack Piers is the founder of this Neo2 software, alongside the lead programmer, Amit Gupta and binary options strategist, Michael Freeman. Freeman is an auto-trading expert and also a mentor by profession. The trio have created one of the most powerful tools that will transform the industry as far as trading binary options software is concerned.
                </p>
              </div>
              <div class="col-xs-3 col-sm-1 col-md-1">
                <div class="cnt">
                  <a class='btn btn-success' href="http://beyondtrkk.com/path/lp.php?trvid=10030&trvx=5a5f9cc6" target="_blank">visit</a>
                </div>
              </div>
            </div>

            <div class="row line_wr">
              <div class="col-xs-3 col-sm-3 col-md-3 logo">
                <img src="{{ URL::asset('images/daily_profit.png') }}">
              </div>
              <div class="col-xs-3 col-sm-8 col-md-8 desc">
                <p>
                  Created by John Becker, 1K Daily Profit is a binary options automated trading system that has recently been released. Similar to other automated trading robots, it is user-friendly. Traders simply need to set their parameters such as risk level and investment amount.
                </p>
              </div>
              <div class="col-xs-3 col-sm-1 col-md-1">
                <div class="cnt">
                  <a class='btn btn-success' href="http://beyondtrkk.com/path/lp.php?trvid=10031&trvx=b1f3dcc5" target="_blank">visit</a>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>

@stop
