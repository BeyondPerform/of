@extends('layout.calc_payment')
@section('content')
  <div class='payment_root'>
    <!-- <a class="logout" href="<?php echo url('/'); ?>/logout"><span class="fa fa-sign-out"></span></a> -->

    <div class="modal creditcard_creation_modal fade in" aria-hidden="false">
  		<div class="modal-dialog">
  			<div class="modal-content">
  				<div class="modal-header">
            <div class='intro'>
              <div class="get_started_bage"><img src="<?php echo url('/'); ?>/images/get_started_bage.png"></div>
              <p class='price'>47.00 / Month</p>
              <p>Thank you for registering with OptionFigures, the leading binary options asset calculator for binary options and forex traders.</p>
              <p>Remember, if you join our monthly membership today, you will not only have access to our leading asset calculator, but you will receive up-to-date information about strategies, upcoming webinars, promotions and important trading announcements.</p>
              <p>Those who complete their membership registration today will also receive an additional 7-day E-mail Introduction to Binary Options course for FREE!</p>
              <p>Here at OptionFigures, we have a "No questions asked" strict 14 days money back guarantee policy, for all our products under our membership. You have the option to cancel your membership in any given time via our member's support system.</p>
            </div>
  				</div>
  				<div class="modal-body">
  					<div class="card_type_holder"></div>
  					<div class="cc_form_segment">
  						<p class="hl">Payment Method</p>
  					  <ul class="nav nav-tabs" role="tablist">
  					    <li role="presentation" class="active"><a href="#credit_card_payment" aria-controls="credit_card_payment" role="tab" data-toggle="tab">Credit/Debit Card</a></li>
  					  </ul>
  					  <div class="tab-content">
  					    <div role="tabpanel" class="tab-pane active" id="credit_card_payment">
  								<form id="susbscribe_form">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="client_token" id="client_token" value="<?php echo $client_token; ?>">
  									<div class="cc_form_segment">
  										<div class="cc_form_wrap">
  											<div class="cc_form_line clearfix">
  												<label>Credit Card: <span class="req">*</span></label>
  												<div class="two_rows_fields c_card_line clearfix">
  													<input type="text" placeholder="Card number" id="cc_cardnumber" class="form-control cc_cardnumber">
  													<input type="text" autocomplete="on" placeholder="CVV2" id="cc_cvv" class="form-control cc_cvv">
  												</div>
  											</div>
  											<div class="cc_form_line clearfix">
  												<label>Expiration (MM/YYYY): <span class="req">*</span></label>
  												<div><input type="text" placeholder="MM/YYYY" id="cc_expire" class="form-control cc_expire"></div>
  											</div>
  										</div>
  									</div>
  									<div class="cc_form_cnt clearfix">
  										<div class="pull-left clearfix">
  											<div class="brain_signature clearfix">
  												<div class="lock"><img src="<?php echo url(); ?>/images/pay_modal_lock.png"></div>
  												<p>Payments by <span>Braintree</span></p>
  											</div>
  											<p class="paypal_loader pull-right">
  												<span class="fa fa-spinner fa-spin"></span>
  												<span>Please wait ...</span>
  											</p>
  										</div>
  										<div class="pull-right cc_cnt">
  											<button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
  											<button type="submit" onclick="return subscribeAccount(this)" class="btn btn-primary">Purchase</button>
  										</div>
  									</div>
  									<div class="cc_form_cnt clearfix">
  										<div class="pay_cards clearfix">
  											<div class="card"><img src="<?php echo url('/'); ?>/images/pc_mastercard.jpg"></div>
  											<div class="card"><img src="<?php echo url('/'); ?>/images/pc_maestro.jpg"></div>
  											<div class="card"><img src="<?php echo url('/'); ?>/images/pc_visa.jpg"></div>
  											<div class="card"><img src="<?php echo url('/'); ?>/images/pc_discover.jpg"></div>
  											<div class="card"><img src="<?php echo url('/'); ?>/images/pc_visa_electron.jpg"></div>
  											<div class="card"><img src="<?php echo url('/'); ?>/images/pc_western.jpg"></div>
  											<div class="card"><img src="<?php echo url('/'); ?>/images/pc_american.jpg"></div>
  											<div class="card"><img src="<?php echo url('/'); ?>/images/pc_delta.jpg"></div>
  											<div class="card"><img src="<?php echo url('/'); ?>/images/pc_solo.jpg"></div>
  											<div class="card"><img src="<?php echo url('/'); ?>/images/pc_cirrus.jpg"></div>
  										</div>
  									</div>
  								</form>
  							</div>
  					  </div>
  					</div>

  				</div>
  			</div>
  		</div>
  	</div>

    <!-- <div class='shadow'></div> -->
    <!-- <img src="<?php echo url('/'); ?>/images/calc_placeholder.png"> -->
  </div>

@stop
