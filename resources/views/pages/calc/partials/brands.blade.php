<div class='rubix-panel-container'>
	<div class='rubix-panel-header bg-lightred fg-white'>
		<div class='container-fluid rubix-grid'>
			<div class='row'>
				<div class='col-xs-12'>
					<h3><?php echo $t; ?></h3>
				</div>
			</div>
		</div>
	</div>
	<div class='rubix-panel-body'>
		<div class='container-fluid rubix-grid'>
			<div class='row'>
				<div class='col-xs-12'>
					<div class='promotions_section'>
						<div class='promo_acc_wrap'>


							<div class='promo_line_wr clearfix'>
	              <div class='logo_wr'>
	                <div class='logo_placeholder'>
	                  <img src="{{ URL::asset('images/avatrade.png') }}">
	                </div>
	              </div>
	              <p class='title'>
	                AvaTrade<br>
	                <a href='javascript: void(0)' data-toggle="modal" data-target="#avatrade_acc_countries">Accepted countries</a>
	              </p>
	              <p class='discount'>100% Bonus</p>
	              <div class='cnt'>
	                <div class='clearfix'>
	                  <a class='step1' href="http://beyondtrkk.com/path/lp.php?trvid=10025&trvx=5f4e17e5" target="_blank">
	                    <p>Step 1</p>
	                    <p>Open Account</p>
	                  </a>
	                  <a class='step2' href="javascript: void(0)">
	                    <p>Step 2</p>
	                    <p>Fund Account</p>
	                  </a>
	                </div>
	              </div>
	            </div>

							<div class="promo_line_wr clearfix">
	              <div class="logo_wr">
	                <div class="logo_placeholder">
	                  <img src="<?php echo url(); ?>/images/option500.png">
	                </div>
	              </div>
	              <p class="title">
	                Option500<br>
	                <a href="javascript: void(0)" data-toggle="modal" data-target="#option500_acc_countries">Accepted countries</a>
	              </p>
	              <p class="discount">100% Bonus</p>

	              <div class='cnt'>
	                <div class='clearfix'>
	                  <a class='step1' href="javascript: void(0)" onclick="activateApiCallModal('Option500')">
	                    <p>Step 1</p>
	                    <p>Open Account</p>
	                  </a>
	                  <a class='step2' href='https://www.option500.com/banking/?utm_medium=referral&utm_source=pNxmnOXYO8&affiliateId=pNxmnOXYO8&campaignId=176&a_aid=dfc8a48e&serial=Deposit_Page&affParams1={OUR_LEAD_ID}' target="_blank">
	                    <p>Step 2</p>
	                    <p>Fund Account</p>
	                  </a>
	                </div>
	              </div>
	            </div>

	            <div class="promo_line_wr clearfix">
	              <div class="logo_wr">
	                <div class="logo_placeholder">
	                  <img src="<?php echo url(); ?>/images/trade_solid.png">
	                </div>
	              </div>
	              <p class="title">
	                TradeSolid<br>
	                <a href="javascript: void(0)" data-toggle="modal" data-target="#tradesolid_acc_countries">Accepted countries</a>
	              </p>
	              <p class="discount">100% Bonus</p>

	              <div class='cnt'>
	                <div class='clearfix'>
	                  <a class='step1' href="javascript: void(0)" onclick="activateApiCallModal('TradeSolid')">
	                    <p>Step 1</p>
	                    <p>Open Account</p>
	                  </a>
	                  <a class='step2' href='https://www.tradesolid.com/my-account/?activeZone=deposit' target="_blank">
	                    <p>Step 2</p>
	                    <p>Fund Account</p>
	                  </a>
	                </div>
	              </div>
	            </div>

	            <div class="promo_line_wr clearfix">
	              <div class="logo_wr">
	                <div class="logo_placeholder" style="background-color: #273135; text-align: center">
	                  <img src="{{ URL::asset('images/topoption.png') }}">
	                </div>
	              </div>
	              <p class="title">
	                TopOption<br>
	                <a href="javascript: void(0)" data-toggle="modal" data-target="#topoption_acc_countries">Accepted countries</a>
	              </p>
	              <p class="discount">100% Bonus</p>
	              <div class="cnt">
	                <div class="clearfix">
	                  <a class="step1" href="http://beyondtrkk.com/path/lp.php?trvid=10024&trvx=36ed18f8" target="_blank">
	                    <p>Step 1</p>
	                    <p>Open Account</p>
	                  </a>
	                  <a class="step2" href="javascript: void(0)">
	                    <p>Step 2</p>
	                    <p>Fund Account</p>
	                  </a>
	                </div>
	              </div>
	            </div>

	            <div class="promo_line_wr clearfix">
	              <div class="logo_wr">
	                <div class="logo_placeholder">
	                  <img src="{{ URL::asset('images/tradecom.png') }}">
	                </div>
	              </div>
	              <p class="title">
	                Trade.com<br>
	                <a href="javascript: void(0)" data-toggle="modal" data-target="#tradecom_acc_countries">Accepted countries</a>
	              </p>
	              <p class="discount">100% Bonus</p>
	              <div class="cnt">
	                <div class="clearfix">
	                  <a class="step1" href="http://beyondtrkk.com/path/lp.php?trvid=10032&trvx=cd7c7d8b" target="_blank">
	                    <p>Step 1</p>
	                    <p>Open Account</p>
	                  </a>
	                  <a class="step2" href="javascript: void(0)">
	                    <p>Step 2</p>
	                    <p>Fund Account</p>
	                  </a>
	                </div>
	              </div>
	            </div>

	            <div class="promo_line_wr clearfix">
	              <div class="logo_wr">
	                <div class="logo_placeholder">
	                  <img src="{{ URL::asset('images/KeyOptionLogo2.png') }}">
	                </div>
	              </div>
	              <p class="title">
	                Keyoption<br>
	                <a href="javascript: void(0)" data-toggle="modal" data-target="#keyoption_acc_countries">Accepted countries</a>
	              </p>
	              <p class="discount">100% Bonus</p>
	              <div class="cnt">
	                <div class="clearfix">
	                  <a class="step1" href="javascript: void(0)" onclick="activateApiCallModal('Keyoption')">
	                    <p>Step 1</p>
	                    <p>Open Account</p>
	                  </a>
	                  <a class="step2" href="javascript: void(0)">
	                    <p>Step 2</p>
	                    <p>Fund Account</p>
	                  </a>
	                </div>
	              </div>
	            </div>

	            <div class="promo_line_wr clearfix">
	              <div class="logo_wr">
	                <div class="logo_placeholder">
	                  <img src="{{ URL::asset('images/option_live.png') }}">
	                </div>
	              </div>
	              <p class="title">
	                OptionLive<br>
	                <a href="javascript: void(0)" data-toggle="modal" data-target="#optionlive_acc_countries">Accepted countries</a>
	              </p>
	              <p class="discount">100% Bonus</p>
	              <div class="cnt">
	                <div class="clearfix">
	                  <a class="step1" href="javascript: void(0)" onclick="activateApiCallModal('OptionLive')">
	                    <p>Step 1</p>
	                    <p>Open Account</p>
	                  </a>
	                  <a class="step2" href="javascript: void(0)">
	                    <p>Step 2</p>
	                    <p>Fund Account</p>
	                  </a>
	                </div>
	              </div>
	            </div>


							<!-- <div class='promo_line_wr clearfix'>
								<p class='counter'>1.</p>
								<div class='logo_wr'>
									<div class='logo_placeholder'>
										<img src="<?php echo url('/'); ?>/calc/images/zoomtrader137x66.png">
									</div>
								</div>
								<p class='title'>
									Zoom Trader<br>
									<a href='javascript: void(0)' data-toggle="modal" data-target="#zoomtrader_acc_countries">Accepted countries</a>
								</p>
								<p class='discount'>100% Bonus</p>
								<div class='cnt'>
									<div class='clearfix'>
										<a class='step1' href='javascript: void(0)' onclick="activateApiCallModal('Zoom Trader')">
											<p>Step 1</p>
											<p>Open Account</p>
										</a>
										<a class='step2' href='https://www.zoomtraderglobal.com/deposit'>
											<p>Step 2</p>
											<p>Fund Account</p>
										</a>
									</div>
								</div>
							</div>

							<div class='promo_line_wr clearfix'>
								<p class='counter'>2.</p>
								<div class='logo_wr'>
									<div class='logo_placeholder'>
										<img src="<?php echo url('/'); ?>/calc/images/optionstars-logo.png">
									</div>
								</div>
								<p class='title'>
									OptionStars<br>
									<a href='javascript: void(0)' data-toggle="modal" data-target="#optionstars_acc_countries">Accepted countries</a>
								</p>
								<p class='discount'>100% Bonus</p>
								<div class='cnt'>
									<div class='clearfix'>
										<a class='step1' href='javascript: void(0)' onclick="activateApiCallModal('OptionStars')">
											<p>Step 1</p>
											<p>Open Account</p>
										</a>
										<a class='step2' href='javascript:void(0)'>
											<p>Step 2</p>
											<p>Fund Account</p>
										</a>
									</div>
								</div>
							</div> -->

			     	</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@include('pages.brand_ac_modals')
