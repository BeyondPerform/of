@extends('layout.calc')
@section('content')

<h2 class='section_title'>Commodities Calculator</h2>
<p>Commodities Historical Data</p>

<div class='rubix-panel-container'>
<div class='rubix-panel-header bg-lightred fg-white'>
  <div class='container-fluid rubix-grid'>
    <div class='row'>
      <div class='col-xs-12'>
        <ul id="comm_trade_navi" class='nav nav-tabs nav-justified nav-red mt_15'>
          <li class='b-tab active'>
            <a href="#curr_calc_1" data-chart-id="comm_chart_1" role="tab" data-toggle="tab">GOLD</a>
          </li>
          <li class='b-tab'>
            <a href="#curr_calc_2" data-chart-id="comm_chart_2" role="tab" data-toggle="tab">SILVER</a>
          </li>
          <li class='b-tab'>
            <a href="#curr_calc_3" data-chart-id="comm_chart_3" role="tab" data-toggle="tab">COPPER</a>
          </li>
          <li class='b-tab'>
            <a href="#curr_calc_4" data-chart-id="comm_chart_4" role="tab" data-toggle="tab">CRUDE OIL</a>
          </li>
          <li class='b-tab'>
            <a href="#curr_calc_5" data-chart-id="comm_chart_5" role="tab" data-toggle="tab">NATURAL GAS</a>
          </li>
          <li class='b-tab'>
            <a href="#curr_calc_6" data-chart-id="comm_chart_6" role="tab" data-toggle="tab">US WHEAT</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class='rubix-panel-body'>
  <div class='container-fluid rubix-grid'>
    <div class='row'>
      <div class='col-xs-12'>
        <div class='tab-content'>
          <div class='tab-pane trade_charts_section curr_mode clearfix active' id="curr_calc_1">
            <div class='chart_side'>
              <div id='comm_chart_1'></div>
            </div>
            <div class='config_rside'>
              <p class='hl'>Calculate</p>
              <p class='desc'>
                Please Choose your favorite contract and timeslot and press run button
              </p>
              <div class='calc_opts_form'>
                <div class='calc_opt_line clearfix'>
                  <label>Select Contract</label>
                  <select class='form-control asset_contract w_200'>
                    <option value='0'>Select</option>
                    <option value='GOLD'>GOLD</option>
                    <option value='SILVER'>SILVER</option>
                    <option value='COPPER'>COPPER</option>
                    <option value='CRUDE OIL'>CRUDE OIL</option>
                    <option value='NATURAL GAS'>NATURAL GAS</option>
                    <option value='US WHEAT'>US WHEAT</option>
                  </select>
                </div>
                <div class='calc_opt_line clearfix'>
                  <label>Select Timeslot</label>
                  <select class='form-control asset_timeslot w_200'>
                    <option value='0'>Select</option>
                    <option value='1 Day'>1 Day</option>
                    <option value='1 Hour'>1 Hour</option>
                    <option value='15 Minutes'>15 Minutes</option>
                    <option value='5 Minute'>5 Minute</option>
                    <option value='1 Minute'>1 Minute</option>
                  </select>
                </div>
                <button type='button' onclick="startCalcModal()" class='btn btn btn-lg btn-success'>Calculate</button>
              </div>
            </div>
          </div>
          <div class='tab-pane trade_charts_section curr_mode clearfix' id="curr_calc_2">
            <div class='chart_side'>
              <div id='comm_chart_2'></div>
            </div>
            <div class='config_rside'>
              <p class='hl'>Calculate</p>
              <p class='desc'>
                Please Choose your favorite contract and timeslot and press run button
              </p>
              <div class='calc_opts_form'>
                <div class='calc_opt_line clearfix'>
                  <label>Select Contract</label>
                  <select class='form-control asset_contract w_200'>
                    <option value='0'>Select</option>
                    <option value='GOLD'>GOLD</option>
                    <option value='SILVER'>SILVER</option>
                    <option value='COPPER'>COPPER</option>
                    <option value='CRUDE OIL'>CRUDE OIL</option>
                    <option value='NATURAL GAS'>NATURAL GAS</option>
                    <option value='US WHEAT'>US WHEAT</option>
                  </select>
                </div>
                <div class='calc_opt_line clearfix'>
                  <label>Select Timeslot</label>
                  <select class='form-control asset_timeslot w_200'>
                    <option value='0'>Select</option>
                    <option value='1 Day'>1 Day</option>
                    <option value='1 Hour'>1 Hour</option>
                    <option value='15 Minutes'>15 Minutes</option>
                    <option value='5 Minute'>5 Minute</option>
                    <option value='1 Minute'>1 Minute</option>
                  </select>
                </div>
                <button type='button' onclick="startCalcModal()" class='btn btn btn-lg btn-success'>Calculate</button>
              </div>
            </div>
          </div>
          <div class='tab-pane trade_charts_section curr_mode clearfix' id="curr_calc_3">
            <div class='chart_side'>
              <div id='comm_chart_3'></div>
            </div>
            <div class='config_rside'>
              <p class='hl'>Calculate</p>
              <p class='desc'>
                Please Choose your favorite contract and timeslot and press run button
              </p>
              <div class='calc_opts_form'>
                <div class='calc_opt_line clearfix'>
                  <label>Select Contract</label>
                  <select class='form-control asset_contract w_200'>
                    <option value='0'>Select</option>
                    <option value='GOLD'>GOLD</option>
                    <option value='SILVER'>SILVER</option>
                    <option value='COPPER'>COPPER</option>
                    <option value='CRUDE OIL'>CRUDE OIL</option>
                    <option value='NATURAL GAS'>NATURAL GAS</option>
                    <option value='US WHEAT'>US WHEAT</option>
                  </select>
                </div>
                <div class='calc_opt_line clearfix'>
                  <label>Select Timeslot</label>
                  <select class='form-control asset_timeslot w_200'>
                    <option value='0'>Select</option>
                    <option value='1 Day'>1 Day</option>
                    <option value='1 Hour'>1 Hour</option>
                    <option value='15 Minutes'>15 Minutes</option>
                    <option value='5 Minute'>5 Minute</option>
                    <option value='1 Minute'>1 Minute</option>
                  </select>
                </div>
                <button type='button' onclick="startCalcModal()" class='btn btn btn-lg btn-success'>Calculate</button>
              </div>
            </div>
          </div>
          <div class='tab-pane trade_charts_section curr_mode clearfix' id="curr_calc_4">
            <div class='chart_side'>
              <div id='comm_chart_4'></div>
            </div>
            <div class='config_rside'>
              <p class='hl'>Calculate</p>
              <p class='desc'>
                Please Choose your favorite contract and timeslot and press run button
              </p>
              <div class='calc_opts_form'>
                <div class='calc_opt_line clearfix'>
                  <label>Select Contract</label>
                  <select class='form-control asset_contract w_200'>
                    <option value='0'>Select</option>
                    <option value='GOLD'>GOLD</option>
                    <option value='SILVER'>SILVER</option>
                    <option value='COPPER'>COPPER</option>
                    <option value='CRUDE OIL'>CRUDE OIL</option>
                    <option value='NATURAL GAS'>NATURAL GAS</option>
                    <option value='US WHEAT'>US WHEAT</option>
                  </select>
                </div>
                <div class='calc_opt_line clearfix'>
                  <label>Select Timeslot</label>
                  <select class='form-control asset_timeslot w_200'>
                    <option value='0'>Select</option>
                    <option value='1 Day'>1 Day</option>
                    <option value='1 Hour'>1 Hour</option>
                    <option value='15 Minutes'>15 Minutes</option>
                    <option value='5 Minute'>5 Minute</option>
                    <option value='1 Minute'>1 Minute</option>
                  </select>
                </div>
                <button type='button' onclick="startCalcModal()" class='btn btn btn-lg btn-success'>Calculate</button>
              </div>
            </div>
          </div>

          <div class='tab-pane trade_charts_section curr_mode clearfix' id="curr_calc_5">
            <div class='chart_side'>
              <div id='comm_chart_5'></div>
            </div>
            <div class='config_rside'>
              <p class='hl'>Calculate</p>
              <p class='desc'>
                Please Choose your favorite contract and timeslot and press run button
              </p>
              <div class='calc_opts_form'>
                <div class='calc_opt_line clearfix'>
                  <label>Select Contract</label>
                  <select class='form-control asset_contract w_200'>
                    <option value='0'>Select</option>
                    <option value='GOLD'>GOLD</option>
                    <option value='SILVER'>SILVER</option>
                    <option value='COPPER'>COPPER</option>
                    <option value='CRUDE OIL'>CRUDE OIL</option>
                    <option value='NATURAL GAS'>NATURAL GAS</option>
                    <option value='US WHEAT'>US WHEAT</option>
                  </select>
                </div>
                <div class='calc_opt_line clearfix'>
                  <label>Select Timeslot</label>
                  <select class='form-control asset_timeslot w_200'>
                    <option value='0'>Select</option>
                    <option value='1 Day'>1 Day</option>
                    <option value='1 Hour'>1 Hour</option>
                    <option value='15 Minutes'>15 Minutes</option>
                    <option value='5 Minute'>5 Minute</option>
                    <option value='1 Minute'>1 Minute</option>
                  </select>
                </div>
                <button type='button' onclick="startCalcModal()" class='btn btn btn-lg btn-success'>Calculate</button>
              </div>
            </div>
          </div>

          <div class='tab-pane trade_charts_section curr_mode clearfix' id="curr_calc_6">
            <div class='chart_side'>
              <div id='comm_chart_6'></div>
            </div>
            <div class='config_rside'>
              <p class='hl'>Calculate</p>
              <p class='desc'>
                Please Choose your favorite contract and timeslot and press run button
              </p>
              <div class='calc_opts_form'>
                <div class='calc_opt_line clearfix'>
                  <label>Select Contract</label>
                  <select class='form-control asset_contract w_200'>
                    <option value='0'>Select</option>
                    <option value='GOLD'>GOLD</option>
                    <option value='SILVER'>SILVER</option>
                    <option value='COPPER'>COPPER</option>
                    <option value='CRUDE OIL'>CRUDE OIL</option>
                    <option value='NATURAL GAS'>NATURAL GAS</option>
                    <option value='US WHEAT'>US WHEAT</option>
                  </select>
                </div>
                <div class='calc_opt_line clearfix'>
                  <label>Select Timeslot</label>
                  <select class='form-control asset_timeslot w_200'>
                    <option value='0'>Select</option>
                    <option value='1 Day'>1 Day</option>
                    <option value='1 Hour'>1 Hour</option>
                    <option value='15 Minutes'>15 Minutes</option>
                    <option value='5 Minute'>5 Minute</option>
                    <option value='1 Minute'>1 Minute</option>
                  </select>
                </div>
                <button type='button' onclick="startCalcModal()" class='btn btn btn-lg btn-success'>Calculate</button>
              </div>
            </div>
          </div>


        </div>
      </div>
    </div>
  </div>
</div>
</div>

@include('pages.calc.partials.brands', array('t' => 'Trade with Confidence'))

<script type="text/javascript">
  var comm_chart_1 = <?= $comm_chart_1 ?>;
  var comm_chart_2 = <?= $comm_chart_2 ?>;
  var comm_chart_3 = <?= $comm_chart_3 ?>;
  var comm_chart_4 = <?= $comm_chart_4 ?>;
  var comm_chart_5 = <?= $comm_chart_5 ?>;
  var comm_chart_6 = <?= $comm_chart_6 ?>;
</script>

@stop
