@extends('layout.default')
@section('content')

  <header class="header-main"> @include('layout.header')</header>

  <section class="basic_section head_section">
    <div class="container haveborder dark_grey">
      <div class="head">
        <p>What Could YOU Do with an Extra</p>
        <p>$1,000, $5,000 or Even $10,000 a Month?</p>
      </div>

      <div class="player_payment clearfix">
        <div class='l'>
          <iframe class='video_demo' width="480" height="260" src="https://www.youtube.com/embed/zAQP80CWolg?autoplay=1&controls=0" frameborder="0" allowfullscreen></iframe>
        </div>
        <div class="r">
          <div class="h">
            <p class="lightroboto">YES, I WANT</p>
            <p class="blackroboto">INSTANT ACCESS</p>
          </div>
          <div class='subscribe_pay_stack'>
            <div class='subscribe_btn_wr'>
              <a href="javascript: void(0)" class='subscribe_btn' onclick="activateSubscribeAccountModal()">subscribe now</a>
            </div>
            <!-- <div class='pay_icons'>
              <ul class='clearfix'>
                <li><img src="{{ URL::asset('images/Visa.png') }}"></li>
                <li><img src="{{ URL::asset('images/Mastercard.png') }}"></li>
                <li><img src="{{ URL::asset('images/PayPal.png') }}"></li>
                <li><img src="{{ URL::asset('images/American-Express.png') }}"></li>
                <li><img src="{{ URL::asset('images/Discover.png') }}"></li>
              </ul>
            </div> -->
          </div>
        </div>
      </div>

      <p class="supliment-text">
        Imagine how your life would change if you didn’t have to worry about money ever again. Exotic vacations, new cars, luxury homes – it would all be within your reach.
				But to get there you have to stop chasing after scams, schemes and gimmicks and give real, proven systems a chance. You’ll be glad you did!
      </p>

    </div>
  </section>

  <section class="basic_section how-did-you">
    <div class="container haveborder">
      <h2 class="roboto-h2">Can you really make money online?</h2>
      <div class="inner-padding">
        <p>
          Yes, but it’s not easy. Unless…
          You can tell which systems are authentic and legitimate and which ones are scams.
          Problem is, there are 50 fraudsters and scammy ‘make-money-fast’ schemes for each real system, and sometimes it’s simply not possible to tell the good from bad.
        </p>
				<h2 class="roboto-h2">STAY AWAY FROM…</h2>
        <div class="opportunities">
          <div class="opportunities-block clearfix"> <!-- point-block 1 -->
              <div class="opportunities-block-img">
                  <div class="special-block">
                      <span>ONLINE SURVEYS</span>
                  </div>
              </div>
              <p class="opportunities-block-text">
                  Online surveys are a great way to make money…if you love making pennies and clicking
                  random questionnaires for hours on end. Taking online surveys just isn’t worth it
                  because it forces you to devalue your time and keeps you from doing more productive
                  things, which, compared to online surveys, is anything else!
              </p>
          </div> <!-- *end* point-block 1 -->
          <div class="opportunities-block clearfix"> <!-- point-block 2 -->
              <div class="opportunities-block-img">
                  <div class="special-block">
                      <span>NEW BUSINESS IDEAS</span>
                  </div>
              </div>
              <p class="opportunities-block-text">
                  Owning you own business is the ultimate dream. No boss to answer to, no rigid work
                  schedules…and no profits. While launching a venture may sound like a grand idea, less
                  than 5% of all companies survive beyond the first year. The odds are stacked against
                  you, and purely speaking in terms of numbers and statistics, it’s best to pursue other avenues than to start a business.
              </p>
          </div> <!-- *end* point-block 2 -->
          <div class="opportunities-block clearfix"> <!-- point-block 3 -->
              <div class="opportunities-block-img">
                  <div class="special-block">
                      <span>ONLINE GIGS</span>
                  </div>
              </div>
              <p class="opportunities-block-text">
                  There are hundreds of ways to make money online with micro-gigs, from writing two-
                  line reviews to clicking on ads. While some of these do pay, the amount is so ridiculously
                  small that it’s usually not worth the effort. Rather than go through the ordeal of
                  registering, learning the task and becoming disappointed later on, the smarter alternative is to never start!
              </p>
          </div> <!-- *end* point-block 3 -->
        </div>
        <p class="bottom-text">If you’re looking for a fast, reliable and proven way to make money online with minimal time or input, none of these will work because they’re not designed to make you any real money, but to make money for the people behind the scenes – the developers, the advertisers, the publishers and the businesses.</p>
      </div>
    </div>
  </section>

  <section class="basic_section but-now">
    <div class="container haveborder dark_grey">
      <h2 class="roboto-h2">Make Money NOW Trading Binary Options!</h2>
      <div class="inner-padding">
        <p>
          Getting into the financial markets may seen daunting at first, but thanks to the Internet and
          widespread access to information and technology resources, anyone can get into it with no
          training or education. Sure, it helps to understand the underlying principles of price movements, but that shouldn’t stop you from making money NOW.
        </p>
        <br>
        <p>
          <strong>With the Option Figures system, you have a powerful, easy-to-use tool to start making real money online.</strong>
          Even if you don’t know the first thing about binary trading, you can start in a matter of minutes. Nothing to learn, no experience required.
        </p>
      </div>
    </div>
  </section>

  <section class='basic_section charts_tables'>
    <div class="container haveborder">
      <h2 class="roboto-h2">Access OptionFigures Anywhere!</h2>
      <div>
        <img src="{{ URL::asset('images/screenshot_option_figures.png') }}">
      </div>
    </div>
  </section>

  <section class='basic_section charts_tables'>
        <div class="container haveborder">
            <h2 class="roboto-h2">No simulation, All 100% Real Stats!</h2>
            <?php
                $loop_dates = array();
                $loop_dates[] = date('Y-m-d', strtotime('-1 day', time()));
                $loop_dates[] = date('Y-m-d', strtotime('-2 day', time()));
                $loop_dates[] = date('Y-m-d', strtotime('-3 day', time()));
            ?>
            <div class='ch_tbl_wr'>
                <div class='ch_tbl_wr_inside'>

                    <?php foreach($loop_dates as $l_date) { ?>
                        <?php $d = date('d/m/y', strtotime($l_date)); ?>
                        <h2>Saturday <?php echo $d; ?>. Initial deposit €250</h2>
                        <table class="fancyTable">
                        <tbody><tr>
                        <th>Asset</th>
                        <th class="type">Type</th>
                        <th class="investment">Investment</th>
                        <th>Start date</th>
                        <th>Expiration date</th>
                        <th>Entry rate</th>
                        <th>End rate</th>
                        <th class="payout">Payout</th>
                        <th>Status</th>
                        <th class="balance">Balance</th>
                        </tr>
                        <tr>
                        <td>EUR/USD</td>
                        <td class="type type-call">▲</td>
                        <td class="investment">€30</td>
                        <td>00:09 <?php echo $d; ?></td>
                        <td>00:10 <?php echo $d; ?></td>
                        <td>1.3225</td>
                        <td>1.32251</td>
                        <td class="payout">€51</td>
                        <td class="status-1">won</td>
                        <td class="balance">€271</td>
                        </tr>
                        <tr>
                        <td>AUD/USD</td>
                        <td class="type type-put">▼</td>
                        <td class="investment">€25</td>
                        <td>00:31 <?php echo $d; ?></td>
                        <td>00:32 <?php echo $d; ?></td>
                        <td>0.92089</td>
                        <td>0.92073</td>
                        <td class="payout">€43</td>
                        <td class="status-1">won</td>
                        <td class="balance">€289</td>
                        </tr>
                        <tr>
                        <td>USD/JPY</td>
                        <td class="type type-call">▲</td>
                        <td class="investment">€35</td>
                        <td>00:55 <?php echo $d; ?></td>
                        <td>00:56 <?php echo $d; ?></td>
                        <td>97.181</td>
                        <td>97.185</td>
                        <td class="payout">€58</td>
                        <td class="status-1">won</td>
                        <td class="balance">€312</td>
                        </tr>
                        <tr>
                        <td>AUD/USD</td>
                        <td class="type type-call">▲</td>
                        <td class="investment">€25</td>
                        <td>01:25 <?php echo $d; ?></td>
                        <td>01:26 <?php echo $d; ?></td>
                        <td>0.92043</td>
                        <td>0.92052</td>
                        <td class="payout">€43</td>
                        <td class="status-1">won</td>
                        <td class="balance">€330</td>
                        </tr>
                        <tr>
                        <td>EUR/JPY</td>
                        <td class="type type-put">▼</td>
                        <td class="investment">€35</td>
                        <td>01:46 <?php echo $d; ?></td>
                        <td>01:47 <?php echo $d; ?></td>
                        <td>128.359</td>
                        <td>128.336</td>
                        <td class="payout">€60</td>
                        <td class="status-1">won</td>
                        <td class="balance">€355</td>
                        </tr>
                        <tr>
                        <td>EUR/JPY</td>
                        <td class="type type-call">▲</td>
                        <td class="investment">€30</td>
                        <td>02:07 <?php echo $d; ?></td>
                        <td>02:08 <?php echo $d; ?></td>
                        <td>128.38</td>
                        <td>128.399</td>
                        <td class="payout">€51</td>
                        <td class="status-1">won</td>
                        <td class="balance">€376</td>
                        </tr>
                        <tr>
                        <td>USD/JPY</td>
                        <td class="type type-call">▲</td>
                        <td class="investment">€25</td>
                        <td>02:30 <?php echo $d; ?></td>
                        <td>02:31 <?php echo $d; ?></td>
                        <td>97.218</td>
                        <td>97.217</td>
                        <td class="payout">€0</td>
                        <td class="status-0">lost</td>
                        <td class="balance">€351</td>
                        </tr>
                        <tr>
                        <td>AUD/USD</td>
                        <td class="type type-call">▲</td>
                        <td class="investment">€25</td>
                        <td>02:54 <?php echo $d; ?></td>
                        <td>02:55 <?php echo $d; ?></td>
                        <td>0.92156</td>
                        <td>0.92161</td>
                        <td class="payout">€43</td>
                        <td class="status-1">won</td>
                        <td class="balance">€369</td>
                        </tr>
                        <tr>
                        <td>USD/JPY</td>
                        <td class="type type-put">▼</td>
                        <td class="investment">€30</td>
                        <td>03:18 <?php echo $d; ?></td>
                        <td>03:19 <?php echo $d; ?></td>
                        <td>97.249</td>
                        <td>97.236</td>
                        <td class="payout">€50</td>
                        <td class="status-1">won</td>
                        <td class="balance">€389</td>
                        </tr>
                        <tr>
                        <td>EUR/USD</td>
                        <td class="type type-put">▼</td>
                        <td class="investment">€35</td>
                        <td>03:41 <?php echo $d; ?></td>
                        <td>03:42 <?php echo $d; ?></td>
                        <td>1.32215</td>
                        <td>1.32214</td>
                        <td class="payout">€60</td>
                        <td class="status-1">won</td>
                        <td class="balance">€414</td>
                        </tr>
                        <tr>
                        <td>EUR/USD</td>
                        <td class="type type-call">▲</td>
                        <td class="investment">€30</td>
                        <td>04:08 <?php echo $d; ?></td>
                        <td>04:09 <?php echo $d; ?></td>
                        <td>1.32378</td>
                        <td>1.3238</td>
                        <td class="payout">€51</td>
                        <td class="status-1">won</td>
                        <td class="balance">€435</td>
                        </tr>
                        <tr>
                        <td>AUD/USD</td>
                        <td class="type type-call">▲</td>
                        <td class="investment">€30</td>
                        <td>04:28 <?php echo $d; ?></td>
                        <td>04:29 <?php echo $d; ?></td>
                        <td>0.92445</td>
                        <td>0.92433</td>
                        <td class="payout">€0</td>
                        <td class="status-0">lost</td>
                        <td class="balance">€405</td>
                        </tr>
                        <tr>
                        <td>AUD/USD</td>
                        <td class="type type-call">▲</td>
                        <td class="investment">€35</td>
                        <td>04:53 <?php echo $d; ?></td>
                        <td>04:54 <?php echo $d; ?></td>
                        <td>0.92373</td>
                        <td>0.92368</td>
                        <td class="payout">€0</td>
                        <td class="status-0">lost</td>
                        <td class="balance">€370</td>
                        </tr>
                        <tr>
                        <td>AUD/USD</td>
                        <td class="type type-call">▲</td>
                        <td class="investment">€35</td>
                        <td>05:14 <?php echo $d; ?></td>
                        <td>05:15 <?php echo $d; ?></td>
                        <td>0.92353</td>
                        <td>0.92356</td>
                        <td class="payout">€60</td>
                        <td class="status-1">won</td>
                        <td class="balance">€395</td>
                        </tr>
                        <tr>
                        <td>USD/JPY</td>
                        <td class="type type-call">▲</td>
                        <td class="investment">€30</td>
                        <td>05:37 <?php echo $d; ?></td>
                        <td>05:38 <?php echo $d; ?></td>
                        <td>97.302</td>
                        <td>97.316</td>
                        <td class="payout">€50</td>
                        <td class="status-1">won</td>
                        <td class="balance">€415</td>
                        </tr>
                        <tr>
                        <td>USD/JPY</td>
                        <td class="type type-put">▼</td>
                        <td class="investment">€30</td>
                        <td>06:06 <?php echo $d; ?></td>
                        <td>06:07 <?php echo $d; ?></td>
                        <td>97.302</td>
                        <td>97.282</td>
                        <td class="payout">€50</td>
                        <td class="status-1">won</td>
                        <td class="balance">€435</td>
                        </tr>
                        <tr>
                        <td>EUR/USD</td>
                        <td class="type type-put">▼</td>
                        <td class="investment">€25</td>
                        <td>06:32 <?php echo $d; ?></td>
                        <td>06:33 <?php echo $d; ?></td>
                        <td>1.32336</td>
                        <td>1.32335</td>
                        <td class="payout">€43</td>
                        <td class="status-1">won</td>
                        <td class="balance">€453</td>
                        </tr>
                        <tr>
                        <td>AUD/USD</td>
                        <td class="type type-put">▼</td>
                        <td class="investment">€35</td>
                        <td>06:57 <?php echo $d; ?></td>
                        <td>06:58 <?php echo $d; ?></td>
                        <td>0.92099</td>
                        <td>0.92091</td>
                        <td class="payout">€60</td>
                        <td class="status-1">won</td>
                        <td class="balance">€478</td>
                        </tr>
                        <tr>
                        <td>EUR/USD</td>
                        <td class="type type-put">▼</td>
                        <td class="investment">€30</td>
                        <td>07:26 <?php echo $d; ?></td>
                        <td>07:27 <?php echo $d; ?></td>
                        <td>1.32346</td>
                        <td>1.32348</td>
                        <td class="payout">€0</td>
                        <td class="status-0">lost</td>
                        <td class="balance">€448</td>
                        </tr>
                        <tr>
                        <td>GBP/USD</td>
                        <td class="type type-call">▲</td>
                        <td class="investment">€25</td>
                        <td>07:47 <?php echo $d; ?></td>
                        <td>07:48 <?php echo $d; ?></td>
                        <td>1.54948</td>
                        <td>1.54954</td>
                        <td class="payout">€43</td>
                        <td class="status-1">won</td>
                        <td class="balance">€466</td>
                        </tr>
                        <tr>
                        <td>USD/CHF</td>
                        <td class="type type-call">▲</td>
                        <td class="investment">€25</td>
                        <td>08:08 <?php echo $d; ?></td>
                        <td>08:09 <?php echo $d; ?></td>
                        <td>0.9269</td>
                        <td>0.92668</td>
                        <td class="payout">€0</td>
                        <td class="status-0">lost</td>
                        <td class="balance">€441</td>
                        </tr>
                        <tr>
                        <td>EUR/USD</td>
                        <td class="type type-call">▲</td>
                        <td class="investment">€25</td>
                        <td>08:29 <?php echo $d; ?></td>
                        <td>08:30 <?php echo $d; ?></td>
                        <td>1.32411</td>
                        <td>1.32435</td>
                        <td class="payout">€43</td>
                        <td class="status-1">won</td>
                        <td class="balance">€459</td>
                        </tr>
                        <tr>
                        <td>EUR/USD</td>
                        <td class="type type-put">▼</td>
                        <td class="investment">€30</td>
                        <td>08:57 <?php echo $d; ?></td>
                        <td>08:58 <?php echo $d; ?></td>
                        <td>1.32326</td>
                        <td>1.32324</td>
                        <td class="payout">€51</td>
                        <td class="status-1">won</td>
                        <td class="balance">€480</td>
                        </tr>
                        <tr>
                        <td>USD/CHF</td>
                        <td class="type type-call">▲</td>
                        <td class="investment">€35</td>
                        <td>09:22 <?php echo $d; ?></td>
                        <td>09:23 <?php echo $d; ?></td>
                        <td>0.92872</td>
                        <td>0.929</td>
                        <td class="payout">€60</td>
                        <td class="status-1">won</td>
                        <td class="balance">€505</td>
                        </tr>
                        <tr>
                        <td>GBP/USD</td>
                        <td class="type type-call">▲</td>
                        <td class="investment">€30</td>
                        <td>09:44 <?php echo $d; ?></td>
                        <td>09:45 <?php echo $d; ?></td>
                        <td>1.54866</td>
                        <td>1.54852</td>
                        <td class="payout">€0</td>
                        <td class="status-0">lost</td>
                        <td class="balance">€475</td>
                        </tr>
                        <tr>
                        <td>EUR/JPY</td>
                        <td class="type type-put">▼</td>
                        <td class="investment">€30</td>
                        <td>10:05 <?php echo $d; ?></td>
                        <td>10:06 <?php echo $d; ?></td>
                        <td>129.494</td>
                        <td>129.493</td>
                        <td class="payout">€51</td>
                        <td class="status-1">won</td>
                        <td class="balance">€496</td>
                        </tr>
                        <tr>
                        <td>USD/CHF</td>
                        <td class="type type-put">▼</td>
                        <td class="investment">€25</td>
                        <td>10:26 <?php echo $d; ?></td>
                        <td>10:27 <?php echo $d; ?></td>
                        <td>0.92847</td>
                        <td>0.92844</td>
                        <td class="payout">€43</td>
                        <td class="status-1">won</td>
                        <td class="balance">€514</td>
                        </tr>
                        <tr>
                        <td>AUD/USD</td>
                        <td class="type type-put">▼</td>
                        <td class="investment">€30</td>
                        <td>10:48 <?php echo $d; ?></td>
                        <td>10:49 <?php echo $d; ?></td>
                        <td>0.92348</td>
                        <td>0.92349</td>
                        <td class="payout">€0</td>
                        <td class="status-0">lost</td>
                        <td class="balance">€484</td>
                        </tr>
                        <tr>
                        <td>USD/CHF</td>
                        <td class="type type-call">▲</td>
                        <td class="investment">€35</td>
                        <td>11:10 <?php echo $d; ?></td>
                        <td>11:11 <?php echo $d; ?></td>
                        <td>0.92853</td>
                        <td>0.92866</td>
                        <td class="payout">€60</td>
                        <td class="status-1">won</td>
                        <td class="balance">€509</td>
                        </tr>
                        <tr>
                        <td>USD/CHF</td>
                        <td class="type type-put">▼</td>
                        <td class="investment">€25</td>
                        <td>11:32 <?php echo $d; ?></td>
                        <td>11:33 <?php echo $d; ?></td>
                        <td>0.92833</td>
                        <td>0.92812</td>
                        <td class="payout">€43</td>
                        <td class="status-1">won</td>
                        <td class="balance">€527</td>
                        </tr>
                        <tr>
                        <td>AUD/USD</td>
                        <td class="type type-call">▲</td>
                        <td class="investment">€25</td>
                        <td>11:52 <?php echo $d; ?></td>
                        <td>11:53 <?php echo $d; ?></td>
                        <td>0.92459</td>
                        <td>0.9246</td>
                        <td class="payout">€43</td>
                        <td class="status-1">won</td>
                        <td class="balance">€545</td>
                        </tr>
                        <tr>
                        <td>USD/CAD</td>
                        <td class="type type-call">▲</td>
                        <td class="investment">€35</td>
                        <td>12:18 <?php echo $d; ?></td>
                        <td>12:19 <?php echo $d; ?></td>
                        <td>1.03795</td>
                        <td>1.03794</td>
                        <td class="payout">€0</td>
                        <td class="status-0">lost</td>
                        <td class="balance">€510</td>
                        </tr>
                        <tr>
                        <td>EUR/USD</td>
                        <td class="type type-put">▼</td>
                        <td class="investment">€30</td>
                        <td>12:41 <?php echo $d; ?></td>
                        <td>12:42 <?php echo $d; ?></td>
                        <td>1.32231</td>
                        <td>1.32216</td>
                        <td class="payout">€51</td>
                        <td class="status-1">won</td>
                        <td class="balance">€531</td>
                        </tr>
                        <tr>
                        <td>EUR/JPY</td>
                        <td class="type type-put">▼</td>
                        <td class="investment">€25</td>
                        <td>13:06 <?php echo $d; ?></td>
                        <td>13:07 <?php echo $d; ?></td>
                        <td>129.002</td>
                        <td>129.014</td>
                        <td class="payout">€0</td>
                        <td class="status-0">lost</td>
                        <td class="balance">€506</td>
                        </tr>
                        <tr>
                        <td>EUR/JPY</td>
                        <td class="type type-call">▲</td>
                        <td class="investment">€35</td>
                        <td>13:27 <?php echo $d; ?></td>
                        <td>13:28 <?php echo $d; ?></td>
                        <td>129.107</td>
                        <td>129.11</td>
                        <td class="payout">€60</td>
                        <td class="status-1">won</td>
                        <td class="balance">€531</td>
                        </tr>
                        <tr>
                        <td>EUR/USD</td>
                        <td class="type type-call">▲</td>
                        <td class="investment">€25</td>
                        <td>13:54 <?php echo $d; ?></td>
                        <td>13:55 <?php echo $d; ?></td>
                        <td>1.32105</td>
                        <td>1.321</td>
                        <td class="payout">€0</td>
                        <td class="status-0">lost</td>
                        <td class="balance">€506</td>
                        </tr>
                        <tr>
                        <td>GBP/USD</td>
                        <td class="type type-call">▲</td>
                        <td class="investment">€25</td>
                        <td>14:18 <?php echo $d; ?></td>
                        <td>14:19 <?php echo $d; ?></td>
                        <td>1.54382</td>
                        <td>1.54388</td>
                        <td class="payout">€43</td>
                        <td class="status-1">won</td>
                        <td class="balance">€524</td>
                        </tr>
                        <tr>
                        <td>USD/JPY</td>
                        <td class="type type-call">▲</td>
                        <td class="investment">€35</td>
                        <td>14:38 <?php echo $d; ?></td>
                        <td>14:39 <?php echo $d; ?></td>
                        <td>97.718</td>
                        <td>97.723</td>
                        <td class="payout">€58</td>
                        <td class="status-1">won</td>
                        <td class="balance">€547</td>
                        </tr>
                        <tr>
                        <td>USD/JPY</td>
                        <td class="type type-call">▲</td>
                        <td class="investment">€35</td>
                        <td>15:00 <?php echo $d; ?></td>
                        <td>15:01 <?php echo $d; ?></td>
                        <td>97.652</td>
                        <td>97.658</td>
                        <td class="payout">€58</td>
                        <td class="status-1">won</td>
                        <td class="balance">€570</td>
                        </tr>
                        <tr>
                        <td>EUR/JPY</td>
                        <td class="type type-call">▲</td>
                        <td class="investment">€35</td>
                        <td>15:26 <?php echo $d; ?></td>
                        <td>15:27 <?php echo $d; ?></td>
                        <td>128.652</td>
                        <td>128.726</td>
                        <td class="payout">€60</td>
                        <td class="status-1">won</td>
                        <td class="balance">€595</td>
                        </tr>
                        <tr>
                        <td>EUR/JPY</td>
                        <td class="type type-put">▼</td>
                        <td class="investment">€35</td>
                        <td>15:57 <?php echo $d; ?></td>
                        <td>15:58 <?php echo $d; ?></td>
                        <td>128.589</td>
                        <td>128.615</td>
                        <td class="payout">€0</td>
                        <td class="status-0">lost</td>
                        <td class="balance">€560</td>
                        </tr>
                        <tr>
                        <td>GBP/USD</td>
                        <td class="type type-put">▼</td>
                        <td class="investment">€35</td>
                        <td>16:22 <?php echo $d; ?></td>
                        <td>16:23 <?php echo $d; ?></td>
                        <td>1.54085</td>
                        <td>1.54072</td>
                        <td class="payout">€60</td>
                        <td class="status-1">won</td>
                        <td class="balance">€585</td>
                        </tr>
                        <tr>
                        <td>EUR/USD</td>
                        <td class="type type-put">▼</td>
                        <td class="investment">€30</td>
                        <td>16:50 <?php echo $d; ?></td>
                        <td>16:51 <?php echo $d; ?></td>
                        <td>1.31335</td>
                        <td>1.3131</td>
                        <td class="payout">€51</td>
                        <td class="status-1">won</td>
                        <td class="balance">€606</td>
                        </tr>
                        <tr>
                        <td>AUD/USD</td>
                        <td class="type type-call">▲</td>
                        <td class="investment">€25</td>
                        <td>17:17 <?php echo $d; ?></td>
                        <td>17:18 <?php echo $d; ?></td>
                        <td>0.921</td>
                        <td>0.92097</td>
                        <td class="payout">€0</td>
                        <td class="status-0">lost</td>
                        <td class="balance">€581</td>
                        </tr>
                        <tr>
                        <td>EUR/JPY</td>
                        <td class="type type-call">▲</td>
                        <td class="investment">€25</td>
                        <td>17:45 <?php echo $d; ?></td>
                        <td>17:46 <?php echo $d; ?></td>
                        <td>127.957</td>
                        <td>127.959</td>
                        <td class="payout">€43</td>
                        <td class="status-1">won</td>
                        <td class="balance">€599</td>
                        </tr>
                        <tr>
                        <td>USD/JPY</td>
                        <td class="type type-call">▲</td>
                        <td class="investment">€35</td>
                        <td>18:08 <?php echo $d; ?></td>
                        <td>18:09 <?php echo $d; ?></td>
                        <td>97.628</td>
                        <td>97.632</td>
                        <td class="payout">€58</td>
                        <td class="status-1">won</td>
                        <td class="balance">€622</td>
                        </tr>
                        </tbody></table>
                    <?php } ?>

                </div>
            </div>
        </div>
    </section>

    <section class='basic_section make-sense'>
      <div class="container haveborder">
        <div class="inner-padding"> <!-- inner-padding -->
          <h2 class="roboto-h2">Binary options sounds complicated…is it?</h2>
          <p>
              Anything having to do with the financial markets is complicated, but the beauty of binary
              options trading is how easily it sidesteps the complexities so that just about anyone can get into it.
          </p>
          <br/>
          <p>
            <span class="styled-text">Binary options trading is a very special type of trading in which traders bet on the future price direction of a stock, currency or commodity.</span>
            Say gold is trading at $100 and the Option Figures system tells you that the price will go up to $103 in 15 minutes.
            If you follow the system (and bet that the price will go UP) and your decision is right,<strong> you will earn 85% or more in the trade! </strong>
          </p>
          <br/>
		      <p>Each trade is basically an <strong> UP or DOWN decision</strong>. If you believe the price will go up, click UP, if you believe the price will go down, click DOWN. Nothing could be simpler.</p>
		      <br/>
          <p>What Option Figures does is tell you which direction the price will likely move, so you don’t have to do the technical analysis yourself. All you need to get started with binary options trading is:</p>
  				<br/>
  				- Internet connection
  				<br/>
  				- Trading account with a reliable broker
  				<br/>
  				- Subscription to Option Figures
  				<br/>
  				<h2 class="roboto-h2">What if I lose money?</h2>
  				<p>
  				You will, no question about it. But if you follow the system, <strong>you will make a LOT more than you lose.</strong> Isn’t that what trading is all about? There’s no system known to man that is right 100% of the time. It doesn’t exist and never will.</p>
  				<br/>
  				<p><strong>What makes a system effective is how many more times it’s correct than incorrect. </strong> If you lose $10 and make back $100, you haven’t really lost any money. </p>
  				<br/>
  				<p>When it comes to the financial markets, a lot of people make a lot of money, and a lot of people also LOSE a lot of money. What’s the difference between the two groups?</p>
  				<br/>
  				<p>The winners have a winning system! They’ve found something that works and they stick to it. That’s essentially all that it comes down to. This is what separates the winners from the losers.</p>
  				<br/>
  				<p>Imagine having an automated tool that does all the behind-the-scenes gruntwork to calculate and accurately predict price movements for you? You’d have A WINNING SYSTEM that tells you
  				which direction a price would move, letting you place trades backed with confidence and with the reassurance of a state-of-the-art decision-support tool.
  				<br/>
  				<h2 class="roboto-h2">But I don’t know anything about the financial markets…</h2>
  				<p>Great, that’s exactly why you need Option Figures. It may be hard to believe but many of the most successful binary traders you’ll meet got into the game WITHOUT knowing the first thing about it. They picked it up along the way and honed their trading skills and developed their own unique trading styles. It’s a long and intensive process.</p>
  				<br/>
  				Thankfully, with Option Figures, you can bypass all that effort. <strong> If you can click a few buttons you can make money with Option Figures.</strong>
  				It’s perfectly suited for beginners and can help expert traders make better trades as well.
  				<br/>
  				<h2 class="roboto-h2">How do you know which direction a price will move?</h2>
  				<br/>
  				<p>You don’t. But you don’t have to. That’s why Options Figures is there.</p>
  				<br/>
  				<p>Without a proven system, binary trading would be a lot like betting on the outcome of a coin flip. There are only two outcomes – heads or tails for a coin flip, or up or down for a price movement.<strong> You’d be right as many times as you’d be wrong. That’s a statistical FACT.</strong></p>
  				<br/>
  				<p>With Option Figures, the odds are skewed in your favor because price movements are not random occurrences as coin flips, but are governed by hundreds of variables including demand,
  				supply, historical prices, trends, the general state of the economy, political stability, confidence in the financial markets, and lots of other factors.</p>
  				<br/>
  				<p>Except for economists and math professors it’s VIRTUALLY IMPOSSIBLE to accurately predict price movements because there are just so many variables and they change so fast.
  				Option Figures does is it evaluates EACH AND EVERY VARIABLE that could impact the price, and calculates their cumulative effect on the price in real time.</p>
  				<br/>
  				<p>The result? An accurate prediction that gives you the most likely price movement in the near future. <strong> Even if it’s wrong sometimes, as long as you’re making the right decisions more often than the wrong ones, you’ll always make money!</strong></p>
  				<br/>
  				<p style='margin-bottom: 40px;'>Option Figures is FULLY AUTOMATED and EXTREMELY POWERFUL. There’s nothing to install, and you don’t even have to learn how to use it.
  				All you have to do is select the asset you want to trade, select a timeframe for the price movement, then view the result. Option Figures takes into consideration
  				<strong>every available piece of information regarding the asset</strong> and generates the most accurate prediction based on the latest information.</p>
        </div> <!-- *end* inner-padding -->
      </div>
    </section>

    <section class="basic_section but-how">
      <div class="container haveborder blue">
        <div class="inner-padding">
          <h2 class="roboto-h2">Trade with confidence</h2>
          <div class="procedure clearfix">
            <div class="pr-block">
              <div class="head">
                <span class="number">1</span><br>
                <span>Select Asset</span>
              </div>
              <div class="img-square img-asset"></div>
              <p>Select from stock indices, currency pairs or commodities</p>
            </div>
            <div class="pr-block">
              <div class="head">
                <span class="number">2</span><br>
                <span>Specify Timeframe</span>
              </div>
              <div class="img-square img-timeframe"></div>
              <p>Specify the timeframe for the trade</p>
            </div>
            <div class="pr-block last-pr-block">
              <div class="head">
                <span class="number">3</span><br>
                <span>View Results</span>
              </div>
              <div class="img-square img-predi"></div>
              <p>Check the prediction and place your trade</p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="basic_section really-simple">
      <div class="container haveborder">
        <div class="inner-padding">
          <h2 class="roboto-h2">What about brokers?</h2>
          <p>
            Good question. Brokers are the final piece of the puzzle. As a trader, you’ll need to open an
            account with a broker to trade binary options. For first-time traders, it can be a real challenge
            to choose a reliable broker because there are simply too many to compare, and the not-so-great ones FAR outnumber the legitimate ones
          </p>
          <br/>
          <p>
            To ensure Option Figures users are not scammed by second-rate brokers I’ve made the service
	          available to only traders of the top 5 brokers in the market today, not to mention the safest and most reputable brokers in binary options.
          </p>
	        <br/>
	        <p class="bottom-text"> For traders, an account with a reliable broker combined with a proven tool like Option Figures is all it takes to make real money online, super fast and from anywhere!</p>
          <div class="chart"><img src="{{ URL::asset('images/chart-img.png') }}"></div>
          <p>
            All this success came about because, thanks to OptionFigures,
            <strong>I know what to trade and when to trade it.</strong>
          </p>
        </div>
      </div>
    </section>

    <section class="basic_section trade_kit">
      <div class="container haveborder dark_grey">
        <div class="inner-padding">
          <h2 class="roboto-h2">The Complete Binary Trading Kit</h2>
          <p>
            With Option Figures you have the most powerful, accurate and complete trading tool to get started with binary options trading.
            It’s simple to use, and tells you with maximum accuracy what to trade, when to trade, and which direction the price of an asset will move.
          </p>
	        <p>
            Remember, <strong> all you need to make steady profits is a system that is right more often than it is wrong! </strong> And that’s what you get with Option Figures.
          </p>
          <br/>
	        <h2 class="roboto-h2">How do I start?</h2>
	        <p>
	          It’s easy to get started. Simply enter your email in the form below and you’ll be redirected to a special Members’ Area with full instructions on opening your broker account and accessing Option Figures. It’ll only take a minute.
          </p>
          <br/>
          <p>
            <strong><i>But you have to act fast!</i></strong> I’m only giving away a limited number of accounts to use Option Figures, after which I’ll charge the regular retail price of $1,997.
	          If you want access to one of the most powerful automated tools to trade binary options, sign up now for a rewarding and thrilling journey in binary options trading!
          </p>
        </div>
      </div>
    </section>

    <section class="basic_section buy_now">
      <div class="container haveborder">
        <div class="inner-padding">
          <div class='product_desc_wr'>
            <h2 class='roboto-h2'>Signup for Free!</h2>
            <p class='desc'>
              OptionFigures Calculator is a powerful, easy to use tool to start making real money online with Binary Options Trading. You can start in a matter of minutes. Nothing to learn, no experience required.
            </p>
            <!-- <ul>
              <li>
                Your initial charge will be 14&euro;. You will then be charged 97&euro; per month for the unlimited duration
              </li>
              <li>
                Membership account (automatic registration)
              </li>
              <li>
                Wordwide product
              </li>
              <li>
                Immediate membership account creation after success subscription
              </li>
              <li>
                Membership account will be updated on the 1st of each month
              </li>
              <li>
                For any question feel free to support@option-figures.com
              </li>
            </ul> -->
          </div>

          <div class="get_access">
            <div class="h">
              <p class="lightroboto">YES, I WANT</p>
              <p class="blackroboto">INSTANT ACCESS</p>
            </div>
            <div class='subscribe_pay_stack'>
              <div class='subscribe_btn_wr'>
                <a href="javascript: void(0)" class='subscribe_btn' onclick="activateSubscribeAccountModal()">subscribe now</a>
              </div>
              <!-- <div class='pay_icons'>
                <ul class='clearfix'>
                  <li><img src="{{ URL::asset('images/Visa.png') }}"></li>
                  <li><img src="{{ URL::asset('images/Mastercard.png') }}"></li>
                  <li><img src="{{ URL::asset('images/PayPal.png') }}"></li>
                  <li><img src="{{ URL::asset('images/American-Express.png') }}"></li>
                  <li><img src="{{ URL::asset('images/Discover.png') }}"></li>
                </ul>
              </div> -->
            </div>
          </div>

        </div>
      </div>
    </section>

    <div class="modal fade in susbscribe_form_modal" id="susbscribe_form_modal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class='tl_header clearfix'>
              <img src="{{ URL::asset('images/logoimg.png') }}"><h4>Register an Account.</h4>
            </div>
          </div>
          <form>
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <div class="modal-body">
              <div class="info_block">
                <p class='hl'>Your Information</p>
                <div class='two_rows_wr clearfix'>
                  <div class='r'>
                      <input type="text" class="form-control" id="fname" name="fname" placeholder="First Name">
                      <span class='req'>*</span>
                  </div>
                  <div class='r'>
                      <input type="text" class="form-control" id="lname" name="lname" placeholder="Last Name">
                      <span class='req'>*</span>
                  </div>
                </div>
                <div class='gen_rows_wr'>
                  <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone">
                  <span class='req'>*</span>
                </div>
                <div class='gen_rows_wr'>
                  <input type="text" class="form-control" id="email" name="email" placeholder="Email">
                  <span class='req'>*</span>
                </div>
                <div class='gen_rows_wr'>
                  <input type="password" class="form-control" id="psw" name="psw" placeholder="Password">
                  <span class='req'>*</span>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary" onclick="return subscribeAccount(this)">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    <footer>
      <div class='foot_navi'>
        <div class="container">
          <ul class="clearfix">
            <li><a href="<?php echo url('/'); ?>/terms">Terms & Conditions</a></li>
            <li>|</li>
            <li><a href="<?php echo url('/'); ?>/privacy">Privacy Policy</a></li>
            <li>|</li>
            <li><a href="<?php echo url('/'); ?>/risk_disclaimer">Risk Disclaimer</a></li>
          </ul>
        </div>
      </div>

  	  <div class="container">
  	    Disclaimer: Trading in the foreign exchange and binary options markets carries a high level of risk. If you choose to partake in the activity of trading futures and options, you should carefully consider your investment objectives, level of experience and appetite for such risk. The possibility exists that you could sustain a total loss of some or all of your initial investment. As such, trading may not be suitable for all investors. You should not invest money that you cannot afford to lose. Before trading, you should make yourself aware of all the risks associated with futures and options trading, and seek professional advice from an independent or suitably licensed financial advisor. Under no circumstances shall OptionFigures or its affiliates have any liability to any person or entity for (a) any loss or damage in whole or part caused by, resulting from, or relating to any transactions related to the OptionFigures services or (b) any direct, indirect, special consequential or incidental damages whatsoever.
  	    <br>
  	    <br>CFTC Rule 4.41: These results are based on simulated or hypothetical performance results that have certain limitations. Unlike the results shown in an actual performance record, these results do not represent actual trading results. Because these trades have not been executed, these results may have under- or overcompensated for the impact, if any, of certain market factors, such as lack of liquidity and other factors. Simulated or hypothetical trading programs are generally subject to the fact that they are designed with the benefit of hindsight. No representation is being made that any account will or is likely to achieve profits similar to those presented.
  	    <br>
  	    <br>OptionFigures is not a financial advisory service and does not provide financial advice. The information that we provide, including that which is delivered via the OptionFigures service, does not constitute financial advice and is to be used solely for educational purposes.
  	  </div>
  	</footer>

    <script src="//static.getclicky.com/js" type="text/javascript"></script>
  	<script type="text/javascript">try{ clicky.init(100911701); }catch(e){}</script>
  	<noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/100911701ns.gif" /></p></noscript>


    <!-- Google Code for OF - Home - Global -->
    <!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide: google.com/ads/remarketingsetup -->
    <script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 928769519;
    var google_conversion_label = "74WkCPmpiWkQ78vvugM";
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
    <div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/928769519/?value=1.00&amp;currency_code=EUR&amp;label=74WkCPmpiWkQ78vvugM&amp;guid=ON&amp;script=0"/>
    </div>
    </noscript>

@stop
