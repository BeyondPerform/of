@extends('layout.default')
@section('content')
  <div class="thankyou_page">
    <div class='header_section'>
      <div class="container">
        <div class="logo">
          <a href="<?php echo url('/'); ?>"><img src="{{ URL::asset('images/logo_dark.png') }}"></a>
        </div>
        <p class="decs">
          Welcome to OptionFigures, the ultimate<br>technical analysis <span class="bold">Binary Options</span> <span class="bold_orange">Calculator</span>
        </p>
      </div>
    </div>

    <div class='signin_section'>
      <div class="head">
        <p><span class="orange">Please enter your email</p>
      </div>
      <div class="content">
        <div class="form_wr none_border">
          <div class='calc_login_box'>
              @if (count($errors) > 0)
                 <div class="alert alert-danger">
                     <strong>Whoops!</strong> There were some problems with your input.<br><br>
                     <ul>
                         @foreach ($errors->all() as $error)
                             <li>{{ $error }}</li>
                         @endforeach
                     </ul>
                 </div>
             @endif
             @if(Session::has('status'))
                 <p class="alert alert-info" style="margin-bottom: 15px">{{ Session::get('status') }}</p>
             @endif
            <form action="email" method="POST">
              <input type="hidden" name="_token" value="{{csrf_token()}}">
              <div class="form-group">
                <input class='form-control' type="email" name="email" placeholder="Email">
              </div>
              <div class="form-group cnt">
                <button type="submit" class="btn btn-sm register-btn">Submit</button>
              </div>
            </form>
          </div>
        </div>

      </div>
    </div>

    <div class='foot_section'>
      <div class='foot_navi'>
        <div class="container">
          <ul class="clearfix">
            <li><a href="<?php echo url('/'); ?>/terms">Terms & Conditions</a></li>
            <li>|</li>
            <li><a href="<?php echo url('/'); ?>/privacy">Privacy Policy</a></li>
            <li>|</li>
            <li><a href="<?php echo url('/'); ?>/risk_disclaimer">Risk Disclaimer</a></li>
          </ul>
        </div>
      </div>
      <div class='container'>
        <p>
         Trading in the foreign exchange and binary options markets carries a high level of risk. If you choose to partake in the activity of trading futures and options, you should carefully consider your investment
         objectives, level of experience and appetite for such risk. The possibility exists that you could sustain a total loss of some or all of your initial investment. As such, trading may not be suitable for all investors. You should
         not invest money that you cannot afford to lose. Before trading, you should make yourself aware of all the risks associated with futures and options trading, and seek professional advice from an independent or suitably
         licensed financial advisor. Under no circumstances shall OptionFigures or its affiliates have any liability to any person or entity for (a) any loss or damage in whole or part caused by, resulting from, or relating to any
         transactions related to the OptionFigures services or (b) any direct, indirect, special consequential or incidental damages whatsoever.
        </p>
        <p>
         CFTC Rule 4.41: These results are based on simulated or hypothetical performance results that have certain limitations. Unlike the results shown in an actual performance record, these results do not represent actual
         trading results. Because these trades have not been executed, these results may have under- or overcompensated for the impact, if any, of certain market factors, such as lack of liquidity and other factors. Simulated
         or hypothetical trading programs are generally subject to the fact that they are designed with the benefit of hindsight. No representation is being made that any account will or is likely to achieve profits similar to those
         presented.
        </p>
        <p>
         OptionFigures is not a financial advisory service and does not provide financial advice. The information that we provide, including that which is delivered via the OptionFigures service, does not constitute financial
         advice and is to be used solely for educational purposes.
        </p>
      </div>
    </div>

  </div>

@stop
