@extends('layout.default')
@section('content')
  <div class="thankyou_page">
    <div class='header_section'>
      <div class="container">
        <div class="logo">
          <a href="<?php echo url('/'); ?>"><img src="{{ URL::asset('images/logo_dark.png') }}"></a>
        </div>
        <p class="decs">
          Welcome to OptionFigures, the ultimate<br>technical analysis <span class="bold">Binary Options</span> <span class="bold_orange">Calculator</span>
        </p>
      </div>
    </div>

    <div class='signin_section'>
      <div class="head">
        <p><span class="orange">Thank you</span> for your purchase!</p>
      </div>
      <div class="content">
        <div class="form_wr">
          <p class='h'>Access your Account</p>
          <p class='desc'>
            Enter the username and password you used to purchased OptionFigures and start using the tool. If you have any difficulties to login to your account , please click the "Forget Password" link and we'll send you the instructions on how to reset your password.
          </p>
          <div class='calc_login_box'>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
            @endif
            <form action="signin" method="POST">
              <input type="hidden" name="_token" value="{{csrf_token()}}">
              <div class="form-group">
                <input class='form-control' type="email" name="email" placeholder="Email">
              </div>
              <div class="form-group">
                <input class="form-control" type="password" name="password" placeholder="Password">
              </div>
              <div class="form-group cnt">
                <button type="submit" class="btn btn-sm register-btn">Login Now</button>
                <a href="<?php echo url('/'); ?>/password/email" class="btn btn-sm forgot-psw-btn">Forget Password</a>
              </div>
            </form>
          </div>
        </div>

        <div class="promotions_section">
          <p>Trade with Confidence</p>
          <div class='promo_acc_wrap'>

            <!--<div class='promo_line_wr clearfix'>
              <div class='logo_wr'>
                <div class='logo_placeholder'>

                </div>
              </div>
              <p class='title'>
                BinaryOnline.com<br>
                <a href='javascript: void(0)' data-toggle="modal" data-target="#avatrade_acc_countries">Accepted countries</a>
              </p>
              <p class='discount'>100% Bonus</p>
              <div class='cnt'>
                <div class='clearfix'>
                  <a class='step1' href="javascript: void(0)" onclick="activateApiCallModal('BinaryOnline.com')">
                    <p>Step 1</p>
                    <p>Open Account</p>
                  </a>
                  <a class='step2' href="javascript: void(0)">
                    <p>Step 2</p>
                    <p>Fund Account</p>
                  </a>
                </div>
              </div>
            </div>-->

            <!--<div class='promo_line_wr clearfix'>
              <div class='logo_wr'>
                <div class='logo_placeholder'>

                </div>
              </div>
              <p class='title'>
                UFX Trading<br>
                <a href='javascript: void(0)' data-toggle="modal" data-target="#avatrade_acc_countries">Accepted countries</a>
              </p>
              <p class='discount'>100% Bonus</p>
              <div class='cnt'>
                <div class='clearfix'>
                  <a class='step1' href="javascript: void(0)" onclick="activateApiCallModal('UFX Trading')">
                    <p>Step 1</p>
                    <p>Open Account</p>
                  </a>
                  <a class='step2' href="javascript: void(0)">
                    <p>Step 2</p>
                    <p>Fund Account</p>
                  </a>
                </div>
              </div>
            </div>-->

            <div class='promo_line_wr clearfix'>
              <div class='logo_wr'>
                <div class='logo_placeholder'>
                  <img src="{{ URL::asset('images/avatrade.png') }}">
                </div>
              </div>
              <p class='title'>
                AvaTrade<br>
                <a href='javascript: void(0)' data-toggle="modal" data-target="#avatrade_acc_countries">Accepted countries</a>
              </p>
              <p class='discount'>100% Bonus</p>
              <div class='cnt'>
                <div class='clearfix'>
                  <a class='step1' href="http://beyondtrkk.com/path/lp.php?trvid=10025&trvx=5f4e17e5" target="_blank">
                    <p>Step 1</p>
                    <p>Open Account</p>
                  </a>
                  <a class='step2' href="javascript: void(0)">
                    <p>Step 2</p>
                    <p>Fund Account</p>
                  </a>
                </div>
              </div>
            </div>

            <div class="promo_line_wr clearfix">
              <div class="logo_wr">
                <div class="logo_placeholder">
                  <img src="<?php echo url(); ?>/images/option500.png">
                </div>
              </div>
              <p class="title">
                Option500<br>
                <a href="javascript: void(0)" data-toggle="modal" data-target="#option500_acc_countries">Accepted countries</a>
              </p>
              <p class="discount">100% Bonus</p>

              <div class='cnt'>
                <div class='clearfix'>
                  <a class='step1' href="javascript: void(0)" onclick="activateApiCallModal('Option500')">
                    <p>Step 1</p>
                    <p>Open Account</p>
                  </a>
                  <a class='step2' href='https://www.option500.com/banking/?utm_medium=referral&utm_source=pNxmnOXYO8&affiliateId=pNxmnOXYO8&campaignId=176&a_aid=dfc8a48e&serial=Deposit_Page&affParams1={OUR_LEAD_ID}' target="_blank">
                    <p>Step 2</p>
                    <p>Fund Account</p>
                  </a>
                </div>
              </div>
            </div>

            <div class="promo_line_wr clearfix">
              <div class="logo_wr">
                <div class="logo_placeholder">
                  <img src="<?php echo url(); ?>/images/trade_solid.png">
                </div>
              </div>
              <p class="title">
                TradeSolid<br>
                <a href="javascript: void(0)" data-toggle="modal" data-target="#tradesolid_acc_countries">Accepted countries</a>
              </p>
              <p class="discount">100% Bonus</p>

              <div class='cnt'>
                <div class='clearfix'>
                  <a class='step1' href="javascript: void(0)" onclick="activateApiCallModal('TradeSolid')">
                    <p>Step 1</p>
                    <p>Open Account</p>
                  </a>
                  <a class='step2' href='https://www.tradesolid.com/my-account/?activeZone=deposit' target="_blank">
                    <p>Step 2</p>
                    <p>Fund Account</p>
                  </a>
                </div>
              </div>
            </div>

            <div class="promo_line_wr clearfix">
              <div class="logo_wr">
                <div class="logo_placeholder" style="background-color: #273135; text-align: center">
                  <img src="{{ URL::asset('images/topoption.png') }}">
                </div>
              </div>
              <p class="title">
                TopOption<br>
                <a href="javascript: void(0)" data-toggle="modal" data-target="#topoption_acc_countries">Accepted countries</a>
              </p>
              <p class="discount">100% Bonus</p>
              <div class="cnt">
                <div class="clearfix">
                  <a class="step1" href="http://beyondtrkk.com/path/lp.php?trvid=10024&trvx=36ed18f8" target="_blank">
                    <p>Step 1</p>
                    <p>Open Account</p>
                  </a>
                  <a class="step2" href="javascript: void(0)">
                    <p>Step 2</p>
                    <p>Fund Account</p>
                  </a>
                </div>
              </div>
            </div>

            <div class="promo_line_wr clearfix">
              <div class="logo_wr">
                <div class="logo_placeholder">
                  <img src="{{ URL::asset('images/tradecom.png') }}">
                </div>
              </div>
              <p class="title">
                Trade.com<br>
                <a href="javascript: void(0)" data-toggle="modal" data-target="#tradecom_acc_countries">Accepted countries</a>
              </p>
              <p class="discount">100% Bonus</p>
              <div class="cnt">
                <div class="clearfix">
                  <a class="step1" href="http://beyondtrkk.com/path/lp.php?trvid=10032&trvx=cd7c7d8b" target="_blank">
                    <p>Step 1</p>
                    <p>Open Account</p>
                  </a>
                  <a class="step2" href="javascript: void(0)">
                    <p>Step 2</p>
                    <p>Fund Account</p>
                  </a>
                </div>
              </div>
            </div>

            <div class="promo_line_wr clearfix">
              <div class="logo_wr">
                <div class="logo_placeholder">
                  <img src="{{ URL::asset('images/KeyOptionLogo2.png') }}">
                </div>
              </div>
              <p class="title">
                Keyoption<br>
                <a href="javascript: void(0)" data-toggle="modal" data-target="#keyoption_acc_countries">Accepted countries</a>
              </p>
              <p class="discount">100% Bonus</p>
              <div class="cnt">
                <div class="clearfix">
                  <a class="step1" href="javascript: void(0)" onclick="activateApiCallModal('Keyoption')">
                    <p>Step 1</p>
                    <p>Open Account</p>
                  </a>
                  <a class="step2" href="javascript: void(0)">
                    <p>Step 2</p>
                    <p>Fund Account</p>
                  </a>
                </div>
              </div>
            </div>

            <div class="promo_line_wr clearfix">
              <div class="logo_wr">
                <div class="logo_placeholder">
                  <img src="{{ URL::asset('images/option_live.png') }}">
                </div>
              </div>
              <p class="title">
                OptionLive<br>
                <a href="javascript: void(0)" data-toggle="modal" data-target="#optionlive_acc_countries">Accepted countries</a>
              </p>
              <p class="discount">100% Bonus</p>
              <div class="cnt">
                <div class="clearfix">
                  <a class="step1" href="javascript: void(0)" onclick="activateApiCallModal('OptionLive')">
                    <p>Step 1</p>
                    <p>Open Account</p>
                  </a>
                  <a class="step2" href="javascript: void(0)">
                    <p>Step 2</p>
                    <p>Fund Account</p>
                  </a>
                </div>
              </div>
            </div>

          </div>
        </div>

      </div>
    </div>

    <div class='foot_section'>
      <div class='foot_navi'>
        <div class="container">
          <ul class="clearfix">
            <li><a href="<?php echo url('/'); ?>/terms">Terms & Conditions</a></li>
            <li>|</li>
            <li><a href="<?php echo url('/'); ?>/privacy">Privacy Policy</a></li>
            <li>|</li>
            <li><a href="<?php echo url('/'); ?>/risk_disclaimer">Risk Disclaimer</a></li>
          </ul>
        </div>
      </div>
      <div class='container'>
        <p>
         Trading in the foreign exchange and binary options markets carries a high level of risk. If you choose to partake in the activity of trading futures and options, you should carefully consider your investment
         objectives, level of experience and appetite for such risk. The possibility exists that you could sustain a total loss of some or all of your initial investment. As such, trading may not be suitable for all investors. You should
         not invest money that you cannot afford to lose. Before trading, you should make yourself aware of all the risks associated with futures and options trading, and seek professional advice from an independent or suitably
         licensed financial advisor. Under no circumstances shall OptionFigures or its affiliates have any liability to any person or entity for (a) any loss or damage in whole or part caused by, resulting from, or relating to any
         transactions related to the OptionFigures services or (b) any direct, indirect, special consequential or incidental damages whatsoever.
        </p>
        <p>
         CFTC Rule 4.41: These results are based on simulated or hypothetical performance results that have certain limitations. Unlike the results shown in an actual performance record, these results do not represent actual
         trading results. Because these trades have not been executed, these results may have under- or overcompensated for the impact, if any, of certain market factors, such as lack of liquidity and other factors. Simulated
         or hypothetical trading programs are generally subject to the fact that they are designed with the benefit of hindsight. No representation is being made that any account will or is likely to achieve profits similar to those
         presented.
        </p>
        <p>
         OptionFigures is not a financial advisory service and does not provide financial advice. The information that we provide, including that which is delivered via the OptionFigures service, does not constitute financial
         advice and is to be used solely for educational purposes.
        </p>
      </div>
    </div>

  </div>

  @include('pages.brand_ac_modals')

@stop
