@extends('layout.login')
@section('content')
  <div class="basic_wr user_login_wr">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-4 col-sm-offset-4">

          <div class="panel form_holder">
            <div class="panel-heading">
              <div class="tl_header clearfix">
                <img src="{{ URL::asset('images/logoimg.png') }}">
              </div>
            </div>
            <div class="panel-body">

              @if (count($errors) > 0)
                  <div class="alert alert-danger">
                      <strong>Whoops!</strong> There were some problems with your input.<br><br>
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif
              @if(Session::has('message'))
                  <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
              @endif

              <form role="form" action="signin" method="POST" class="login_form" autocomplete="off">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
								<div class="form-group">
									<div class="input-group input-group-lg">
										<div class="input-group-addon">
											<span class="fa fa-envelope aria-hidden="true""></span>
										</div>
										<input class="form-control" type="email" name="email" placeholder="Email">
									</div>
								</div>
								<div class="form-group">
									<div class="input-group input-group-lg">
										<div class="input-group-addon">
											<span class="fa fa-key aria-hidden="true""></span>
										</div>
										<input class="form-control" type="password" name="password" placeholder="Password">
									</div>
								</div>
								<div class="form-group clearfix">
                  <button role="button" class="btn btn-primary pull-right" type="submit">Login</button>
								</div>
							</form>

            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
@stop
