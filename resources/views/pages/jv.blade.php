@extends('layout.jv')
@section('content')

<div id="document" class="document">

			<!-- HEADER
			================================= -->
			<header id="header" class="header-section section section-dark navbar navbar-fixed-top">

				<div class="container-fluid">

					<div class="navbar-header navbar-left">

						<!-- RESPONSIVE MENU BUTTON -->
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>

						<!-- HEADER LOGO -->
						<a class="navbar-logo navbar-brand anchor-link" href="#hero">
							<img src="{{ URL::asset('jv/images/logos/header-logo.png') }}" srcset="{{ URL::asset('jv/images/logos/header-logo@2x.png 2x') }}" alt="OF">
						</a>

					</div>

					<nav id="navigation" class="navigation navbar-collapse collapse navbar-right">

						<!-- NAVIGATION LINKS -->
						<ul id="header-nav" class="nav navbar-nav">

							<li><a href="#hero" class="hidden">Top</a></li>

							<!-- <li><a href="#prizes">Prizes</a></li> -->
							<li><a href="#how-it-works">How it Works</a></li>
							<li><a href="#links">Affiliate Links</a></li>
							<li><a href="#emails">Emails</a></li>
							<li><a href="#keywords">Keyword Lists</a></li>
							<li><a href="#banners">Banners</a></li>


							<!-- HEADER ACTION BUTTON -->
							<li class="header-action-button"><a href="#hero" class="btn btn-primary">Join</a></li>
						</ul>

					</nav>

				</div>

			</header>

			<!-- HERO
			================================= -->
			<section id="hero" class="hero-section hero-layout-classic hero-layout-features-and-form section section-dark">

				<div class="section-background">

					<!-- IMAGE BACKGROUND -->
					<div class="section-background-image parallax" data-stellar-ratio="0.4">
						<img src="{{ URL::asset('jv/images/backgrounds/slide-1.jpg') }}" alt="" style="opacity: 0.2;">
					</div>

					<!-- VIDEO BACKGROUND -->
					<!-- <div class="section-background-video section-background-dot-overlay parallax" data-stellar-ratio="0.4">
						<video preload="auto" autoplay loop muted poster="images/backgrounds/video-fallback-bg.jpg" style="opacity: 0.3;">
							<source type="video/mp4" src="videos/video-bg.mp4">
							<source type="video/ogg" src="videos/video-bg.ogv">
							<source type="video/webm" src="videos/video-bg.webm">
						</video>
					</div> -->

					<!-- SLIDESHOW BACKGROUND -->
					<!-- <ul class="section-background-slideshow parallax" data-stellar-ratio="0.4" data-speed="800" data-timeout="4000">
						<li><img src="images/backgrounds/hero-bg-slideshow-1.jpg" alt="" style="opacity: 0.25;"></li>
						<li><img src="images/backgrounds/hero-bg-slideshow-2.jpg" alt="" style="opacity: 0.25;"></li>
						<li><img src="images/backgrounds/hero-bg-slideshow-3.jpg" alt="" style="opacity: 0.2;"></li>
					</ul> -->

				</div>

				<div class="container">

					<div class="hero-content">
						<div class="hero-content-inner">

							<div class="hero-heading row" data-animation="fadeIn">
								<div class="col-md-10 col-md-offset-1">

									<h1 class="hero-title">Industry's First Binary Options Calculator!</h1>

									<p class="hero-tagline">Here's why “OptionFigures” will be the launch you do not want to miss out on!</p>

								</div>
							</div>

							<div class="hero-features row">

								<div class="hero-features-left col-md-7">

									<p class="lead">We’ve tested this latest version of OptionFigures extensively so you can be sure that you will generate extraordinary EPCs with this offer. It has demonstrated extremely high performance across our binary, forex, trading and biz opp lists, so get in on the action..</p>
									<ul class="icon-list">
										<li>
											<span class="icon-list-icon fa fa-hand-o-up" data-animation="bounceIn"></span>
											<h4 class="icon-list-title">NO REFUNDS</h4>
											<p>NO MORE worries about making a lot of money only to see 40% of it disappear to refunds.</p>
										</li>
										<li>
											<span class="icon-list-icon fa fa-heart-o" data-animation="bounceIn"></span>
											<h4 class="icon-list-title">NO RESERVES</h4>
											<p>I'm not going to hold back - 30%, 20%, not even 10% of your earnings in reserve.</p>
										</li>
										<li>
											<span class="icon-list-icon fa fa-lightbulb-o" data-animation="bounceIn"></span>
											<h4 class="icon-list-title">FAST PAYMENTS & CASH PRICES!</h4>
											<p>NO MORE WAITING! There is nothing worse than getting only a percentage of your money... then waiting up to 6 months to finally get the rest! </p>
										</li>
									</ul>

								</div>

								<div class="hero-features-right col-md-5" data-animation="fadeIn">

									<!-- FORM -->
									<form class="form" role="form">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
										<h4 class="form-heading">Fill the form below to access our JV Tools area!</h4>

										<div class="form-validation alert"></div>

										<div class="form-group">
											<label for="fname">First Name</label>
											<input type="text" name="fname" id="fname" class="form-control" placeholder="Enter First Name" autocomplete="off">
										</div>

										<div class="form-group">
											<label for="lname">Last Name</label>
											<input type="text" name="lname" id="lname" class="form-control" placeholder="Enter Last Name" autocomplete="off">
										</div>

										<div class="form-group">
											<label for="email">Email Address</label>
											<input type="email" name="email" id="email" class="form-control" placeholder="Enter Email Address" autocomplete="off">
										</div>

										<div class="form-group">
                      <label for="clicksure_id">Clicksure ID</label>
                      <input type="text" name="clicksure_id" id="clicksure_id" class="form-control" placeholder="Enter Your Clicksure ID" autocomplete="off">
										</div>

                    <div class="form-group">
                      <label for="skype_id">Skype ID</label>
                      <input type="text" name="skype_id" id="skype_id" class="form-control" placeholder="Enter Your Skype ID" autocomplete="off">
                    </div>

										<div class="form-group form-group-submit">
											<button type="submit" class="btn btn-primary btn-lg btn-block" data-loading-text="Action Button" onclick="return submitClicksureForm(this);">Get Access</button>
										</div>
									</form>
								</div>

							</div>

						</div>
					</div>

				</div>

			</section>

			<!-- HEADLINE
			================================= -->
			<section style='padding-bottom: 0px;' id="headline" class="headline-section section-gray section">

				<div class="container">

					<h2 class="section-heading text-center hidden">Tagline</h2>

					<div class="row">
						<div class="col-md-10 col-md-offset-1">

							<p class="headline-text">
								OptionFigures is another funnel guaranteed to earn you big profits. There is nothing for your subscribers to purchase; they register for an advanced calculator that can predict signals or run automatically.</p>

						</div>
					</div>

				</div>

				<section class="section-dark section">

					<!-- <div class="section-background">
						<div class="section-background-image parallax" data-stellar-ratio="0.4">
							<img src="images/backgrounds/slide-1.jpg" alt="" style="opacity: 0.15;">
						</div>
					</div> -->

					<div class="container">

						<h3 class="closing-shout" style='font-size: 34px;'>Winning without competing?</h3>

						<p class='hero-tagline text-center'>Introducing OptionFigures Rewards Program!</p>

						<img class='bags_pic' src="{{ URL::asset('jv/images/bags.png') }}">
					</div>

				</section>

				<div class="row">
					<div class="col-md-10 col-md-offset-1">
						<p class='headline-text'>
							OptionFigures' rewards program is driven by a simple idea: rewards and/or prizes should not be based on competition, but on individual performance .That’s why we’ve done away with competitions altogether.
							The "OptionFigures' Rewards Program" is ALWAYS ON. Even during off-peak seasons. Even when you’re asleep.
						</p>
					</div>
				</div>

				<div class="row new_section_hiw">
					<div class="col-md-10 col-md-offset-1">
						<h2 class='section-heading reset_after text-center'>Here’s how it works</h2>
						<div class='new_sec_txt_zone'>
							<p>Drive X number of conversions: You'll be enter to our Reward Level 1</p>
							<p>Drive X + Y number of conversions: You'll be enter to our Reward Level 2</p>
							<p>...you get the picture</p>
							<p>
								The OptionFigures' rewards program is as innovative as exciting. All you’ve got to do is sign up with Clicksure, push traffic and start racking up those conversions. ALL affiliates are eligible to win from ALL Reward Levels, ALL the time.
							</p>
						</div>
					</div>
				</div>

				<div class="row new_section_hiw rewards_sec">
					<div class="col-md-10 col-md-offset-1">
						<h2 class='section-heading reset_after text-center mt_30'>The Rewards</h2>
						<div class='new_sec_txt_zone'>
							<p class='text-center'>Up to 10 Sales = $75.00 CPA</p>
	            <p class='text-center'>11-20 Sales = $80.00 CPA</p>
	            <p class='text-center'>21-30 Sales = $85.00 CPA</p>
	            <p class='text-center'>31-40 Sales = $90.00 CPA</p>
	            <p class='text-center'>41-50 Sales = $95.00 CPA</p>
	            <p class='text-center'>51 and above = $100.00 CPA!!</p>
	            <p class='text-center'>OptionFigures rewards program will be active until September 2016. Plenty of time to push hard and make plenty of cash!</p>
						</div>
					</div>
				</div>

			</section>

			<!-- DESCRIPTION
			================================= -->
			<!-- <center><section id="prizes" class="prizes-section section">

				<div class="container">

					<h1 class="section-heading text-center">OptionFigures will be launching on the 30.3.2015! </h1>
							<p class="lead">$250 CPA! No Refunds, No Reserves, No Stress & No Problems!
							Cash Prizes will also be available to the tune of $10,000</p>
					<div class="container">

							<center><img src="images/contents/about.png" alt="Description Image"></center>
<br/><br/>
<p class="lead">Launch with OptionFigures and enjoy a top quality product, great cash prizes, a full set of JV Tools and high EPC rates! Sign in with your Name, Email and Clicksure ID for full access to our JV Tools area.</p>

						</div>

</section></center>

<br/>

<br/> -->
			<!-- HOW IT WORKS
			================================= -->
			<section id="how-it-works" class="how-it-works-section section">

				<div class="container-fluid">

					<h2 class="section-heading text-center">How it Works</h2>

					<div class="hiw-row row">

						<!-- HOW IT WORKS - ITEM 1 -->
						<div class="col-md-3 col-sm-6" data-animation="fadeIn">
							<div class="hiw-item">
								<div class="hiw-item-text">
									<span class="hiw-item-icon">1</span>
									<h4 class="hiw-item-title">Create an Account</h4>
									<p class="hiw-item-description"><P>We're launching this amazing funnel using Clicksure network.<br />don’t have a Clicksure account? <a href="https://www.clicksure.com/signup/affiliate" class="glow">click here to sign up for one</a></span>
</p>
								</div>
							</div>
						</div>

						<!-- HOW IT WORKS - ITEM 2 -->
						<div class="col-md-3 col-sm-6" data-animation="fadeIn">
							<div class="hiw-item even">
								<div class="hiw-item-text">
									<span class="hiw-item-icon">2</span>
									<h4 class="hiw-item-title">Get Links</h4>
									<p class="hiw-item-description">Pick one of the landing pages under the "Affiliate Links" section. Then, replace the ‘XXX’ with your Clicksure ID and you're all set!</p>
								</div>
							</div>
						</div>

						<div class="hidden-md hidden-lg clear"></div>

						<!-- HOW IT WORKS - ITEM 3 -->
						<div class="col-md-3 col-sm-6" data-animation="fadeIn">
							<div class="hiw-item">
								<div class="hiw-item-text">
									<span class="hiw-item-icon">3</span>
									<h4 class="hiw-item-title">Start Promoting</h4>
									<p class="hiw-item-description">OptionFigures has demonstrated extremely high performance across ALL binary, forex, trading and biz opp placements, keywords and lists </p>
								</div>
							</div>
						</div>

						<!-- HOW IT WORKS - ITEM 4 -->
						<div class="col-md-3 col-sm-6" data-animation="fadeIn">
							<div class="hiw-item even">
								<div class="hiw-item-text">
									<span class="hiw-item-icon">4</span>
									<h4 class="hiw-item-title">Earn Commissions</h4>
									<p class="hiw-item-description">Start earning $100 CPA per conversion! No Refunds and No Reserves. Cash Prizes will also be available</p>
								</div>
							</div>
						</div>

					</div>

				</div>

			</section>

			<!-- NUMBERS
			================================= -->
			<section id="numbers" class="numbers-section section-dark section">

				<div class="section-background">

					<!-- IMAGE BACKGROUND -->
					<div class="section-background-image parallax" data-stellar-ratio="0.4">
						<img src="{{ URL::asset('jv/images/backgrounds/slide-1.jpg') }}" style="opacity: 0.2;">
					</div>

				</div>

				<div class="container">

					<h2 class="section-heading text-center">Make Money with OptionFigures Today!</h2>

					<div class="numbers-row row">

						<!-- NUMBERS - ITEM 1 -->
						<div class="col-md-3 col-sm-6">
							<div class="numbers-item">
								<div class="numbers-item-counter"><span class="counter-up">67</span>$</div>
								<div class="numbers-item-caption">CPA</div>
							</div>
						</div>

						<!-- NUMBERS - ITEM 2 -->
						<div class="col-md-3 col-sm-6">
							<div class="numbers-item">
								<div class="numbers-item-counter"><span class="counter-up">2.9</span>$</div>
								<div class="numbers-item-caption">EPC</div>
							</div>
						</div>

						<!-- NUMBERS - ITEM 3 -->
						<div class="col-md-3 col-sm-6">
							<div class="numbers-item">
							<div class="numbers-item-counter"></span> NET 32 </div>
								<div class="numbers-item-caption">PAYMENT CYCLE</div>
							</div>
						</div>

						<!-- NUMBERS - ITEM 4 -->
						<div class="col-md-3 col-sm-6">
							<div class="numbers-item">
								<div class="numbers-item-counter"></span> <img src="{{ URL::asset('jv/images/contents/click.png') }}" alt="Description Image"></div>
								<div class="numbers-item-caption">NETWORK</div>
							</div>
						</div>

					</div>

				</div>

			</section>

						<!-- HEADLINE
			================================= -->
			<section id="headline" class="headline-section section-gray section">

				<div class="container">

					<h2 class="section-heading text-center hidden">Tagline</h2>

					<div class="row">
						<div class="col-md-10 col-md-offset-1">
                <h2 class="section-heading text-center">SUPPORT</h2>

							<p class="headline-text">
								If you need any help with promoting the OptionFigures or have any affiliate questions, please get in touch at: jv[at]option-figures.com</p>
</p>

						</div>
					</div>

				</div>

			</section>

			<!-- CLOSING
			================================= -->
			<section id="closing" class="closing-section section-dark section">

				<div class="section-background">

					<!-- IMAGE BACKGROUND -->
					<div class="section-background-image parallax" data-stellar-ratio="0.4">
						<img src="{{ URL::asset('jv/images/backgrounds/slide-1.jpg') }}" alt="" style="opacity: 0.15;">
					</div>

				</div>

				<div class="container">

					<h3 class="closing-shout">So What Are You Waiting For? Getting Started Is Easy!</h3>

					<div class="closing-buttons" data-animation="tada"><a href="#hero" class="anchor-link btn btn-lg btn-primary">GET ACCESS NOW</a></div>

				</div>

			</section>

			<!-- AFFILIATE LINKS
			================================= -->
			<center><section id="links" class="links-section section">
				<?php
					$clicksure_id = "XXX";
					if(@$_GET['custom_ClicksureID']) {
						$clicksure_id = $_GET['custom_ClicksureID'];
					}
				?>
				<div class="container">
                <h2 class="section-heading text-center">AFFILIATE LINKS</h2>
				<br/>
				<p class="lead">Landing Page 1</p>
<center><textarea style="width:60%;height:50px;resize:none;">http://<?php echo $clicksure_id; ?>.opfigures.cpa.clicksure.com</textarea></center>

				<br/>
<P>please replace the ‘XXX’ with your clicksure ID<br />don’t have a clicksure account? <a href="https://www.clicksure.com/signup/affiliate" class="glow">click here to sign up for one</a></span>

				</div>

			</section></center>


			<!-- EMAIL COPY
			================================= -->
			<center><section id="emails" class="emails-section section">

				<div class="container">
                <h2 class="section-heading text-center">PROVEN EMAIL COPY</h2>
<br/>

 <div class='email_frames_wrap'>
            <div class="emailbox"><p>Email 1</p>
                <textarea style="width:60%;height:250px;resize:none;">
Subject: $26,881 For Your Checking Account

Hi {Firstname}

You read that right.. $26,881 waiting for you.

Activate this FREE software, Follow the 3 simple steps and become a millionaire
this year

>> Claim Your Money Here
http://<?php echo $clicksure_id; ?>.opfigures.cpa.clicksure.com

Regards

Your name
                </textarea>
            </div>
            <div class="emailbox"><p>Email 2</p>
                <textarea style="width:60%;height:250px;resize:none;">
Subject: Warning - Making money this fast is addictive


Hello {firstname}

This software should come with a warning on it! Because making money this fast really is addictive. The buzz about OptionFigures system is coming thick and fast.

>> Check Out All The Buzz

http://<?php echo $clicksure_id; ?>.opfigures.cpa.clicksure.com

thanks

Your name
                </textarea>
            </div>
            <div class="emailbox"><p>Email 3</p>
                <textarea style="width:60%;height:250px;resize:none;">
Subject: I Want To WARN You!

Hi {firstname}

I feel it is important to let you know this ASAP.

So I had to email you to warn you.

This offer is not going to be around for very long at all. Josh has limited the OptionFigures software to the first 50 people that claim his FREE software.

Josh will be charging $1799 thereafter.

>> Claim your FREE software now.
http://<?php echo $clicksure_id; ?>.opfigures.cpa.clicksure.com

Your name
                </textarea>
            </div>
            <div class="emailbox"><p>Email 4</p>
                <textarea style="width:60%;height:250px;resize:none;">
Subject: 5 mins a day to turn $100 into $2400..?


Hi {firstname}

I have dealt with brokers and binary software for years.
I have seen the good, the bad and the ugly when it comes to these so called "amazing software".

Most of them are average and are ok at giving a guideline. But, with OptionFigures, I have to say this software really does do exactly as it say's.

I have to point out, that its rare to get a software that really does smash it. It's even rare to get the
owner of the software giving away the software for FREE, forever. It shows just how confident he is in the software. He knows for a fact that this works.

>> Claim Your Free For Life Software
http://<?php echo $clicksure_id; ?>.opfigures.cpa.clicksure.com

Your name
                </textarea>
            </div>
            <div class="emailbox"><p>Email 5</p>
                <textarea style="width:60%;height:250px;resize:none;">
Subject: $1,400.093 Dollars Last Year! Josh Will Show You How ;-)



Hey

OptionFigures is the most profitable software in the world. I just had an email from some-one that I wanted to show you.

"Hi - I have to say I was a bit skeptical when I saw the OptionFigures offer. But I really wanted to learn how to make money. I am not very good with computers so I was a little worried. But, the software has been so simple to use. The brokers have been amazing, so helpful and friendly. I am delighted I took up this offer. In the space of 3 days I have already made $18,209! Thank you so much for letting me know about this offer." - Jack, Denver - USA

It just shows how people with very little experience in computers are making huge profits. Start now

>> Start Making  Easy Money Today
http://<?php echo $clicksure_id; ?>.opfigures.cpa.clicksure.com


Your name

                </textarea>
            </div>
            <div class="emailbox"><p>Email 6</p>
                <textarea style="width:60%;height:250px;resize:none;">
Subject: Urgent! Your Download Link Has Been Activated.. Please Follow These Steps!



Hi {firstname}

I really don't want you to sit there kicking yourself in a few weeks time. Trust me, if you miss out on this, you really be kicking yourself.

>> Click Here And Accept Your OptionFigures System for FREE!
http://<?php echo $clicksure_id; ?>.opfigures.cpa.clicksure.com

I emailed Josh before to ask him how it was going. He said that there are only a few places left. After that he will have to stop the FREE offer and sell it for $1799 from next week onwards.

A few places left, make sure that one of those places is yours NOW.

>> Click Here And Secure Your Software Now
http://<?php echo $clicksure_id; ?>.opfigures.cpa.clicksure.com

Thanks

Your name


                </textarea>
            </div>
            <div class="emailbox"><p>Email 7</p>
                <textarea style="width:60%;height:250px;resize:none;">
Subject: 1 million in one year! $2,880 Everyday.. How Much Do You Need?



Hey

Word is spreading quickly about this, so you need to hurry if you want to make some money.

These people are not experienced traders. In fact, most of the people who have bought this soft-ware haven't even got any experience whatsoever and didn't even know what binary is.

So if you have no experience, then don't sweat... you don't need experience. You just need to be able to click a few buttons and hey presto!

http://<?php echo $clicksure_id; ?>.opfigures.cpa.clicksure.com

Your Name
                </textarea>
            </div>
            <div class="emailbox"><p>Email 8</p>
                <textarea style="width:60%;height:250px;resize:none;">
Subject: Would You LikeAn Extra $32,289 This Month?

Hi {firstname}

If you have answered, "No", then delete this email now.

If you answered "Yes" (or words to that effect!), then claim one of the places that might be left right now.

I must warn you though, if you click and you can't access it, then it means that all the places for this FREE software have gone. If you can access it, then consider yourself one of the lucky ones.

>> Claim Your Place Here
http://<?php echo $clicksure_id; ?>.opfigures.cpa.clicksure.com

Kind Regards

Yourname
                </textarea>
            </div>
            <div class="emailbox"><p>Email 9</p>
                <textarea style="width:60%;height:250px;resize:none;">
Subject: Your $2,880 Per Day Account Has Been Approved!



Hey

Please follow these 3 steps to ACTIVATE your account

Warning This Link will expire

>> Click Here To Activate Account
http://<?php echo $clicksure_id; ?>.opfigures.cpa.clicksure.com


Kind Regards

YourName
                </textarea>
            </div>
            <div class="emailbox"><p>Email 10</p>
                <textarea style="width:60%;height:250px;resize:none;">
Subject: Do you need more money?


Hi {firstname}

If you are like so many others out there that answer "Yes" to that question then keep reading.

You have to ask yourself, if you are ever going to get that house you have always wanted? Buy the car you have always wanted? Are you doing something now, that would achieve this?

If you are serious about acquiring everything you want, then join the elite and make huge profits.

>> Click Here To Live The Life Of Your Dreams
http://<?php echo $clicksure_id; ?>.opfigures.cpa.clicksure.com

Being with a reputable broker that we direct you to, and having FREE software will help you get the huge profits you see other traders reaping.

Start Now.


Thanks

Your name
                </textarea>
            </div>
            <div class="emailbox"><p>Email 11</p>
                <textarea style="width:60%;height:250px;resize:none;">
Subject: $1,120 Every Hour..Find Out How?


Hi {firstname}

This software is bringing amazing results for people.

>> Click  Here And Watch FREE Video!
http://<?php echo $clicksure_id; ?>.opfigures.cpa.clicksure.com

But you must be quick! If you go on the page and it states "Offer closed" then, I am afraid you have missed out. You will have an opportunity in the future to buy it for $1799.

So hurry and check to see if you are able to get one of the few places left.


Kind Regards

Yourname
                </textarea>
            </div>
            <div class="emailbox"><p>Email 12</p>
                <textarea style="width:60%;height:250px;resize:none;">
Subject: 100% NEW, 100% Powerful for 2015



Hi {firstname}

Well, days like this do not come around very often. In fact, days like this don't come around every decade.

This software is so NEW, so POWERFUL, it will blow your mind. You are going to be blown away at how easily this software pull in such massive amounts of money, so easily!

Do you want to hear the good part? Its FREE! That's right. All you have to do is follow the instruc-tions and start making money TODAY

>> Click Here To Get Your FREE system
http://<?php echo $clicksure_id; ?>.opfigures.cpa.clicksure.com

Be quick, while you still can get it. Click here to download it now before the site disappears.

Enjoy.

YOURNAME
                </textarea>
            </div>
            <div class="emailbox"><p>Email 13</p>
                <textarea style="width:60%;height:250px;resize:none;">
Subject: Download Your $2,881 Per Day System Now!



Hi {firstname}

After years of failing, Matty had grown sick and tired of trying all the so called money making sys-tems. He had wasted a small fortune, not to mention a hell of a lot of time. Matty had tried every-thing from MLM, online marketing, surveys... you name it... he has tried it. They all had one thing in common... they had sold him an empty promise.

But when Matty saw OptionFigures, he decided to give it a go. He made a promise to himself a while back that he will never stop looking and trying no matter what. Because if you don't try... you don't get. So 2 weeks ago, he watched this video

>> Click Here And Watch FREE Video
http://<?php echo $clicksure_id; ?>.opfigures.cpa.clicksure.com

and he liked what he saw. Next, he filled in his email, followed the deposit instructions with the broker and received the software. In 2 weeks, he has made $36,203. The guy is ecstatic as you can imagine. Do what he did, become a success story.


Claim Your System Here
http://<?php echo $clicksure_id; ?>.opfigures.cpa.clicksure.com

Thanks

Yourname
                </textarea>
            </div>
			<div class="emailbox"><p>Email 14</p>
                <textarea style="width:60%;height:250px;resize:none;">
Subject: [SHOCKING] Video of college kid banking $3K in 24 hours


Everyone is talking about how one of our members (who asked to remain anonymous)…

…banked over $3K in 24 hours.

>> http://<?php echo $clicksure_id; ?>.opfigures.cpa.clicksure.com <<

She finally decided to share her story and spill the beans…

…and the truth will SHOCK you.

>> http://<?php echo $clicksure_id; ?>.opfigures.cpa.clicksure.com <<

Can you handle the truth,
Name
                </textarea>
            </div>
<div class="emailbox"><p>Email 15</p>
                <textarea style="width:60%;height:250px;resize:none;">
Subject: Your money account is PENDING REVIEW (please read immediately)


Thank you for your recent application for our brand-new money system.

We are reviewing it this week but wanted to give you a chance to improve your odds of being accepted.

http://<?php echo $clicksure_id; ?>.opfigures.cpa.clicksure.com

All you have to do is click the link below and tell us what’s the FIRST thing you’re going to buy with your first $1M.

Go here:

http://<?php echo $clicksure_id; ?>.opfigures.cpa.clicksure.com

Thank me later,
Name

                </textarea>
            </div>
<div class="emailbox"><p>Email 16</p>
                <textarea style="width:60%;height:250px;resize:none;">
Subject: (URGENT) Deposit $382.39 into your bank account tonight


This is an automated message notifying you of an accounting mistake on our end.

It seems we have underpaid you in the amount of $382.39 in your last commission payment.

>> http://<?php echo $clicksure_id; ?>.opfigures.cpa.clicksure.com <<

We apologize for our mistake and have made those funds available for immediate deposit.

Pick up your money here:

>> http://<?php echo $clicksure_id; ?>.opfigures.cpa.clicksure.com <<

(NOTE: If you have any questions please call our customer service dept. at 1-800-392-3929).

Thanks,
Name

                </textarea>
            </div>
<div class="emailbox"><p>Email 17</p>
                <textarea style="width:60%;height:250px;resize:none;">
Subject: URGENT Job Offer (Start Banking Immediately!)


I’m always on the hunt of REAL, PROFITABLE ways to work from home…

…and I found this:

>> http://<?php echo $clicksure_id; ?>.opfigures.cpa.clicksure.com <<

I immediately thought of you - the pay is over $200 a hour and no experience necessary.

In fact, all you have to do is press a button a few times every hour.

Apply here:

>> http://<?php echo $clicksure_id; ?>.opfigures.cpa.clicksure.com <<

(WARNING: This will be taken down within 24 hours due to excessive # of applicants)

Good luck,
Name
                </textarea>
            </div>
<div class="emailbox"><p>Email 18</p>
                <textarea style="width:60%;height:250px;resize:none;">
Subject: Please accept our apology (FREE gift inside)


We received your application for to become a trial member of our brand-new money system…

…and we apologize for not getting back to you sooner.

FYI - it took longer than expected to review +3,000 applications.

>> http://<?php echo $clicksure_id; ?>.opfigures.cpa.clicksure.com <<

As a “THANK YOU” for your patience we’re going to gift you a FREE private access link:

>> http://<?php echo $clicksure_id; ?>.opfigures.cpa.clicksure.com <<

Enjoy,
Name

                </textarea>
            </div>
<div class="emailbox"><p>Email 19</p>
                <textarea style="width:60%;height:250px;resize:none;">
Subject: 5 “newbies” bank $90K in 67 days


Do NOT miss out on this.

This is a complete game-changer if you want to make money online.

>> http://<?php echo $clicksure_id; ?>.opfigures.cpa.clicksure.com <<

5 newbies banked over $90K using this exact system..

…none of them had ever made a dime online.

Start here:

>> http://<?php echo $clicksure_id; ?>.opfigures.cpa.clicksure.com <<


Thank me later,
Name

                </textarea>
            </div>
<div class="emailbox"><p>Email 20</p>
                <textarea style="width:60%;height:250px;resize:none;">
Subject: Congratulations - you’ve WON (save this email!)


Hey,

    Congratulations!

Out of over 500 random applicants, we have selected you to receive a FREE VIP private download today.

>> http://<?php echo $clicksure_id; ?>.opfigures.cpa.clicksure.com <<

This will give you instant access to a PROVEN money system that’s created over 83 new millionaires in the last 9 months.

Claim your VIP account here:

>> http://<?php echo $clicksure_id; ?>.opfigures.cpa.clicksure.com <<

(WARNING: If you do NOT claim this within 24 hours, we will be forced to gift this to the next lottery winner).

Enjoy,
Name
                </textarea>
            </div>
<div class="emailbox"><p>Email 21</p>
                <textarea style="width:60%;height:250px;resize:none;">
Subject: SUPER easy software banks $1,282 per day (proof inside)


Hey,

    The below video reveals how I’m banking over $1,200 per day…

…while working less than 2 hours (if at all).

>> http://<?php echo $clicksure_id; ?>.opfigures.cpa.clicksure.com <<

You can make this work for you too.

Go here to sign up:

>> http://<?php echo $clicksure_id; ?>.opfigures.cpa.clicksure.com <<

*HURRY* - only 5 trial licenses are available.

Thank me later,
Name
                </textarea>
            </div>
            <div class="emailbox"><p>Email 22</p>
                <textarea style="width:60%;height:250px;resize:none;">
Subject: Warning - 1 Place Left and then its gone

Hi {firstname}

Hopefully, I am in time with getting this email to you. I have just heard from Josh, and he said that there is 1 place left to claim this FREE software. After that, you will need to purchase the software at a whopping $1799.

>>Get it now here
http://<?php echo $clicksure_id; ?>.opfigures.cpa.clicksure.com


I really wouldn't hang around for too long. If you are serious about making money, and serious about trading then take action now while you can get this software for FREE.

>> Actually Click Here To See If There Is Any Left
    http://<?php echo $clicksure_id; ?>.opfigures.cpa.clicksure.com

Kind Regards

Yourname
                </textarea>
            </div>
        </div>
				</div>

			</section></center>


			<!-- KEYWORD LISTS & PPV PLACEMENTS
			================================= -->
			<section id="keywords" class="keywords-section section">

				<div class="container">
                <h2 class="section-heading text-center">KEYWORD LISTS & PPV PLACEMENTS</h2>
<br/>

   <Center> <table width="50%" border="0" cellspacing="5" cellpadding="5" align="center">
          <tr>
            <td><span class="MsoNormal">
              <textarea name="keywords2" cols="20" rows="18">Keywords
binary options
binary software
binary trading
binary options brokers
binary options strategy
binary option
binary options trading
binary options signals
binary options review
binary options trading strategy
binary options strategies
binary option trading
binary options trading platform
binary option signals
trade binary options
best binary options broker
trading platform
binary options reviews
trading systems
best trading platform
binary options trading strategies
binary trading strategies
binary options forum
binary options trading system
swing trading
trading online
binary trade
nadex binary options
binary option broker
trading for dummies
trading currency
binary options daily
trading stocks
trading commodities
digital options
trading simulator
equity trading
trading fx
stock trading online
binary options trading systems
binary option trading strategies
trading oil
paper trading
binary options broker
binary options demo account
trading for beginners
options
forex binary options
online trading</textarea>
            </span></td>
            <td><span class="MsoNormal">
              <textarea name="keywords3" cols="20" rows="18">Keywords
binary option strategies
trading binary options
trading
binary options scam
cfd trading
forex trading strategies
options trading
day trading
trading stock online
options traders
banc de binary
future trading
fx trading
futures trading
binary options demo
currency trading
trading software
share trading
forex trading
futures
best trading software
binary options 101
carbon trading
call option
binary options wiki
trading platforms
binary options software
free binary options signals
put option
any option
commodities trading
trade forex
fx options
binary stock trading
how to trade binary options
what is a binary option
trade option
binary trading scams
spread trading
binary options uk
binary options guide
option trading
binary option brokers
commodities options trading
binary trading reviews
what are binary options
binary options xposed
binary options trading signals
trading money
commodity trading
algorithmic trading
trading strategies
trading options
trading tools
currency trading
online trading forex
trading strategies forex
trading forex online
oil trading
oil price
how to trade oil
trading commodities
forex traders
oil trading companies
forex trading online
foreign exchange trading
oil trader
forex day trading
forex trader
commodity markets
learn to trade forex
online currency trading
forex trading systems
trading forex
forex online trading
fx trade
trading system forex
commodity trading jobs
forex broker
forex trading account
forex trading system
forex trading tools
trading oil
trading in commodity
how to trade forex for beginners
forex strategies
forex news
forex trade
forex trading platforms
fx trader
what is forex trading
fx trading strategies
automated trading forex
day trading forex
forex education
forex trading
day forex trading
fx trading
oil prices
forex factory
trading commodity
oil traders
trading fx
commodities trading
best forex broker
forex market
online trading
forex live
forex account
easy forex
online forex
trading platforms
forextrading
forex trading forex
how to trade forex online
forex currency
forex demo account
trading platform
gas trading
commodity trading
how to trade commodities
trade oil online
automated forex trading
trading
forex course
forex demo
oil trading academy
spread trading
cfd trading
gold trading
futures trading
day trading
forex exchange
trading commodity futures
forex markets
forex trading company
stock trading
forex online
forex converter
forex
options trading
trading systems
trading software
forex trading strategies
exchange traded commodities
oil market
best forex trader
fx forex
trading futures
commodity trading firms
energy trading
forex tutorial
what is forex</textarea>
            </span></td>
            <td><span class="MsoNormal">
              <textarea name="keywords4" cols="20" rows="18">PPV Targets
9gag.com
coinmill.com
currencyconverter.co.uk
eanswers.com
eroprofile.com
perezhilton.com
search.findwide.com
thehindu.com
voyeurhit.com
westernjournalism.com
xossip.com
youm7.com
zdnet.com
mgid.com
mail.com
nbcnews.com
k7x.com
lifehack.org
advancedsearch2.virginmedia.com
888
independent.co.uk
broker
casino.com
betfair
trading
lse.co.uk
5-yal.com
aastocks.com
americablog.com
anyoption
ariva.de
bananaguide.com
barchart.com
bbc.co.uk
binary option
binary options trading
binary trading
binaryoptionsthatsuck.com
bingoonthebox
boursorama.com
brokerage
businessweek.com
cnbc.com
day trading
digitaljournal.com
exchange-rates.org
financialexpress.com
finviz.com
forex trading strategies
forexcrunch.com
fxnewstoday.ae
iforex
iforex.com
indexmundi.com
investinganswers.com
investorplace.com
ironfx.com
lewrockwell.com
money-zine.com
money.usnews.com
myfxbook.com
netdania.com
oanda.com
oilprice.com
pch.com
plumpeum.com
stockcharts.com
theaustralian.com.au
todocoleccion.net
trading fx
trading online
trading stocks
trading212.com
ubs.com
ufxmarkets.com
yepporn.com
youjizzfree.net
zerohedge.com
anyoption.com
dailyfx.com
forexfactory.com
forexpeacearmy.com
fxcm.com
gtoptions.com
livecharts.co.uk
stocktwits.com
thestreet.com
tradestation.com
wallstcheatsheet.com
4-traders.com
adn.com
advfn.com
afrointroductions.com
allslots
andlil.com
arabsh.com
avenuexxx.com
babypips.com
beegfree.com
bestbinaryoptionbrokers.net
bestsexo.com
bforex.com
binary options
binarymatrixpro.com
borsagundem.com
bullbearings.co.uk
calculator.net
cfd trading
contenko.com
cumhuriyet.com.tr
cutt.us
dailyforex.com
dinarrecaps.com
easy-forex.com
ecorazzi.com
empflixfree.net
etoro
eztrader.com
fapvid.com
finanzen.net
forex.com
ftop.ru
fxcm
fxdd.com
fxempire.com
fxpro.co.uk
fxstreet.com
gifeye.com
gifsfor.com
gocurrency.com
goldprice.org
gunsandammo.com
handelsblatt.com
heavy-r.com
helium.com
howtogeek.com
ibexnetwork.com
iforex.es
iii.co.uk
ilsole24ore.com
insidermonkey.com
insuredprofits.com
investing.com
investopedia.com
jahteens.com
jsmineset.com
junior-broker.com
ladbrokes
ladygames.com
lakvisiontv.net
learntotradethemarket.com
libfx.net
likuoo.com
make money online
make money online with
make online money
making money online
marketonmobile.com
markets.com
miamiherald.com
money18.on.cc
moneycontrol.com
morazzia.com
mubasher.info
muslimass.com
nowretro.com
nuvid.com
oneinthepink.com
online make money
online money making
online trading
onlinetradingconcepts.com
onvista.de
opteck.com
optionow.com
options
options trading
plus500
qqc.co
scratchmania
sgx.com
silverdoctors.com
sitemeter.com
stockhouse.com
svhunters.com
svscomics.com
taringa.net
tddirectinvesting.co.uk
themoneyconverter.com
themovs.com
thepiratebay.sx
tinylinks.co
to make money online
topoption.com
trade binary options
traderush
traderush.com
tradingeconomics.com
translate.googleusercontent.com
trend-online.com
tubemoms.net
videosbang.com
xbooru.com
xforex.com
xvidz69.com
100percentprofitbot.com
10option.com
12binaryoptions.com
2014millionaire.co
2015millionaire.co
2015millionairesclub.com
24h-options.com
24hgold.com
24option-trading.com
24option.com
2work-at-home.com
30daychange.co
3x.com.tw
50den.com
69xtube.com
6ixy.com
70kmethod.com
7daymillionaire.co
7easyforex.blogspot.com
7red
a3mal-ksa.com
abcbourse.com
acheter-des-actions.com
action-girls.net
actionforex.com
activtrades.it
ad-hoc-news.de
adityatrading.in
ae.fxcomparison.com
afreepornmovies.com
agweb.com
ahorro.com
akademiaforex.com
aktien-portal.at
aktienboard.com
alahlitadawul.com
alexblog.info
allbinoptions.com
allhardsex.com
alongporn.com
alpari.co.uk
alpari.com
alpari.de
alphafundsoftware.com
americanbankingnews.com
americanbulls.com
amilftube.net
analisaforex.com
analitika-forex.ru
angelcapitalmarket.com
angrymovs.com
anirudhsethireport.com
anonymoustrader.co
anyoptions.com
anyoptiontrading.com
apostasbinarias.com
ar.forexmagnates.com
arab.dailyforex.com
arabfreeporn.net
arabicbroker.com
argentbourse.com
as1.advfn.com
asextremetube.com
asianbig.net
asianfreeporn.org
asianpornforfree.net
asslutload.com
astrofinanza.com
astubegalore.com
asx.com.au
asxnxx.com
asyouporn.com
autobinaryspy.com
automoneyapp.com
automoneymachines.com
automonster.ca
autoprofitreplicator.com
autorv.com
avafx.com
avengertrader.com
ayondo.com
bancxp.com
bankaholic.com
batimariolo.com
bazaartrend.com
bbinary.com
bbwchubbyporn.com
bbwtubeporn.xxx
beeg-porn.com
befektetek.hu
beginnersinvest.about.com
belegger.nl
best binary options broker
best trading platform
bestbrokerbinaryoptions.com
betrug.org
bigprofitgenerator.com
bin.ua
binaereoptionen.com
binairesoptions.net
binarnyeopciony.ru
binary option broker
binary option reviews
binary option signals
binary option strategies
binary option trading
binary option trading strategies
binary options brands
binary options broker
binary options brokers
binary options daily
binary options demo account
binary options forum
binary options review
binary options reviews
binary options scam
binary options signals
binary options software
binary options strategies
binary options strategy
binary options trading platform
binary options trading strategies
binary options trading strategy
binary options trading system
binary options trading systems
binary software
binary trade
binary trading strategies
binary-brokers.com
binary-option-broker.com
binary-option-strategy.org
binary-option.co.uk
binary-options-broker.com
binary-options-brokers.com
binarygodfather.com
binarymachine.co
bolsamadrid.es
brainz.org
chirikkudukka.com
clickindia.com
commodityonline.com
compare binary option brokers
compare binary options brokers
corvetteforum.com
creatives.pichunter.com
crisiskiller.com
currencynewstrading.com
dailybinaryprofits.com
dailypaul.com
dailytradealert.com
davemanuel.com
delivery.trafficforce.com
deltastock.com
desertmillionaire.com
developunit.info
digital options
dollars2pounds.com
earnforex.com
earningswhispers.com
easy-forex
economywatch.com
ecosalon.com
elitetraderapp.com
emicalculator.net
enigmacode.co
epn.dk
equity trading
equityclock.com
etfdailynews.com
fast make money online
fast making money online
fast money making online
fast online money making
fastincomeapp.com
fastprofits.co
filthycampus.com
filthyrx.com
financialsense.com
finevids.com
firedoglake.com
fitwatch.com
fixtures365.com
forecasts.org
forex binary options
forex.pk
forex4noobs.com
forexdirectory.net
forexmagnates.com
forexpf.ru
forexstrategiesresources.com
forextime.com
forextraders.com
forextrading.about.com
forexyard.com
free make money online
free-vidz.com
freemoneysystem.co
freetrannytubes.com
french-girls.tv
futures.tradingcharts.com
futuresmag.com
fwdaff.com
fx-rate.net
fxtrade.oanda.com
gallery-dump.com
gaylikefuck.com
getmema.com
globaltrader365.com
guaranteedwealth.co
hindi.moneycontrol.com
hobbytalk.com
how make money online
humoron.com
hymarkets.com
i3investor.com
iboner.com
imgnext.com
imgserve.net
imgsin.com
indexsignal.com
indiainfoline.com
insiderjohn.com
insiderlegalbot.com
instantcashcreator.com
instanttradingprofit.com
intellitraders.com
investing.money.msn.com
investorshub.advfn.com
jizz.mobi
job-applications.com
kickasstorrents.come.in
la-calculatrice.com
ladies.de
liveprofits.co
madmovs.com
make fast money online
make free money online
make free online money
make money fast online
make money free online
make money from online
make money online at
make money online com
make money online fast
make money online free
make money online from
make money online to
make money online way
make money with online
make online free money
make online money fast
making fast money online
making money fast online
making money online fast
metatrader5.com
millionaireblueprint.co
millionairetrader.co
missionrevenge.com
mmb.moneycontrol.com
mobilebinarymachine.com
money making online
moviexshare.com
mql5.com
mybinaryrevenge.com
mycashbot.com
nadex binary options
nassr.com
newbie-trading-guide.com
nyx.com
olderhill.com
onetwotrade.com
online fast making money
online make money fast
online making money
online making money fast
opensourcespy.com
openthemagazine.com
optionbit.com
options traders
ozrobot.com
paper trading
paydayapp.co
perfectlegs.net
picsrom.com
pp.developunit.info
probinaryrobot.com
profitmaximizer.net
pureprofits.co
quiminet.com
quotes.ino.com
redferret.net
redvak.com
riskfreeprofits.net
savingsexp-a.akamaihd.net
secretmoneyvault.net
sgxniftydowfutureslive.com
sheshaft.com
shyteens.me
smarterlifestyles.com
stock trading online
stockpair
supremesa-a.akamaihd.net
swing trading
swissbinaryrobot.com
teenamp.com
testfull
the100kclub.co
tokyobot.com
topoption
traderxp.com
trading binary options
trading commodities
trading currency
trading for beginners
trading for dummies
trading oil
trading platform
trading simulator
trading stock online
trading systems
trading-for-winners.com
tradingeverest.com
traffic-delivery.com
travlang.com
verajohn
verifiedtrader.net
vitorrent.org
voyeurblog.net
wallstreet-online.de
wallstreetitalia.com
walterfootball.com
watch.dardarkom.com
wheresgeorge.com
widget.plugrush.com
winfxstrategy.com
xmania.fr
xnxx.vc
yoummy.com
zacks.com
zex.me
zonebourse.com

              </textarea>
            </span></td>
          </tr>
        </table></Center>

				</div>

			</section>


			<!-- PROMO BANNERS
			================================= -->
			<center><section id="banners" class="banners-section section">

				<div class="container">
                <h2 class="section-heading text-center">PROMO BANNERS</h2>
<br/>
<div class='download_cnt'>
			<div class="closing-buttons" data-animation="tada"><a href="{{ URL::asset('jv/assets/optionfigures.zip') }}" class="anchor-link btn btn-lg btn-primary">Download All Banners</a></div>
        </div>
		<br/>
        <p>
        </p>
        <div class='banners_promo_pics_wr'>
            <div class='banners_pics'>

            </div>
			<br/><br/>
            <div class='banners_pics'>

            </div>
        </div>

				</div>
<div class='banners_promo_pics_wr'>
            <h2 class="section-heading text-center">SET 1</h2>
            <div class='banners_pics'>
				<div class="container AC" id="banners">
				<div class="clear h50"></div>
										<div class="clear h5"></div>
					<img src="http://option-figures.com/jv/assets/img/optionfigures/v1/Option_figures_728x90.gif" />
					<div class="clear"></div>
					<textarea style="width:60%;height:50px;resize:none;"><a href="YOUR AFFILIATE LINK"><img src="http://option-figures.com/jv/assets/img/optionfigures/v1/Option_figures_728x90.gif" /></a></textarea>
					<div class="clear h50"></div>
										<div class="clear h5"></div>
					<img src="http://option-figures.com/jv/assets/img/optionfigures/v1/Option_figures_468x60.gif" />
					<div class="clear"></div>
					<textarea style="width:60%;height:50px;resize:none;"><a href="YOUR AFFILIATE LINK"><img src="http://option-figures.com/jv/assets/img/optionfigures/v1/Option_figures_468x60.gif" /></a></textarea>
					<div class="clear h50"></div>
										<div class="clear h5"></div>
					<img src="http://option-figures.com/jv/assets/img/optionfigures/v1/Option_figures_336x228.gif" />
					<div class="clear"></div>
					<textarea style="width:60%;height:50px;resize:none;"><a href="YOUR AFFILIATE LINK"><img src="http://option-figures.com/jv/assets/img/optionfigures/v1/Option_figures_336x228.gif" /></a></textarea>
					<div class="clear h50"></div>
										<div class="clear h5"></div>
					<img src="http://option-figures.com/jv/assets/img/optionfigures/v1/Option_figures_300x250.gif" />
					<div class="clear"></div>
					<textarea style="width:60%;height:50px;resize:none;"><a href="YOUR AFFILIATE LINK"><img src="http://option-figures.com/jv/assets/img/optionfigures/v1/Option_figures_300x250.gif" /></a></textarea>
					<div class="clear h50"></div>
										<div class="clear h5"></div>
					<img src="http://option-figures.com/jv/assets/img/optionfigures/v1/Option_figures_250x250.gif" />
					<div class="clear"></div>
					<textarea style="width:60%;height:50px;resize:none;"><a href="YOUR AFFILIATE LINK"><img src="http://option-figures.com/jv/assets/img/optionfigures/v1/Option_figures_250x250.gif" /></a></textarea>
					<div class="clear h50"></div>
										<div class="clear h5"></div>
					<img src="http://option-figures.com/jv/assets/img/optionfigures/v1/Option_figures_160x600.gif" />
					<div class="clear"></div>
					<textarea style="width:60%;height:50px;resize:none;"><a href="YOUR AFFILIATE LINK"><img src="http://option-figures.com/jv/assets/img/optionfigures/v1/Option_figures_160x600.gif" /></a></textarea>
					<div class="clear h50"></div>
										<div class="clear h5"></div>
					<img src="http://option-figures.com/jv/assets/img/optionfigures/v1/Option_figures_120x600.gif" />
					<div class="clear"></div>
					<textarea style="width:60%;height:50px;resize:none;"><a href="YOUR AFFILIATE LINK"><img src="http://option-figures.com/jv/assets/img/optionfigures/v1/Option_figures_120x600.gif" /></a></textarea>
					<div class="clear h50"></div>
								</div>

		</div>

		<br/><br/>
            <h2 class="section-heading text-center">SET 2</h2>
            <div class='banners_pics'>
			<div class="container AC" id="banners">
				<div class="clear h50"></div>
										<div class="clear h5"></div>
					<img src="http://option-figures.com/jv/assets/img/optionfigures/v2/Option_figures_728x90.gif" />
					<div class="clear"></div>
					<textarea style="width:60%;height:50px;resize:none;"><a href="YOUR AFFILIATE LINK"><img src="http://option-figures.com/jv/assets/img/optionfigures/v2/Option_figures_728x90.gif" /></a></textarea>
					<div class="clear h50"></div>
										<div class="clear h5"></div>
					<img src="http://option-figures.com/jv/assets/img/optionfigures/v2/Option_figures_468x60.gif" />
					<div class="clear"></div>
					<textarea style="width:60%;height:50px;resize:none;"><a href="YOUR AFFILIATE LINK"><img src="http://option-figures.com/jv/assets/img/optionfigures/v2/Option_figures_468x60.gif" /></a></textarea>
					<div class="clear h50"></div>
										<div class="clear h5"></div>
					<img src="http://option-figures.com/jv/assets/img/optionfigures/v2/Option_figures_336x228.gif" />
					<div class="clear"></div>
					<textarea style="width:60%;height:50px;resize:none;"><a href="YOUR AFFILIATE LINK"><img src="http://option-figures.com/jv/assets/img/optionfigures/v2/Option_figures_336x228.gif" /></a></textarea>
					<div class="clear h50"></div>
										<div class="clear h5"></div>
					<img src="http://option-figures.com/jv/assets/img/optionfigures/v2/Option_figures_300x250.gif" />
					<div class="clear"></div>
					<textarea style="width:60%;height:50px;resize:none;"><a href="YOUR AFFILIATE LINK"><img src="http://option-figures.com/jv/assets/img/optionfigures/v2/Option_figures_300x250.gif" /></a></textarea>
					<div class="clear h50"></div>
										<div class="clear h5"></div>
					<img src="http://option-figures.com/jv/assets/img/optionfigures/v2/Option_figures_250x250.gif" />
					<div class="clear"></div>
					<textarea style="width:60%;height:50px;resize:none;"><a href="YOUR AFFILIATE LINK"><img src="http://option-figures.com/jv/assets/img/optionfigures/v2/Option_figures_250x250.gif" /></a></textarea>
					<div class="clear h50"></div>
										<div class="clear h5"></div>
					<img src="http://option-figures.com/jv/assets/img/optionfigures/v2/Option_figures_160x600.gif" />
					<div class="clear"></div>
					<textarea style="width:60%;height:50px;resize:none;"><a href="YOUR AFFILIATE LINK"><img src="http://option-figures.com/jv/assets/img/optionfigures/v2/Option_figures_160x600.gif" /></a></textarea>
					<div class="clear h50"></div>
										<div class="clear h5"></div>
					<img src="http://option-figures.com/jv/assets/img/optionfigures/v2/Option_figures_120x600.gif" />
					<div class="clear"></div>
					<textarea style="width:60%;height:50px;resize:none;"><a href="YOUR AFFILIATE LINK"><img src="http://option-figures.com/jv/assets/img/optionfigures/v2/Option_figures_120x600.gif" /></a></textarea>
					<div class="clear h50"></div>
								</div>

			</section></center>


						<!-- CLOSING
			================================= -->
			<section id="closing" class="closing-section section-dark section">

				<div class="section-background">

					<!-- IMAGE BACKGROUND -->
					<div class="section-background-image parallax" data-stellar-ratio="0.4">
						<img src="{{ URL::asset('jv/images/backgrounds/slide-1.jpg') }}" alt="" style="opacity: 0.15;">
					</div>

				</div>

				<div class="container">

					<h3 class="closing-shout">Sign Up For Our JV Leaderboard Contest And HUGE CASH Prizes!</h3>

					<div class="closing-buttons" data-animation="tada"><a href="#hero" class="anchor-link btn btn-lg btn-primary">START NOW</a></div>

				</div>

			</section>


			<!-- FOOTER
			================================= -->
			<section id="footer" class="footer-section section">

				<div class="container">

					<h3 class="footer-logo">

					</h3>

					<div class="footer-socmed">

					</div>

					<div class="footer-copyright">
						&copy; 2016 OptionFigures
					</div>

				</div>

			</section>

		</div>


<script src="//static.getclicky.com/js" type="text/javascript"></script>
<script type="text/javascript">try{ clicky.init(100826572); }catch(e){}</script>
<noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/100826572ns.gif" /></p></noscript>

		<input type="hidden" id="hidLocation" value="http://sumopixel.com/" />
<script type="text/javascript" src="http://sumopixel.com/landing.js"></script>

@stop
