@extends('layout.default')
@section('content')

  <header class="header-main"> @include('layout.header')</header>

  <section class="basic_section tos">
    <div class="container haveborder dark_grey">
      <div class="tos_holder">
        <h2>RISK DISCLAIMER</h2>
        <div class="con">
          <p style="text-align: justify;">Trading in any financial market involves substantial risk of loss and is not suitable for all investors and traders. Any style of trading in any market condition is extremely risky and can result in financial losses in a very short period of time. There is considerable exposure to risk in any transaction including but not limited to, the potential for changing political and/or economic conditions that may substantially affect the price or liquidity of a trade. Trading is a challenging and potentially profitable opportunity for those who are educated and experienced in trading. Before deciding to participate in the markets, you should carefully consider your objectives, level of experience and risk appetite. Most importantly, do not invest money that you cannot afford to lose.</p>
          <p style="text-align: justify;">&nbsp;</p>
          <p style="text-align: justify;">OptionFigures is not offering to buy or sell any of the financial instruments mentioned in any service we offer and is not representing itself as a registered investment advisor or broker dealer and does not provide investment advice. OptionFigures does not guarantee or represent that members/users acting upon any suggestion mentioned or discussed in any of the services we offer or the Materials provided, will result in a profit. All decisions to act upon any suggestions made in any service or Materials we offer is the sole responsibility of the member/ user.</p>
          <p style="text-align: justify;">&nbsp;</p>
          <p style="text-align: justify;">Nothing on the OptionFigures Site should be construed as an invitation to trade. The risks involved in binary trading are high and may not suit every type of investor.&nbsp; OptionFigures will not be held responsible or liable to members/ users or any other parties for losses that may be sustained while trading. YOUR trading and financial actions taken are solely YOUR decision and not that of OptionFigures.</p>
          <p style="text-align: justify;">Revised: March 19, 2016</p>
          <p style="text-align: justify;">&nbsp;</p>
        </div>
      </div>
    </div>
  </section>

  <footer>
    <div class='foot_navi'>
      <div class="container">
        <ul class="clearfix">
          <li><a href="<?php echo url('/'); ?>/terms">Terms & Conditions</a></li>
          <li>|</li>
          <li><a href="<?php echo url('/'); ?>/privacy">Privacy Policy</a></li>
          <li>|</li>
          <li><a href="<?php echo url('/'); ?>/risk_disclaimer">Risk Disclaimer</a></li>
        </ul>
      </div>
    </div>
  </footer>

@stop
