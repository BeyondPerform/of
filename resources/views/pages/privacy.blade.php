@extends('layout.default')
@section('content')

  <header class="header-main"> @include('layout.header')</header>

  <section class="basic_section tos">
    <div class="container haveborder dark_grey">
      <div class="tos_holder">
        <h2>PRIVACY POLICY</h2>
        <div class="con">
          <p style="text-align: justify;"><strong>Privacy Policy</strong><br> This Privacy Policy sets out how OptionFigures uses and protects any information that you provide when you use this website. Should OptionFigures ask you to provide certain information by which you can be identified when using this website, you can be assured that it will only be used in accordance with this Privacy Policy.<br> OptionFigures is firmly committed to preserving and protecting the privacy of all visitors and users of this website. We take strong measures to protect your privacy and the confidentiality of any information that you supply us with and in using it in a fair and lawful manner. We do not share, sell, or disclose any personally identifiable information that we collect with third parties. By using our website and the services provided, you abide to the Privacy Policy here on this page.Please ensure that you read the following Privacy Policy in order to understand how we use and protect the information that you provide us. By using this website, you consent to us using your personal information as set out below.</p>
          <p style="text-align: justify;">&nbsp;</p>
          <p style="text-align: justify;"><strong>1. What Personal Information do we collect?</strong><br> If you apply to open an account and purchase course material with OptionFigures, there is certain information that we will require from you in order to activate the account. This information includes but is not limited to personal details such as name, address, date of birth, contact details, email address and other necessary financial information. From time to time we may also request further information to help us improve our service to you. We obtain most of the information directly from our customers through application or other forms, and from maintaining records of information provided in the course of ongoing customer service.</p>
          <p style="text-align: justify;">&nbsp;</p>
          <p style="text-align: justify;"><strong>2. How do we use this Information?</strong><br> OptionFigures will use information collected from you to provide the information and services requested by you and to improve the quality of our service.<br> We may also collect information about your use of the website, such as pages visited, frequency and trading activities. Your domain IP is recognized by our servers and the pages that you visit on our site are recorded. We may use this information in a collective way in order to carry out quality control and improvements to our site.<br> In addition, your email address will be recorded and stored on our servers in order:</p>
          <ul>
          <li>to send you news about the services to which you have signed up;</li>
          <li>to tell you about our services or provide you with access to services which you request;</li>
          <li>to enable us to answer your queries;</li>
          <li>for verifying your identity for security purposes;</li>
          <li>for marketing our services and other products;</li>
          <li>to help make our web site as useful to you as possible.</li>
          </ul>
          <p style="text-align: justify;">If you do not want us to use your personal information, please let OptionFigures know in writing. If you decide to do so, we may not be able to continue to provide information, services and/or products requested by you and we will have no liability to you in respect of the same.</p>
          <p style="text-align: justify;">&nbsp;</p>
          <p style="text-align: justify;"><strong>3. Disclosure of Information</strong><br> We are subject to the law like everyone else. Therefore we may be required to provide requested information to Government bodies or legal authorities if requested.<br> We will only supply requested information with the appropriate authorization such as a search warrant or if a court order is provided.</p>
          <p style="text-align: justify;">&nbsp;</p>
          <p style="text-align: justify;"><strong>4. Security</strong><br> OptionFigures is committed to ensuring that your personal information is secure. In order to prevent unauthorized access or disclosure, we have put in place suitable electronic, physical, and managerial procedures to secure and safeguard the information we collect online.</p>
          <p>&nbsp;</p>
          <p><strong>5. Updating your Information</strong><br> You may inform us at any time that your personal details have changed or that you wish us to delete the personal information we hold about you.<br> We will change or delete your personal information in accordance with your instructions, except to the extent that we are required to hold your personal information for regulatory or legal purposes, to provide you with the services you have requested or to maintain adequate business records.</p>
          <p style="text-align: justify;">&nbsp;</p>
          <p style="text-align: justify;"><strong>6. Use of Cookies</strong><br> If you use the OptionFigures website, this enables us to use cookies in relation to your access to this website. Cookies are small files of information, which often include a unique identification number or value, which are stored on your computer’s hard drive as a result of you using and accessing this website. The purpose of this information is to provide you with a more relevant and effective experience on this website, including presenting web pages according to your needs or preferences. Cookies are frequently used on many websites on the internet and you can choose if and how a cookie will be accepted by changing your preferences and options in your browser. You may not be able to access some parts of this site if you choose to disable the cookie acceptance in your browser, particularly the secure parts of the website. We therefore recommend you enable cookie acceptance to benefit from all the services on the website.This site and third-party vendors, including Google, use first-party cookies (such as the Google Analytics cookie) and third-party cookies (such as the DoubleClick cookie) together to inform, optimize, and serve ads based on your past visits to this website.These cookies enable tracking and may be used for remarketing purposes across the Google display network and possibly other advertising networks. You can opt-out of this advertising by using the ad-preferences manager.</p>
          <p style="text-align: justify;">&nbsp;</p>
          <p style="text-align: justify;"><strong>7. Links to Other Websites</strong><br> This website contains links to other sites and you should be aware that these sites are in no way controlled by us. Each site will have its own privacy policies and terms of use. Therefore we strongly suggest that you review the Privacy Policy and Terms of Conditions of each website that you visit. This includes any sites that are linked to from this site.</p>
          <p style="text-align: justify;">&nbsp;</p>
          <p style="text-align: justify;"><strong>8. Amendments to the Privacy Policy</strong><br> This Privacy Policy statement will be reviewed from time to time in order to take into account new laws and technology, changes to our operations and practices and to ensure it remains appropriate to the changing environment.We reserve the right to change this Policy at any time. If any changes are made, an updated version will be posted on this page. It is your responsibility to check our Privacy Policy for future changes.</p>
          <p style="text-align: justify;">Revised: March 19, 2016</p>
          <p style="text-align: justify;">&nbsp;</p>
        </div>
      </div>
    </div>
  </section>

  <footer>
    <div class='foot_navi'>
      <div class="container">
        <ul class="clearfix">
          <li><a href="<?php echo url('/'); ?>/terms">Terms & Conditions</a></li>
          <li>|</li>
          <li><a href="<?php echo url('/'); ?>/privacy">Privacy Policy</a></li>
          <li>|</li>
          <li><a href="<?php echo url('/'); ?>/risk_disclaimer">Risk Disclaimer</a></li>
        </ul>
      </div>
    </div>
  </footer>

@stop
