<div class="modal fade in broker_api_call_modal" id="tradesolid_acc_countries">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Accepted countries</h4>
      </div>
      <div class="modal-body">
        <ul>
          <li>Netherland</li>
          <li>Germany</li>
          <li>Norway</li>
          <li>Iceland<l>
          <li>Switzerland</li>
          <li>Sweden</li>
          <li>Malaysia</li>
          <li>Australia</li>
          <li>Hong Kong</li>
          <li>South Africa</li>
          <li>United Arab Emirates</li>
          <li>Saudi Arabia</li>
          <li>Qatar</li>
        </ul>
      </div>
    </div>
  </div>
</div>

<div class="modal fade in broker_api_call_modal" id="option500_acc_countries">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Accepted countries</h4>
      </div>
      <div class="modal-body">
        <ul>
          <li>United Kingdom</li>
          <li>Australia</li>
          <li>New Zealand</li>
          <li>Canada</li>
          <li>Singapore</li>
          <li>Hong Kong</li>
          <li>Malaysia</li>
          <li>United Arab Emirates</li>
          <li>South Africa</li>
        </ul>
      </div>
    </div>
  </div>
</div>

<div class="modal fade in broker_api_call_modal" id="tradecom_acc_countries">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Accepted countries</h4>
      </div>
      <div class="modal-body">
        <ul>
          <li>United Kingdom</li>
          <li>South Africa</li>
          <li>Italy</li>
        </ul>
      </div>
    </div>
  </div>
</div>

<div class="modal fade in broker_api_call_modal" id="topoption_acc_countries">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Accepted countries</h4>
      </div>
      <div class="modal-body">
        <ul>
          <li>Worldwide</li>
        </ul>
      </div>
    </div>
  </div>
</div>

<div class="modal fade in broker_api_call_modal" id="avatrade_acc_countries">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Accepted countries</h4>
      </div>
      <div class="modal-body">
        <ul>
          <li>United Kingdom</li>
          <li>Canada</li>
          <li>Australia</li>
          <li>France</li>
          <li>Italy</li>
          <li>Spain</li>
          <li>Norway</li>
          <li>Denmark</li>
        </ul>
      </div>
    </div>
  </div>
</div>


<div class="modal fade in broker_api_call_modal" id="keyoption_acc_countries">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Accepted countries</h4>
      </div>
      <div class="modal-body">
        <ul>
          <li>Spain</li>
        </ul>
      </div>
    </div>
  </div>
</div>

<div class="modal fade in broker_api_call_modal" id="optionlive_acc_countries">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Accepted countries</h4>
      </div>
      <div class="modal-body">
        <ul>
          <li>France</li>
        </ul>
      </div>
    </div>
  </div>
</div>

<div class="modal fade broker_api_call_modal in" id="broker_api_call_modal" aria-hidden="false">
  <div class="modal-dialog">
    <form>
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
          <h4 class="modal-title">Open Account: <span id="broker_name" class="broker_name"></span></h4>
        </div>
        <div class="modal-body">
          <input type="hidden" name="_token" value="{{csrf_token()}}">
          <div class="two_rows_wr clearfix">
            <input type="text" class="form-control" id="fname" name="fname" placeholder="First Name">
            <input type="text" class="form-control" id="lname" name="lname" placeholder="Last Name">
          </div>
          <div class="gen_rows_wr">
            <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone">
          </div>
          <div class="gen_rows_wr">
            <input type="text" class="form-control" id="email" name="email" placeholder="Email">
          </div>
          <div class="gen_rows_wr">
            <input type="password" class="form-control" id="psw" name="psw" placeholder="Password">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" id="broker_api_sbm" onclick="return false">Open</button>
        </div>
      </div>
    </form>
  </div>
</div>
