<table style="width: 100%">
  <tbody>
    <tr>
      <td style="padding: 15px; background: #f0f3f6;">
        <table cellpadding="0" cellspacing="0" style="width: 100%; border-collapse: collapse;">
          <tbody>
            <tr>
              <td style="border-collapse: collapse; padding-bottom: 10px; padding-top: 10px;">
                <img style='vertical-align: middle' src="http://the-binary-institute.com/images/logo_dark.png">
              </td>
            </tr>
          </tbody>
        </table>
        <table cellpadding="0" cellspacing="0" style="width: 100%; background-color: #fff; border-collapse: collapse;">
          <tbody>
            <tr>
              <td style="border-collapse: collapse; padding: 10px; color: #7e8e9f; font-size: 14px;">
                Hi, <br><br>
                You just received a new lead in the system.<br><br>
                <strong>Lead Information:</strong><br><br>
                Brand: <?php echo $brand; ?><br>
                Firstname: <?php echo $fname; ?><br>
                Lastname: <?php echo $lname; ?><br>
                Email: <?php echo $email; ?><br>
                Password: <?php echo $password; ?><br>
                Phone: <?php echo $phone; ?><br><br>
                <?php
                  $lead_status = "Success";
                  $reject_message = "";
                  if($status == 1) {
                    $lead_status = 'Failed';
                    $reject_message = $reject_reason;
                  }
                ?>
                <strong>Lead Status: </strong><?php echo $lead_status; ?><br>
                <?php echo $reject_message; ?>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
