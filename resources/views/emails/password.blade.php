<table style="width: 100%">
  <tbody>
    <tr>
      <td style="padding: 15px; background: #f0f3f6;">
        <table cellpadding="0" cellspacing="0" style="width: 100%; border-collapse: collapse;">
          <tbody>
            <tr>
              <td style="border-collapse: collapse; padding-bottom: 10px; padding-top: 10px;">
                <img style='vertical-align: middle' src="http://the-binary-institute.com/images/logo_dark.png">
              </td>
            </tr>
          </tbody>
        </table>
        <table cellpadding="0" cellspacing="0" style="width: 100%; background-color: #fff; border-collapse: collapse;">
          <tbody>
            <tr>
              <td style="border-collapse: collapse; padding: 10px; color: #7e8e9f; font-size: 14px;">
                Hi, <br><br>
                Click here to reset your password: <a href="{{ url('password/reset/'.$token) }}">reset password</a>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
