<?php
	$controller = substr(class_basename(Route::currentRouteAction()), 0, (strpos(class_basename(Route::currentRouteAction()), '@') -0) );
	$action = substr(class_basename(Route::currentRouteAction()), (strpos(class_basename(Route::currentRouteAction()), '@') + 1));
?>

<div id="sidebar">

  <div id="avatar">
    <div class="container-fluid rubix-grid" style="z-index:9999997;">
      <div class="row fg-white">
        <div class="col-xs-4 col-xs-collapse-right">
          <img src="<?php echo url('/'); ?>/calc/images/default_avatar.png" width="40" height="40">
        </div>
        <div class="col-xs-8 col-xs-collapse-left" id="avatar-col">
          <div style="top:23px;font-size:16px;position:relative;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;width:160px;">
						{{Auth::user()->email}}
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id='sidebar-container'>
    <div class='sidebar ps-container ps-active-y'>
      <div class='container-fluid rubix-grid'>
        <div class='row'>
          <div class='col-xs-12'>
            <div class='sidebar-nav-container'>
              <ul class='sidebar-nav'>
                <li <?php if($action == 'index') { ?> class='active' <?php } ?>>
                  <a href="<?php echo url('/'); ?>/calculator/home">
                    <span class='name'>Dashboard</span>
                  </a>
                </li>
                <li <?php if($action == 'currencies') { ?> class='active' <?php } ?>>
                  <a href="<?php echo url('/'); ?>/calculator/currencies">
                    <span class='name'>Currencies</span>
                  </a>
                </li>
                <li <?php if($action == 'commodities') { ?> class='active' <?php } ?>>
                  <a href="<?php echo url('/'); ?>/calculator/commodities">
                    <span class='name'>Commodities</span>
                  </a>
                </li>
                <li <?php if($action == 'indices') { ?> class='active' <?php } ?>>
                  <a href="<?php echo url('/'); ?>/calculator/indices">
                    <span class='name'>Indices</span>
                  </a>
                </li>
                <!-- <li <?php if($action == 'brokers') { ?> class='active' <?php } ?>>
                  <a href="<?php echo url('/'); ?>/calculator/brokers">
                    <span class='name'>Brokers</span>
                  </a>
                </li> -->
                <li <?php if($action == 'userguide') { ?> class='active' <?php } ?>>
                  <a href="<?php echo url('/'); ?>/calculator/userguide">
                    <span class='name'>User Guide</span>
                  </a>
                </li>
								<li <?php if($action == 'rbrokers') { ?> class='active' <?php } ?>>
                  <a href="<?php echo url('/'); ?>/calculator/rbrokers">
                    <span class='name'>Recommended brokers</span>
                  </a>
                </li>
								<li <?php if($action == 'promotions') { ?> class='active' <?php } ?>>
                  <a href="<?php echo url('/'); ?>/calculator/promotions">
                    <span class='name'>Exclusive promotions</span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>
