<div class="modal fade in calc_progress_modal" id="calc_progress_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class='modal-header'>
        <img src="<?php echo url('/'); ?>/images/logoimg.png">
      </div>
      <div class="modal-body">
        <p class='segment_hl'>OptionFigures is Calculating ...</p>
        <p class='text-center'>DONOT refresh page!</p>
        <div id="mc_progress" class="progress">
          <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
            <span>0%</span>
          </div>
        </div>
        <div class='arrows_wr'>
          <ul class='clearfix'>
            <li><span id="mc_arrow_1" class='icon-nargela-arrow mc_arrow'></span></li>
            <li><span id="mc_arrow_2" class='icon-nargela-arrow mc_arrow'></span></li>
            <li><span id="mc_arrow_3" class='icon-nargela-arrow mc_arrow'></span></li>
            <li><span id="mc_arrow_4" class='icon-nargela-arrow mc_arrow'></span></li>
            <li><span id="mc_arrow_5" class='icon-nargela-arrow mc_arrow'></span></li>
          </ul>
        </div>
        <textarea class='form-control logs' readonly></textarea>
      </div>
    </div>
  </div>
</div>

<div class="modal fade in calc_progress_modal calc_results_modal" id="calc_results_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class='modal-header'>
        <img src="<?php echo url('/'); ?>/images/logoimg.png">
      </div>
      <div class="modal-body">
        <p class='segment_hl'>Your Results</p>
        <div class='inside_c clearfix'>
          <div class='chart_holder'>
            <div id="modal_progress_chart"></div>
          </div>
          <div class='status_info'>
            <p><span class='boldy'>Asset Chosen:</span> <span class='choosen_asset'>not selected</span></p>
            <p><span class='boldy'>Timeslot:</span> <span class='choosen_timeslot'>not selected</span></p>
            <div class='status_inidicator_holder clearfix'>
              <div class='status_inidicator'>
                <div class='shapes_bean'></div>
              </div>
              <div class='status_inidicator_txt'>
                <p class='shapes_bean_txt'>UP</p>
              </div>
            </div>
          </div>
        </div>

        <p class='black_hl'>Trade with confidence</p>

        <div class='brokers_segment'>
          <div class='promo_acc_wrap'>
            <div class='promo_line_wr clearfix'>
              <div class='logo_wr'>
                <div class='logo_placeholder'>
                  <img src="<?php echo url('/'); ?>/calc/images/zoomtrader137x66.png">
                </div>
              </div>
              <p class='title'>
                Zoom Trader<br>
                <a href='javascript: void(0)' data-toggle="modal" data-target="#zoomtrader_acc_countries">Accepted countries</a>
              </p>
              <p class='discount'>100% Bonus</p>
              <div class='cnt'>
                <div class='clearfix'>
                  <a class='step1' href='javascript: void(0)' onclick="activateApiCallModal('Zoom Trader')">
                    <p>Step 1</p>
                    <p>Open Account</p>
                  </a>
                  <a class='step2' href='https://www.zoomtraderglobal.com/deposit'>
                    <p>Step 2</p>
                    <p>Fund Account</p>
                  </a>
                </div>
              </div>
            </div>

            <div class='promo_line_wr clearfix'>
              <div class='logo_wr'>
                <div class='logo_placeholder'>
                  <img src="<?php echo url('/'); ?>/calc/images/optionstars-logo.png">
                </div>
              </div>
              <p class='title'>
                OptionStars<br>
                <a href='javascript: void(0)' data-toggle="modal" data-target="#optionstars_acc_countries">Accepted countries</a>
              </p>
              <p class='discount'>100% Bonus</p>
              <div class='cnt'>
                <div class='clearfix'>
                  <a class='step1' href='javascript: void(0)' onclick="activateApiCallModal('OptionStars')">
                    <p>Step 1</p>
                    <p>Open Account</p>
                  </a>
                  <a class='step2' href='javascript: void(0)'>
                    <p>Step 2</p>
                    <p>Fund Account</p>
                  </a>
                </div>
              </div>
            </div>
           </div>
        </div>
        <p class='desc'>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        </p>
      </div>
    </div>
  </div>
</div>

<!-- ========= BROKERS INFO MODALS (START) ========= -->
<div class="modal fade in broker_api_call_modal" id="zoomtrader_acc_countries">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Accepted countries</h4>
      </div>
      <div class="modal-body">
        <ul>
          <li>Germany</li>
          <li>GCC (ARAB)</li>
          <li>Sweden</li>
          <li>Denmark</li>
          <li>Finland</li>
          <li>Norway</li>
          <li>Austria</li>
          <li>Switzerland</li>
          <li>Canada</li>
          <li>Australia</li>
          <li>France</li>
          <li>Italy</li>
          <li>United Kindom</li>
          <li>Ireland</li>
          <li>Netherlands</li>
          <li>New Zealand</li>
          <li>Singapore</li>
        </ul>
      </div>
    </div>
  </div>
</div>

<div class="modal fade in broker_api_call_modal" id="optionstars_acc_countries">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Accepted countries</h4>
      </div>
      <div class="modal-body">
        <ul>
          <li>Germany</li>
          <li>GCC (ARAB)</li>
          <li>Sweden</li>
          <li>Denmark</li>
          <li>Finland</li>
          <li>Norway</li>
          <li>Austria</li>
          <li>Switzerland</li>
          <li>Canada</li>
          <li>Australia</li>
          <li>France</li>
          <li>Italy</li>
          <li>United Kindom</li>
          <li>Ireland</li>
          <li>Netherlands</li>
          <li>New Zealand</li>
          <li>Singapore</li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!-- ========= BROKERS INFO MODALS (END) ========= -->

<div class="modal fade in broker_api_call_modal" id="broker_api_call_modal">
  <div class="modal-dialog">
    <form>
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Open Account: <span id='broker_name' class='broker_name'></span></h4>
        </div>
        <div class="modal-body">
          <div class='two_rows_wr clearfix'>
            <input type="text" class="form-control" id="fname" name="fname" placeholder="First Name">
            <input type="text" class="form-control" id="lname" name="lname" placeholder="Last Name">
          </div>
          <div class='gen_rows_wr'>
            <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone">
          </div>
          <div class='gen_rows_wr'>
            <input type="text" class="form-control" id="email" name="email" placeholder="Email">
          </div>
          <div class='gen_rows_wr'>
            <input type="password" class="form-control" id="psw" name="psw" placeholder="Password">
          </div>
        </div>
        <div class='modal-footer'>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" id='broker_api_sbm' onclick="return openBrokerAccount()">Open</button>
        </div>
      </div>
    </form>
  </div>
</div>

<div id="footer-container">
  <div class='container-fluid rubix-grid text-center' id='footer'>
    <div class='row'>
      <div class='col-xs-12'>
        <span>&copy; 2017 OptionFigures</span>
      </div>
    </div>
  </div>
</div>

<script>
  var base_url = "<?php echo url('/'); ?>";
</script>

<script src="{{ URL::asset('build/js/vendor.js') }}"></script>
<!-- <script src="{{ URL::asset('calc/js/jquery-1.9.0.min.js') }}"></script>
<script src="{{ URL::asset('calc/js/bootstrap.min.js') }}"></script> -->
<script src="{{ URL::asset('calc/js/highstock.js') }}"></script>
<script src="{{ URL::asset('calc/js/bootstrap-notify.min.js') }}"></script>
<script src="{{ URL::asset('calc/js/jquery.scrollTo.min.js') }}"></script>
<script src="{{ URL::asset('calc/js/custom_app.js') }}"></script>
<script src="{{ URL::asset('build/js/leads.js') }}"></script>
