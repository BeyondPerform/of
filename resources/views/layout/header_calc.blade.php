<div class='container-fluid rubix-grid' id="navbar">
  <div class='row'>
    <div class='col-xs-12'>
      <nav class='navbar navbar-default navbar-fixed-top' role='navigation' id='rubix-nav-header'>
        <div class='container-fluid'>
          <div class='row'>
            <div class='col-xs-3 visible-xs'>
              <div class='navbar-content pull-left visible-xs-inline-block'>
                <ul class='nav navbar-nav'>
                  <li class='sidebar-btn' data-id='sidebar-btn'>
                    <a class='sidebar-btn' data-id='sidebar-btn' href="<?php echo url('/'); ?>">
                      <span class='rubix-icon fontello icon-fontello-th-list-5'></span>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div class='col-xs-6 col-sm-4'>
              <div class='navbar-header'>
                <a class='navbar-brand' href="<?php echo url('/'); ?>">
                  <img src="<?php echo url('/'); ?>/images/logoimg.png">
                </a>
              </div>
            </div>
            <div class='col-xs-3 col-sm-8'>
              <div class='navbar-content pull-right'>
                <ul class='nav navbar-nav'>
                  <li class='logout'>
                    <a class='logout' href="<?php echo url('/'); ?>/logout">
                      <span class='rubix-icon fontello icon-fontello-off-1'></span>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </nav>
    </div>
  </div>
</div>
