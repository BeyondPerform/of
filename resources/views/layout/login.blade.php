<!doctype html>
<html>
<head>
	<title>Option-Figures.com | Industry First Binary Options Calculator</title>
	<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
	<link rel="stylesheet" href="{{ URL::asset('build/css/vendor.css') }}">
	<link rel='stylesheet' href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{ URL::asset('build/css/elements.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('build/css/public.css') }}">

	<link rel="icon" type="image/png" href="<?php echo url('/'); ?>/favicon.png" />
  <link rel="apple-touch-icon" href="<?php echo url('/'); ?>/favicon.png"/>
</head>

<body class="common_login_layout">
	@yield('content')
	@include('layout.footer')
</body>
</html>
