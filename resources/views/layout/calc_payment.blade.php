<!doctype html>
<html class="default">
<head>
	<title>Option-Figures.com | Industry First Binary Options Calculator</title>
	<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

	<link rel="stylesheet" href="{{ URL::asset('build/css/public.css') }}">
	<link media="screen,print" rel="stylesheet" type="text/css" href="{{ URL::asset('calc/css/main.css') }}">
	<link media="screen,print" rel="stylesheet" type="text/css" href="{{ URL::asset('calc/css/theme.css') }}">
	<link media="screen,print" rel="stylesheet" type="text/css" href="{{ URL::asset('calc/css/colors.css') }}">
	<link media="screen,print" rel="stylesheet" type="text/css" href="{{ URL::asset('calc/css/font-faces.css') }}">
	<link media="screen,print" rel="stylesheet" type="text/css" href="{{ URL::asset('calc/css/fonts.css') }}">
	<link media="screen,print" rel="stylesheet" type="text/css" href="{{ URL::asset('calc/css/custom_main.css') }}">

	<link rel="icon" type="image/png" href="<?php echo url('/'); ?>/favicon.png" />
  <link rel="apple-touch-icon" href="<?php echo url('/'); ?>/favicon.png"/>
</head>

<body>
	<div id="app-container">

		<div class='dashboard' id='container'>
			@include('layout.sidebar_calc')
			@include('layout.header_calc')
			<div id="body">
		    <div class='container-fluid rubix-grid'>
		      @yield('content')
		    </div>
		  </div>
		</div>

	</div>

  <script>
    var base_url = "<?php echo url('/'); ?>";
		var fname = "<?php echo $user->fname; ?>";
		var lname = "<?php echo $user->lname; ?>";
		console.log(fname);
		console.log(lname);
  </script>
  <script src="{{ URL::asset('build/js/vendor.js') }}"></script>
  <script src="https://js.braintreegateway.com/v2/braintree.js"></script>
	<script src="{{ URL::asset('calc/js/jquery.payform.min.js') }}"></script>
	<script src="{{ URL::asset('calc/js/underscore-min.js') }}"></script>
  <script src="{{ URL::asset('build/js/payment.js') }}"></script>

</body>
</html>
