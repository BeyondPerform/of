<!doctype html>
<html>
<head>
	<title>OptionFigures - JV & Affiliates Opportunities</title>
	<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Montserrat:400,700">
  <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800">

  <link rel="stylesheet" href="{{ URL::asset('build/css/vendor.css') }}">
	<link rel='stylesheet' href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{ URL::asset('jv/css/animate.min.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('jv/css/style.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('jv/css/update.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('jv/css/colors/orange.css') }}">

	<link rel="icon" type="image/png" href="<?php echo url('/'); ?>/favicon.png" />
  <link rel="apple-touch-icon" href="<?php echo url('/'); ?>/favicon.png"/>
</head>

<body class="enable-animations enable-preloader">
	@yield('content')

  <script>
    var base_url = "<?php echo url('/'); ?>";
  </script>
  
  <script src="{{ URL::asset('build/js/vendor.js') }}"></script>
  <script src="{{ URL::asset('jv/js/respimage.min.js') }}"></script>
	<script src="{{ URL::asset('jv/js/jpreloader.min.js') }}"></script>
  <script src="{{ URL::asset('jv/js/smoothscroll.min.js') }}"></script>
  <script src="{{ URL::asset('jv/js/jquery.nav.min.js') }}"></script>
  <script src="{{ URL::asset('jv/js/jquery.inview.min.js') }}"></script>
  <script src="{{ URL::asset('jv/js/jquery.counterup.min.js') }}"></script>
  <script src="{{ URL::asset('jv/js/jquery.stellar.min.js') }}"></script>
  <script src="{{ URL::asset('jv/js/js.cookie.js') }}"></script>
  <script src="{{ URL::asset('jv/js/script.js') }}"></script>
  <script src="{{ URL::asset('jv/js/screen.js') }}"></script>
</body>
</html>
