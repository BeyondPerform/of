<!doctype html>
<html class="default">
<head>
	<title>Option-Figures.com | Industry First Binary Options Calculator</title>
	<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
	<link rel="stylesheet" href="{{ URL::asset('build/css/vendor.css') }}">
	<link media="screen,print" rel="stylesheet" type="text/css" href="{{ URL::asset('calc/css/main.css') }}">
	<link media="screen,print" rel="stylesheet" type="text/css" href="{{ URL::asset('calc/css/theme.css') }}">
	<link media="screen,print" rel="stylesheet" type="text/css" href="{{ URL::asset('calc/css/colors.css') }}">
	<link media="screen,print" rel="stylesheet" type="text/css" href="{{ URL::asset('calc/css/font-faces.css') }}">
	<link media="screen,print" rel="stylesheet" type="text/css" href="{{ URL::asset('calc/css/fonts.css') }}">
	<link media="screen,print" rel="stylesheet" type="text/css" href="{{ URL::asset('calc/css/custom_main.css') }}">

	<link rel="icon" type="image/png" href="<?php echo url('/'); ?>/favicon.png" />
  <link rel="apple-touch-icon" href="<?php echo url('/'); ?>/favicon.png"/>
</head>

<body>

	<div id="app-container">

		<div class='dashboard' id='container'>
			@include('layout.sidebar_calc')
			@include('layout.header_calc')
			<div id="body">
		    <div class='container-fluid rubix-grid'>
		      @yield('content')
		    </div>
		  </div>
			@include('layout.footer_calc')
		</div>

	</div>


</body>
</html>
