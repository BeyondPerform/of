<?php if(count($push) > 0) { ?>
	<table class='table'>
		<thead>
			<tr>
				<th>Brand</th>
				<th>Status</th>
				<th>Date</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($push as $p) { ?>
			<tr>
				<td><?php echo $p['brand']; ?></td>
				<td>
					<?php if($p['status'] == 1) { ?>
						<div class='alert alert-success'><p><strong>Success</strong></p></div>
					<?php } else { ?>
						<div class='alert alert-danger'><p><strong>Declined</strong></p></div>
						<div>
							<p><?php echo $p['response']; ?></p>
						</div>
					<?php } ?>
				</td>
				<td><?php echo $p['created_at']; ?></td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
<?php } else { ?>
	<div class='alert alert-danger'><p><strong>no pushes</strong></p></div>
<?php } ?>