@extends('admin.layout.default')
@section('content')
<div id="content" class="content">
	<h1 class="page-header">Dashboard</h1>

	<!-- begin row -->
	<div class="row">
		<div class="col-md-3 col-sm-6">
			<div class="widget widget-stats bg-green">
				<div class="stats-icon"><i class="fa fa-desktop"></i></div>
				<div class="stats-info">
					<h4>EMAIL SUBMIT</h4>
					<p>0</p>
				</div>
				<div class="stats-link">
					<a href="<?php echo url()."/admin/emails"; ?>">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-6">
			<div class="widget widget-stats bg-green">
				<div class="stats-icon"><i class="fa fa-desktop"></i></div>
				<div class="stats-info">
					<h4>BRANDS LEADS</h4>
					<p><?php echo count($leads); ?></p>
				</div>
				<div class="stats-link">
					<a href="<?php echo url()."/admin/bleads"; ?>">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-6">
			<div class="widget widget-stats bg-green">
				<div class="stats-icon"><i class="fa fa-desktop"></i></div>
				<div class="stats-info">
					<h4>Braintree Leads</h4>
					<p><?php echo count($cb_leads); ?></p>
				</div>
				<div class="stats-link">
					<a href="<?php echo url()."/admin/cleads"; ?>">View Detail <i class="fa fa-arrow-circle-o-right"></i></a>
				</div>
			</div>
		</div>
	</div>
</div>
@stop
