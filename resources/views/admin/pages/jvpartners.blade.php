@extends('admin.layout.default')
@section('content')

<div id="content" class="content">
	<h1 class="page-header">JV Partners</h1>
	<div class='row'>
		<div class='col-md-12 ui-sortable'>
			<div class='panel panel-inverse'>
				<div class='panel-heading'>
					<div class='panel-heading-btn'>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
					</div>
					<h4 class='panel-title'>Partners Data</h4>
				</div>
				<div class='alert alert-info fade in'>
					<a id="export_jv_csv" class='btn btn-success' href="<?php echo url(); ?>/admin/exportjv">Export CSV</a>
				</div>
				<div class='panel-body'>
					<?php if(count($jv_partners) > 0) { ?>
						<div class='table-responsive' style='padding-bottom: 20px;'>
						<table id="data-table" class="table table-striped table-bordered nowrap bleads_tbl" width="100%">
							<thead>
								<tr>
									<th>Clicksure ID</th>
									<th>First name</th>
									<th>Last name</th>
									<th>Email</th>
									<th>Skype ID</th>
									<th>Country</th>
									<th>IP</th>
									<th>Date</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($jv_partners as $sub) { ?>
									<?php $lead_id = $sub['id']; ?>
									<tr data-id="<?php echo $lead_id; ?>">
                    <td><?php echo $sub['clicksure_id']; ?></td>
                    <td><?php echo $sub['fname']; ?></td>
										<td><?php echo $sub['lname']; ?></td>
                    <td><?php echo $sub['email']; ?></td>
										<td><?php echo $sub['skype_id']; ?></td>
                    <td><?php echo $sub['country']; ?></td>
                    <td><?php echo $sub['ip']; ?></td>
                    <td><?php echo $sub['created_at']; ?></td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
						</div>
					<?php } else { ?>
						<div class='alert alert-warning'><strong>no any partners</strong></div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>


@stop
