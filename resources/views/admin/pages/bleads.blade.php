@extends('admin.layout.default')
@section('content')

<div id="content" class="content">
	<h1 class="page-header">Brands Leads</h1>
	<div class='row'>
		<div class='col-md-12 ui-sortable'>
			<div class='panel panel-inverse'>
				<div class='panel-heading'>
					<div class='panel-heading-btn'>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
					</div>
					<h4 class='panel-title'>Leads Data</h4>
				</div>
				<div class='alert alert-info fade in clearfix'>
					<a id="export_bleads_csv" class='btn btn-success' href="<?php echo url(); ?>/admin/exportbleads">Export CSV</a>
					<!-- <div class='pull-left'>
						<button type='button' id="import_leads_csv" class='btn btn-success'>Import CSV</button>
						<input type="hidden" name="_csv_upload_token" value="{{csrf_token()}}">
						<a style='margin-left: 15px;' href="<?php echo url(); ?>/admin/csvsampledownload" class='btn-link'>Download CSV Template (Example)</a>
					</div>
					<div class='pull-right'>
						<a id="export_bleads_csv" class='btn btn-success' href="<?php echo url(); ?>/admin/exportbleads">Export CSV</a>
					</div> -->
				</div>
				<div class='panel-body'>
					<?php if(count($leads) > 0) { ?>
						<div class='table-responsive' style='padding-bottom: 20px;'>
						<!-- <table id="data-table" class="table table-striped table-bordered nowrap bleads_tbl" width="100%"> -->
						<table class="table table-striped table-bordered nowrap bleads_tbl" width="100%">
							<thead>
								<tr>
									<th>Brand</th>
									<th>Push</th>
									<th>Status</th>
									<th>ID</th>
									<th>Contact Details</th>
									<th>Password</th>
									<th>Phone</th>
									<th>Country</th>
									<th>IP</th>
									<th>Date</th>
									<th>LeadID</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($leads as $sub) { ?>
									<?php $lead_id = $sub['id']; ?>
									<tr data-id="<?php echo $lead_id; ?>">
										<td>
											<?php echo $sub['brand']; ?>
											<input type="hidden" name="_token_<?php echo $lead_id; ?>" value="{{csrf_token()}}">
										</td>
										<td>
											<div class='min_width_150'>
												<input type='text', class='form-control push_tags', id="push_tags_<?php echo $lead_id; ?>">
												<select class='form-control push_tags_selector' data-id="<?php echo $lead_id; ?>">
													<?php foreach($brands as $brand) { ?>
														<option value="<?php echo $brand; ?>"><?php echo $brand; ?></option>
													<?php } ?>
												</select>
											</div>
											<div class='clearfix' style='margin-top: 10px'>
												<button type='button' class='btn btn-primary btn-sm pull-left' onclick="pushhBrands(this, '<?php echo $lead_id ?>')">Push</button>
												<a class='push_cloud pull-right' href='javascript: void(0)' onclick="activatePushModal(this, '<?php echo $lead_id ?>')"><span class='fa fa-comments fa-2x'></span></a>
											</div>
										</td>
										<td>
											<?php if($sub['reject'] == 1) { ?>
											<p style='color: #ce3f38;'><strong>Declined</strong></p>
											<p><?php echo $sub['reject_reason']; ?></p>
											<?php } else { ?>
											<p style='color: #5eaf5e;'><strong>Accept</strong></p>
											<?php } ?>
										</td>
										<td>
											<span><?php echo $lead_id; ?></span><br>
											<?php if($sub['sale'] == 1) { ?>
											<i class='icon-circle-arrow-right' style='color: #51a351;'></i>
											<?php } ?>
										</td>
										<td>
											<p><?php echo $sub['lname']; ?> <?php echo $sub['fname']; ?></p>
			            		<p><a href="mailto:<?php echo $sub['email']; ?>"><?php echo $sub['email']; ?></a></p>
										</td>
										<td>
											<?php echo $sub['psw']; ?>
										</td>
										<td>
											<?php echo $sub['phone']; ?>
										</td>
										<td>
											<?php echo $sub['country']; ?>
										</td>
										<td>
											<?php echo $sub['ip']; ?>
										</td>
										<td>
											<?php echo $sub['created_at']; ?>
										</td>
										<td>
											<?php echo $sub['clickid']; ?>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
						</div>
					<?php } else { ?>
						<div class='alert alert-warning'><strong>no any leads</strong></div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade in custom_ui_modal" id="lead_push_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Lead Push</h4>
      </div>
      <div class="modal-body">

      </div>
    </div>
  </div>
</div>

@stop
