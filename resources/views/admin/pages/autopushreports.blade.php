@extends('admin.layout.default')
@section('content')

<div id="content" class="content">
	<h1 class="page-header">Auto Push Reports</h1>
	<div class='row'>
		<div class='col-md-12 ui-sortable'>
			<div class='panel panel-inverse'>
				<div class='panel-heading'>
					<div class='panel-heading-btn'>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
					</div>
					<h4 class='panel-title'>Push Reports</h4>
				</div>
				<div class='panel-body'>
          <?php if(count($autopushreports) > 0) { ?>
						<div class='table-responsive' style='padding-bottom: 20px;'>
							<table class="table table-striped table-bordered nowrap bleads_tbl" width="100%">
								<thead>
									<tr>
										<th>User</th>
										<th>Brand</th>
										<th>Status</th>
										<th>Message</th>
										<th>Schedule Date</th>
                    <th>Created Date</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($autopushreports as $lead) { ?>
										<tr>
											<td>
												<p><strong>Firstname:</strong> <?php echo $lead['lead']->fname; ?></p>
												<p><strong>Lastname:</strong> <?php echo $lead['lead']->lname; ?></p>
												<p><strong>Phone:</strong> <?php echo $lead['lead']->phone; ?></p>
												<p><strong>Country:</strong> <?php echo $lead['lead']->country; ?></p>
												<p><strong>IP:</strong> <?php echo $lead['lead']->ip; ?></p>
											</td>
											<td>
                        <?php echo $lead['report']['brand']; ?>
											</td>
											<td>
                        <?php if($lead['report']['status'] == 0) { ?>
                          <span class='label label-primary'>not started</span>
                        <?php } else if($lead['report']['status'] == 1) { ?>
                          <span class='label label-danger'>error</span>
                        <?php } else if($lead['report']['status'] == 2) { ?>
                          <span class='label label-success'>success</span>
                        <?php } ?>
                      </td>
											<td><?php echo $lead['report']['log']; ?></td>
											<td><?php echo $lead['report']['schedule_date']; ?></td>
                      <td><?php echo $lead['report']['created_at']; ?></td>
										</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					<?php } else { ?>
						<div class='alert alert-warning'><strong>no any leads</strong></div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>


@stop
