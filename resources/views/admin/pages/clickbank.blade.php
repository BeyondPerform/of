@extends('admin.layout.default')
@section('content')

<div id="content" class="content">
	<h1 class="page-header">Clickbank Leads</h1>
	<div class='row'>
		<div class='col-md-12 ui-sortable'>
			<div class='panel panel-inverse'>
				<div class='panel-heading'>
					<div class='panel-heading-btn'>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
					</div>
					<h4 class='panel-title'>Clickbank Leads Data</h4>
				</div>
				<div class='alert alert-info fade in'>
					<a id="export_cleads_csv" class='btn btn-success' href="<?php echo url(); ?>/admin/exportclickbank">Export CSV</a>
				</div>
				<div class='panel-body'>
					<?php if(count($clickbank_leads) > 0) { ?>
						<div class='table-responsive' style='padding-bottom: 20px;'>
							<table class="table table-striped table-bordered nowrap bleads_tbl" width="100%">
								<thead>
									<tr>
										<th>Contact Details</th>
										<th>Push</th>
										<th>Email</th>
										<th>Password</th>
										<th>Status</th>
										<th>Date</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($clickbank_leads as $lead) { ?>
										<?php $lead_id = $lead['id']; ?>
										<tr data-id="<?php echo $lead_id; ?>">
											<td>
												<input type="hidden" name="_token_<?php echo $lead_id; ?>" value="{{csrf_token()}}">
												<p><strong>Firstname:</strong> <?php echo $lead['fname']; ?></p>
												<p><strong>Lastname:</strong> <?php echo $lead['lname']; ?></p>
												<p><strong>Phone:</strong> <?php echo $lead['phone']; ?></p>
												<p><strong>Country:</strong> <?php echo $lead['country']; ?></p>
												<p><strong>IP:</strong> <?php echo $lead['ip']; ?></p>
											</td>
											<td>
												<div class='min_width_150'>
													<input type='text', class='form-control push_tags', id="push_tags_<?php echo $lead_id; ?>">
													<select class='form-control push_tags_selector' data-id="<?php echo $lead_id; ?>">
														<?php foreach($brands as $brand) { ?>
															<option value="<?php echo $brand; ?>"><?php echo $brand; ?></option>
														<?php } ?>
													</select>
												</div>
												<div class='clearfix' style='margin-top: 10px'>
													<button type='button' class='btn btn-primary btn-sm pull-left' onclick="pushBusersToBrands(this, '<?php echo $lead_id ?>')">Push</button>
													<a class='push_cloud pull-right' href='javascript: void(0)' onclick="activatePushModal(this, '<?php echo $lead_id ?>')"><span class='fa fa-comments fa-2x'></span></a>
												</div>
											</td>
											<td><?php echo $lead['email']; ?></td>
											<td><?php echo $lead['psw']; ?></td>
											<td>
												<?php if($lead['sale']) { ?>
													<p><span class='label label-success'>payed</span></p>
												<?php } else { ?>
													<p><span class='label label-danger'>no subscriptions</span></p>
												<?php } ?>
											</td>
											<td><?php echo $lead['created_at']; ?></td>
										</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					<?php } else { ?>
						<div class='alert alert-warning'><strong>no any leads</strong></div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade in custom_ui_modal" id="lead_push_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Lead Push</h4>
      </div>
      <div class="modal-body">

      </div>
    </div>
  </div>
</div>

@stop
