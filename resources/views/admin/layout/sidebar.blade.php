<div id="sidebar" class="sidebar">
	<!-- begin sidebar scrollbar -->
	<div data-scrollbar="true" data-height="100%">
		<!-- begin sidebar nav -->
		<ul class="nav">
			<?php
				$current_controller = substr(class_basename(Route::currentRouteAction()), 0, (strpos(class_basename(Route::currentRouteAction()), '@') -0) );
				$current_action = substr(class_basename(Route::currentRouteAction()), (strpos(class_basename(Route::currentRouteAction()), '@') + 1));
			?>
			<!-- <li class="nav-header">Navigation</li> -->
			<li <?php if($current_action == 'index') { ?> class='active' <?php } ?>><a href="<?php echo url()."/admin"; ?>"><i class="fa fa-laptop"></i> <span>Dashboard</span></a></li>
			<li class="has-sub expand">
				<a href="javascript:;">
				    <b class="caret pull-right"></b>
				    <i class="fa fa-laptop"></i>
				    <span>Leads</span>
			    </a>
				<ul class="sub-menu" style='display: block'>
				    <li <?php if($current_action == 'bleads') { ?> class='active' <?php } ?>><a href="<?php echo url()."/admin/bleads"; ?>">Brand Leads <span class='badge pull-right'><?php echo count($leads); ?></span></a></li>
				    <li <?php if($current_action == 'cleads') { ?> class='active' <?php } ?>><a href="<?php echo url()."/admin/cleads"; ?>">Braintree Leads <span class='badge pull-right'><?php echo count($cb_leads); ?></span></a></li>
				</ul>
			</li>
			<li <?php if($current_action == 'jvpartners') { ?> class='active' <?php } ?>><a href="<?php echo url()."/admin/jvpartners"; ?>"><i class="fa fa-laptop"></i> <span>JV partners</span> <span class='badge pull-right'><?php echo count($jv_partners); ?></span></a></li>
			<li <?php if($current_action == 'autopushreports') { ?> class='active' <?php } ?>><a href="<?php echo url()."/admin/autopushreports"; ?>"><i class="fa fa-laptop"></i> <span>Auto Push Reports</span> <span class='badge pull-right'><?php echo count($autopushreports); ?></span></a></li>
			<li <?php if($current_action == 'clickbank') { ?> class='active' <?php } ?>><a href="<?php echo url()."/admin/clickbank"; ?>"><i class="fa fa-laptop"></i> <span>Clickbank Leads</span> <span class='badge pull-right'><?php echo count($clickbank_leads); ?></span></a></li>
			<!-- begin sidebar minify button -->
			<li><a href="javascript:void(0)" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
      <!-- end sidebar minify button -->
		</ul>
		<!-- end sidebar nav -->
	</div>
	<!-- end sidebar scrollbar -->
</div>
