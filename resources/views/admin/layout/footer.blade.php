<!-- begin scroll to top btn -->
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->

<script>
  var base_url = "<?php echo url('/'); ?>";
</script>

<!-- ================== BEGIN BASE JS ================== -->
<script src="{{ URL::asset('admin-theme/plugins/jquery/jquery-1.9.1.min.js') }}"></script>
<script src="{{ URL::asset('admin-theme/plugins/jquery/jquery-migrate-1.1.0.min.js') }}"></script>
<script src="{{ URL::asset('admin-theme/plugins/jquery-ui/ui/minified/jquery-ui.min.js') }}"></script>
<script src="{{ URL::asset('admin-theme/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('admin-theme/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ URL::asset('admin-theme/plugins/jquery-cookie/jquery.cookie.js') }}"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<!-- <script src="{{ URL::asset('admin-theme/plugins/gritter/js/jquery.gritter.js') }}"></script> -->
<script src="{{ URL::asset('admin-theme/plugins/flot/jquery.flot.min.js') }}"></script>
<script src="{{ URL::asset('admin-theme/plugins/flot/jquery.flot.time.min.js') }}"></script>
<script src="{{ URL::asset('admin-theme/plugins/flot/jquery.flot.resize.min.js') }}"></script>
<script src="{{ URL::asset('admin-theme/plugins/flot/jquery.flot.pie.min.js') }}"></script>
<script src="{{ URL::asset('admin-theme/plugins/sparkline/jquery.sparkline.js') }}"></script>
<script src="{{ URL::asset('admin-theme/plugins/jquery-jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ URL::asset('admin-theme/plugins/jquery-jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<script src="{{ URL::asset('admin-theme/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ URL::asset('admin-custom/js/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ URL::asset('admin-custom/js/ajaxupload.js') }}"></script>
<!-- <script src="{{ URL::asset('admin-theme/js/dashboard.min.js') }}"></script>
<script src="{{ URL::asset('admin-theme/js/apps.min.js') }}"></script> -->

<script src="{{ URL::asset('admin-theme/plugins/DataTables/js/jquery.dataTables.js') }}"></script>
<script src="{{ URL::asset('admin-theme/plugins/DataTables/js/dataTables.responsive.js') }}"></script>

<script src="{{ URL::asset('admin-custom/js/toastr.min.js') }}"></script>

<script src="{{ URL::asset('admin-theme/js/dashboard.js') }}"></script>
<script src="{{ URL::asset('admin-theme/js/apps.js') }}"></script>
<!-- ================== END PAGE LEVEL JS ================== -->

<!-- App -->
<script src="{{ URL::asset('admin-custom/js/app.js') }}"></script>
