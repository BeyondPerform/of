<!doctype html>
<html>
<head>
	<meta charset="utf-8" />

	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<link rel="stylesheet" href="{{ URL::asset('admin-theme/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css') }}")>
	<link rel="stylesheet" href="{{ URL::asset('admin-theme/plugins/bootstrap/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('admin-custom/css/toastr.min.css') }}")>
	<link rel='stylesheet' href="{{ URL::asset('admin-theme/plugins/font-awesome/css/font-awesome.min.css') }}">
	<link rel='stylesheet' href="{{ URL::asset('admin-theme/css/animate.min.css') }}">
	<link rel='stylesheet' href="{{ URL::asset('admin-theme/css/style.css') }}">
	<link rel='stylesheet' href="{{ URL::asset('admin-theme/css/style-responsive.min.css') }}">
	<link rel='stylesheet' href="{{ URL::asset('admin-theme/css/theme/default.css') }}">
	<!-- ================== END BASE CSS STYLE ================== -->

	<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
	<link rel='stylesheet' href="{{ URL::asset('admin-theme/plugins/jquery-jvectormap/jquery-jvectormap-1.2.2.css') }}">
	<link rel='stylesheet' href="{{ URL::asset('admin-theme/plugins/bootstrap-datepicker/css/datepicker.css') }}">
	<link rel='stylesheet' href="{{ URL::asset('admin-theme/plugins/bootstrap-datepicker/css/datepicker3.css') }}">
	<!-- <link rel='stylesheet' href="{{ URL::asset('admin-theme/plugins/gritter/css/jquery.gritter.css') }}"> -->
	<link rel='stylesheet' href="{{ URL::asset('admin-theme/plugins/DataTables/css/data-table.css') }}" />
	<!-- ================== END PAGE LEVEL STYLE ================== -->
	<link rel='stylesheet' href="{{ URL::asset('admin-custom/css/bootstrap-tagsinput.css') }}">
	<link rel='stylesheet' href="{{ URL::asset('admin-custom/css/admin_custom.css') }}">

	<!-- ================== BEGIN BASE JS ================== -->
	<script src="{{ URL::asset('admin-theme/plugins/pace/pace.min.js') }}"></script>
	<!-- ================== END BASE JS ================== -->
</head>

<body>
	<!-- begin #page-loader -->
	<!-- <div id="page-loader" class="fade in"><span class="spinner"></span></div> -->
	<!-- end #page-loader -->

	<!-- begin #page-container -->
	<div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
		<!-- begin #header -->
    	<div id="header" class="header navbar navbar-default navbar-fixed-top">
    		@include('admin.layout.header')
    	</div>
    	<!-- end #header -->

    	<!-- begin #sidebar -->
		<div id="sidebar" class="sidebar">
			@include('admin.layout.sidebar')
		</div>
		<div class="sidebar-bg"></div>
		<!-- end #sidebar -->

   			@yield('content')

     	 <!-- begin theme-panel -->
        <div class="theme-panel">
        	@include('admin.layout.theme-panel')
        </div>
  	</div>

  	@include('admin.layout.footer')
</body>
</html>
