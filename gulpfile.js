var gulp = require('gulp');
var gulpif = require('gulp-if');
var uglify = require('gulp-uglify');
var uglifycss = require('gulp-uglifycss');
var less = require('gulp-less');
var coffee = require('gulp-coffee');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');

gulp.task('js_vendor', function () {
    return gulp.src(['bower_components/jquery/jquery.js',
        'bower_components/bootstrap/dist/js/bootstrap.js',
        'bower_components/toastr/toastr.js'])
        .pipe(concat('vendor.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('public/build/js'));
});

gulp.task('coffee', function () {
    return gulp.src([
        'public/coffee/**/*.coffee'])
        .pipe(gulpif(/[.]coffee/, coffee({bare: true})))
        .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('public/build/js'));
});

gulp.task('css_vendor', function () {
    return gulp.src([
        'bower_components/bootstrap/dist/css/bootstrap.css',
        'bower_components/toastr/toastr.css'])
        .pipe(concat('vendor.css'))
        .pipe(uglifycss())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('public/build/css'));
});

gulp.task('less', function () {
    return gulp.src([
        'public/less/**/*.less'])
        .pipe(gulpif(/[.]less/, less()))
        .pipe(uglifycss())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('public/build/css'));
});

gulp.task("watch_less", function() {
  return gulp.watch("public/less/**/*.less", function() {
    gulp.src([
        'public/less/**/*.less'])
        .pipe(gulpif(/[.]less/, less()))
        .pipe(uglifycss())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('public/build/css'));
  })
});

gulp.task("watch_coffee", function() {
  return gulp.watch("public/coffee/**/*.coffee", function() {
    gulp.src([
        'public/coffee/**/*.coffee'])
        .pipe(gulpif(/[.]coffee/, coffee({bare: true})))
        .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('public/build/js'));
  })
});

gulp.task('default', ['js_vendor', 'css_vendor', 'coffee', 'less']);
gulp.task('watch', ['watch_less', 'watch_coffee']);
