<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ClickbankTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
      Model::unguard();

      $cb_leads = DB::connection('of_old')->select("select * from cb_leads");
      foreach ($cb_leads as $l) {
        $fname = "";
        if($l->fname) {
          $fname = $l->fname;
        }
        $lname = "";
        if($l->lname) {
          $lname = $l->lname;
        }
        $email = "";
        if($l->email) {
          $email = $l->email;
        }
        $psw = "";
        if($l->psw) {
          $psw = $l->psw;
        }
        $phone = "";
        if($l->phone) {
          $phone = $l->phone;
        }
        $country = "";
        if($l->gd_country) {
          $country = $l->gd_country;
        }
        $country_code = "";
        if($l->country_code) {
          $country_code = $l->country_code;
        }
        $ip = "";
        if($l->ip) {
          $ip = $l->ip;
        }
        $lead_id = "";
        if($l->lead_id) {
          $lead_id = $l->lead_id;
        }
        $insert_stack = array(
          "fname" => $fname,
          "lname" => $lname,
          "email" => $email,
          "psw" => $psw,
          "phone" => $phone,
          "country" => $country,
          "country_code" => $country_code,
          "ip" => $ip,
          "lead_id" => $lead_id,
          "sale" => $l->sale,
          "created_at" => $l->stamp,
          "updated_at" => $l->stamp
        );
        DB::table('clickbank')->insert($insert_stack);
      }


      Model::reguard();
    }
}
