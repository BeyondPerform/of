<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class OfCrmMigration extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
      Model::unguard();

      // === migration leads from old CRM
      $leads = DB::connection('of_old')->select("select * from submits");
      foreach ($leads as $l) {
        $fname = "";
        if($l->fname) {
          $fname = $l->fname;
        }
        $lname = "";
        if($l->lname) {
          $lname = $l->lname;
        }
        $email = "";
        if($l->email) {
          $email = $l->email;
        }
        $psw = "";
        if($l->psw) {
          $psw = $l->psw;
        }
        $phone = "";
        if($l->phone) {
          $phone = $l->phone;
        }
        $country = "";
        if($l->country) {
          $country = $l->country;
        }
        $country_code = "";
        if($l->country_code) {
          $country_code = $l->country_code;
        }
        $ip = "";
        if($l->ip) {
          $ip = $l->ip;
        }
        $clickid = "";
        if($l->clickid) {
          $clickid = $l->clickid;
        }
        $brand = "";
        if($l->brand) {
          $brand = $l->brand;
        }
        $reject_reason = "";
        if($l->reject_reason) {
          $reject_reason = $l->reject_reason;
        }
        $insert_stack = array(
          "fname" => $fname,
          "lname" => $lname,
          "email" => $email,
          "psw" => $psw,
          "phone" => $phone,
          "country" => $country,
          "country_code" => $country_code,
          "ip" => $ip,
          "clickid" => $clickid,
          "sale" => $l->sale,
          "brand" => $brand,
          "reject" => $l->reject,
          "reject_reason" => $reject_reason,
          "created_at" => $l->stamp,
          "updated_at" => $l->stamp
        );
        DB::table('leads')->insert($insert_stack);
      }
      Model::reguard();
    }
}
