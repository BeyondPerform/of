<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {
    Model::unguard();

    // === USERS init seeder
    if(!DB::table('users')->where('email', 'admin@of.com')->first()) {
      DB::table('users')->insert([
          'email'    => 'admin@of.com',
          'password'      => bcrypt('shulgin66'),
          'role_id'       => 2
      ]);
    }
    if(!DB::table('users')->where('email', 'migration_seed@gmail.com')->first()) {
      DB::table('users')->insert([
          'email'    => 'migration_seed@gmail.com',
          'password'      => bcrypt('123456'),
          'role_id'       => 1
      ]);
    }

    // === ROLES init seeder
    if(!DB::table('roles')->where('role_id', 1)->first()) {
      DB::table('roles')->insert([
          'role_id' => 1,
          'role' => 'user'
      ]);
    }
    if(!DB::table('roles')->where('role_id', 2)->first()) {
      DB::table('roles')->insert([
          'role_id' => 2,
          'role' => 'admin'
      ]);
    }

    // ==== brands seeder (start)
    $seed_brands = array(
      'OneTwoTrade',
      'Keyoption',
      'FTrade',
      'OptionLive',
      'Bank of Options',
      // 'Option500',
      'TradeSolid'
    );
    foreach ($seed_brands as $brand_name) {
      if(!DB::table('brands')->where('brand', $brand_name)->first()) {
        DB::table('brands')->insert([
            'brand' => $brand_name
        ]);
      }
    }


    Model::reguard();
  }


}
