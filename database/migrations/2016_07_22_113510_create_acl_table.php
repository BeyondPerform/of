<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAclTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
      if(!Schema::hasTable('acl')) {
        Schema::create('acl', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('user_id');
          $table->string('level_id', 128);
          $table->string('level_lbl', 256);
          $table->tinyInteger('status')->default(0);
          $table->integer('payment_id')->default(0);
          $table->timestamps();
        });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
      // Schema::drop('acl');
    }
}
