<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
      if(!Schema::hasTable('users')) {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fname', 255)->default("");
            $table->string('lname', 255)->default("");
            $table->string('phone', 255)->default("");
            $table->string('email', 255)->unique();
            $table->string('password', 255);
            $table->string('country', 255)->default("");
            $table->string('country_code', 64)->default("");
            $table->string('ip', 64)->default('0.0.0.0');
            $table->string('lead_id', 64);
            $table->integer('role_id')->default(1);
            $table->tinyInteger('block')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::drop('users');
    }
}
