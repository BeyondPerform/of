<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    if(!Schema::hasTable('roles')) {
      Schema::create('roles', function (Blueprint $table) {
        $table->increments('id')->index();
        $table->tinyInteger('role_id')->default(1);
        $table->string('role');
        $table->timestamp('created_at');
      });
    }
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    // Schema::drop('roles');
  }

}
