<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
      if(!Schema::hasTable('payments')) {
        Schema::create('payments', function (Blueprint $table) {
          $table->increments('id');
          $table->tinyInteger('status')->default(0);
          $table->mediumText('payment_details');
          $table->timestamps();
        });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
      // Schema::drop('payments');
    }
}
