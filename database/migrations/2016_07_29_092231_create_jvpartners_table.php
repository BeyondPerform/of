<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJvpartnersTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
      if(!Schema::hasTable('jvpartners')) {
        Schema::create('jvpartners', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('clicksure_id');
          $table->string('fname', 512);
          $table->string('lname', 512);
          $table->string('email', 256);
          $table->string('skype_id', 256);
          $table->string('ip', 64);
          $table->string('country', 512);
          $table->string('country_code', 45);
          $table->timestamps();
        });
      }

      if(Schema::hasTable('jvpartners')) {
        Schema::table('jvpartners', function($table) {
          if(!Schema::hasColumn('jvpartners', 'skype_id')) {
            $table->string('skype_id', 256)->after('email');
          }
          if(!Schema::hasColumn('jvpartners', 'lname')) {
            $table->string('lname', 512)->after('fname');
          }
        });
      }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
      // Schema::drop('jvpartners');
    }
}
