<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
      if(!Schema::hasTable('country')) {
        Schema::create('country', function (Blueprint $table) {
          $table->increments('id');
          $table->string('iso', 256);
          $table->string('name', 256);
          $table->timestamps();
        });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
      // Schema::drop('country');
    }
}
