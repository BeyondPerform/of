<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClickbankTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
      if(!Schema::hasTable('clickbank')) {
        Schema::create('clickbank', function (Blueprint $table) {
          $table->increments('id');
          $table->string('fname', 256)->default("");
          $table->string('lname', 256)->default("");
          $table->string('email', 256)->default("");
          $table->string('psw', 256)->default("");
          $table->string('phone', 256)->default("");
          $table->string('country', 256)->default("");
          $table->string('country_code', 24)->default("");
          $table->string('ip', 128)->default("");
          $table->string('lead_id', 64)->default("");
          $table->tinyInteger('sale')->default(0);
          $table->timestamps();
        });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
      // Schema::drop('clickbank');
    }
}
