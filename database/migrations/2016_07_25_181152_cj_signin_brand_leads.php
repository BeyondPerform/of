<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CjSigninBrandLeads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
      if(!Schema::hasTable('cj_signin_brand_leads')) {
        Schema::create('cj_signin_brand_leads', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('user_id');
          $table->string('brand', 256);
          $table->tinyInteger('status')->default(0);
          $table->timestamp('schedule_date');
          $table->string('log', 512)->default("");
          $table->mediumText('debug');
          $table->timestamps();
        });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        // Schema::drop('cj_signin_brand_leads');
    }
}
