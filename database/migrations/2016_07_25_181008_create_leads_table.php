<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadsTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    # ==== base version
    if(!Schema::hasTable('leads')) {
      Schema::create('leads', function (Blueprint $table) {
        $table->increments('id');
        $table->string('fname', 256);
        $table->string('lname', 256);
        $table->string('email', 256);
        $table->string('psw', 256);
        $table->string('phone', 256);
        $table->string('country', 512);
        $table->string('country_code', 45);
        $table->string('ip', 64);
        $table->string('clickid', 64);
        $table->tinyInteger('sale')->default(0);
        $table->string('brand', 256);
        $table->tinyInteger('reject')->default(0);
        $table->mediumText('reject_reason');
        $table->timestamps();
      });
    }

    # ==== updates
    if(Schema::hasTable('leads')) {

      Schema::table('leads', function($table) {
        if(!Schema::hasColumn('leads', 'uid')) {
          $table->integer('uid')->after('id')->default(0);
        }
      });

    }
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    // Schema::drop('leads');
  }

}
