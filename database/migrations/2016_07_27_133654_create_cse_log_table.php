<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCseLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
      if(!Schema::hasTable('cse_log')) {
        Schema::create('cse_log', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('ces_id');
          $table->integer('user_id');
          $table->integer('mail_id');
          $table->tinyInteger('status')->default(0);
          $table->timestamp('schedule_date');
          $table->string('log', 512)->default("");
          $table->timestamps();
        });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
      Schema::drop('cse_log');
    }
}
