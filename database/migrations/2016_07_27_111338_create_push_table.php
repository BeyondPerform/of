<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePushTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    if(!Schema::hasTable('push')) {
      Schema::create('push', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('lead_id');
        $table->string('brand', 256);
        $table->string('type', 256)->default("brand");
        $table->tinyInteger('status')->default(0);
        $table->mediumText('response');
        $table->timestamps();
      });
    }
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    // Schema::drop('push');
  }
}
