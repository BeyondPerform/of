<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
      if(!Schema::hasTable('brands')) {
        Schema::create('brands', function (Blueprint $table) {
          $table->increments('id');
          $table->string('brand', 256);
          $table->timestamps();
        });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
      // Schema::drop('brands');
    }
}
